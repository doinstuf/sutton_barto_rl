from setuptools import setup, find_packages
import sys

if not float(sys.version[:3]) >= 3.7:
    raise Exception("Only Python 3.7 and later are supported.")

setup(
    name="sutton_barto_rl",
    packages=[
        package for package in find_packages() if package.startswith("sutton_barto_rl")
    ],
    install_requires=[],
    description="http://incompleteideas.net/book/the-book-2nd.html",
    author="deo1",
    url="https://bitbucket.org/deo1/sutton_barto_rl/src/master/",
    author_email="",
    version="1.0",
)
