from copy import copy
from dataclasses import dataclass, field
from random import choice, randint

from mdp.alias import Dict, List, Iterator, Tuple, Any
from mdp.framework import Agent, Environment, Policy
from mdp.unit import A, S, T, SelectA
from mdp.learning import Algorithms

states = [S(randint(0, int(1e12))) for _ in range(100_000)]
actions = [A(randint(0, int(1e12))) for _ in range(100_000)]
assert len(set(states)) == len(set(actions)) == 100_000
rewards = [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, -1.0]
r_rand = lambda: choice(rewards)


@dataclass
class Env(Environment):
    s: S
    num_s: int = 5  # number of states

    def __post_init__(self):
        super().__post_init__()
        self._states = states[: self.num_s]

    def on_action(self, a: A) -> S:
        return S(a.name)

    def all_sn(self, s: S) -> List[S]:
        return self._states

    def all_s(self) -> List[S]:
        return self._states


@dataclass
class Agt(Agent):
    env: Environment
    num_a: int = 5  # number of actions

    def __post_init__(self):
        self._actions = actions[: self.num_a]

    @staticmethod
    def calc_reward(s: S, sn: S) -> float:
        return r_rand()

    def all_a(self, s: S) -> List[A]:
        return self._actions

    def all_t(self, a: A) -> Iterator[T]:
        return (
            T(t=0, s=s, a=a, r=r, sn=sn, rn=rn)
            for s in self.env.all_s()
            for r in rewards
            for sn in self.env.all_sn(s)
            for rn in rewards
        )


def dummy_episode(agent: Agent, verbose: int = 0, kwargs: Dict[str, Any] = None):
    agent.reset()
    steps: List[T] = []
    reward = 0.0
    while not reward:
        t = agent.act()
        steps.append(t)
        reward = t.rn
    return steps


if __name__ == "__main__":
    """
    Learnings:
    Avoid copies within your `Agent` and `Environment` class, e.g. of `S` and `A`. These are `frozen=True` for that reason.
    Avoid creating new instances when possible.
    Define fast `__hash__` and `__eq__` methods for your `S` and `A` classes.
    Careful with complicated `reset` or `__post_init__` methods on the `Agent` and `Environment`. These can be called each episode.
    """
    from mdp.benchmark import Config, DfFmt, pd, run_benchmark

    # config
    configs = [
        Config(alg, n, select, {})
        for alg in [
            "Algorithms.mc_first_visit",
            "Algorithms.mc_exploring_starts",
            "Algorithms.mc_on_policy_εgreedy",
            # "Algorithms.episodic_value_iteration",
            # "Algorithms.value_iteration",
        ]
        for n in [25_000]
        for select in [SelectA.best]  # , SelectA.rand]
    ]

    # run benchmarks
    results = []
    for num_s_a in [10, 100]:
        agent = Agt(num_a=num_s_a, env=Env(num_s=num_s_a, s=choice(states[:num_s_a])))
        agent.π.v.θ = 0.001
        df = run_benchmark(agent=agent, ep=dummy_episode, configs=configs)
        df["num_s_a"] = num_s_a
        results.append(df)
    rdf = pd.concat(results)

    # summarize results
    algorithm_summary = DfFmt.summarize(rdf, ["alg", "num_s_a"])
    code_summary = DfFmt.summarize(rdf, ["file", "line", "func"])
    by_file = DfFmt.summarize(rdf, ["file"])

    print("\n| algorithm summary:\n", algorithm_summary.head(25))
    print("\n| code summary:\n", code_summary.head(25))
    print("\n| by filename:\n", by_file.head(10))
