from operator import xor
from random import choice, choices, randint, random
from string import ascii_letters, digits
from typing import Any, Callable, DefaultDict, Dict, List, Optional, Union

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

pd.set_option("display.max_columns", 50)
pd.set_option("max_colwidth", 100)
pd.set_option("display.width", 250)
Episode = Any


def nested_xor(l: List[int], i: Optional[int] = None) -> Optional[int]:
    """Recursive pairwise xor of all elements in list"""
    if l and i:  # recursion
        return nested_xor(l[:-1], xor(l[-1], i))
    if l:  # entry
        if len(l) == 2:
            return xor(l[0], l[1])
        return nested_xor(l[:-1], l[-1])
    return i  # terminal


def flatten(l: List[List[Any]]) -> List[Any]:
    """Flatten a list-like object"""
    return [item for sublist in l for item in sublist]


def random_str(n: int = 5) -> str:
    """An n letter string composed of ascii letters and numbers."""
    return "".join(choices(ascii_letters + digits, k=n))


def where(df: pd.DataFrame, func: Callable[[pd.Series], bool]) -> pd.DataFrame:
    """
    A query chaining method that replaces the default index syntax.
    ```python
    df = df.where(lambda x: x['colA'] < 0).where(lambda x: x['colB'] > 0)
    ```
    """
    return df[func(df)]


pd.DataFrame.where = where


should_repr = lambda par, sub: not sub.startswith("_") and (
    par.__dataclass_fields__[sub].repr
    if hasattr(par, "__dataclass_fields__") and sub in par.__dataclass_fields__
    else True
)


val_fmt = (
    lambda v: str(v).split(".")[-1]
    if not isinstance(v, (float, int, bool))
    else int(v)
    if isinstance(v, bool)
    else v
)


def num_or_str(v: Any) -> bool:
    vt = type(v)
    return bool(vt == int or vt == str or vt == float)


def number_repr(ob: Any) -> str:
    return ", ".join(
        [
            f"{k}={v: .1f}" if isinstance(v, float) else f"{k}={v:2,}"
            for k, v in ob.__dict__.items()
            if isinstance(v, int) or isinstance(v, float)
        ]
    )


S = Any  # mdp.unit.S
Y = Any  # mdp.unit.Y


class Plot:
    """Plotting utilities."""

    @staticmethod
    def delta_heatmap(υ: Dict[S, Y]):
        """Useful for debug - rendering values during training."""

        delta = [v.δ for v in υ.values()]
        # delta = [v.v for v in υ.values()]
        sq = np.sqrt(len(delta))
        size = int(np.ceil(sq))
        pad = max(size ** 2 - np.floor(sq) ** 2 - 1, 0)
        delta = delta + [0 for _ in range(int(pad))]
        if size:
            values_sq = np.reshape(np.array(delta), (size, size))
            _ = sns.heatmap(values_sq, annot=True, fmt=".2f", cbar=False)
        plt.show()

    @staticmethod
    def vdf(swp: DefaultDict[str, List], i: str = "iteration") -> pd.DataFrame:
        """Dataframe of values table."""
        df = pd.DataFrame(swp)
        sweep_count = df[i].max()
        sweeps_to_plot = set(np.round(np.logspace(0, np.log2(sweep_count), 15, base=2)))
        sweeps_to_plot.add(0)
        sweeps_to_plot.add(sweep_count - 1)
        df["plot"] = df[i].apply(lambda x: x in sweeps_to_plot)
        return df


if __name__ == "__main__":
    pass
