import pytest
from random import randint, random, choice

from framework import S, A
from learning import R, Backup, γr, γr_rev
from unit import T


ε = 1e-6  # pytest.approx(v, ε)
m = 1_000
s_a_num = 5
rn = lambda: round(2 * (random() - 0.3), 1)
name = lambda: randint(0, s_a_num - 1)
step = lambda t, r: T(t=t, s=S(name()), a=A(name()), r=r, sn=S(name()), rn=r)


class TestR:
    @staticmethod
    def test_unit_discount():
        r, rω = R(γ=1.0, rev=False, γr=γr), R(γ=1.0, rev=True, γr=γr_rev)
        for i in range(m):
            r_next = R(r=rn(), t=i)
            r += r_next
            rω += r_next

        assert r.G == rω.G
        assert r.Q == rω.Q
        assert r.ΣG == rω.ΣG
        assert r.rμ * r.n == pytest.approx(r.Σr, ε)
        assert r.n == m
        assert r.n - 1 == r.t

    @staticmethod
    def test_forwards_backwards():
        rf, rb = R(γ=0.9, rev=False, γr=γr), R(γ=0.9, rev=True, γr=γr_rev)
        rewards = [(rn(), i) for i in range(m)]
        rewards = zip(rewards, reversed(rewards))
        G, Gω = [], []
        for r, rω in rewards:
            rf += R(r=r[0], t=r[1])
            rb += R(r=rω[0], t=rω[1])
            G.append(rf.G)
            Gω.append(rb.G)
            print(
                f"({r[1]}) G: {rf.G:.2f}, Gω: {rb.G:.2f} (r{r[1]+1}: {r[0]}, r{rω[1]+1}: {rω[0]})"
            )

        Q, Qω = sum(G) / len(G), sum(Gω) / len(Gω)
        print(f"G avg: {Q:.2f}, Gω avg: {Qω:.2f}")
        print(rf)
        print(rb)
        assert rf.rμ == pytest.approx(rb.rμ, ε)
        assert rf.G == pytest.approx(rb.G, ε)
        assert rf.Q == pytest.approx(Q, ε)
        assert rb.Q == pytest.approx(Qω, ε)
        assert rf.Σr == pytest.approx(rb.Σr, ε)
        assert rb.G != round(rf.G, 3)
        assert rf.ΣG != round(rb.ΣG, 3)
        assert rf.Q != round(rb.Q, 3)

    @staticmethod
    def test_reset():
        reward = 1
        returns = Backup(rev=False)
        for i in range(2):
            returns += step(t=0, r=reward)
            for j in range(m):
                if i == 1 and j == m - 1:
                    break
                returns += step(t=j + 1, r=reward)

        assert returns.G.n == returns.G.G == returns.G.Σr == m
        assert returns.G.t == m - 1
        assert returns.G.rμ == 1

    @staticmethod
    def test_reset_rev():
        reward = 1
        returns = Backup(rev=True)
        for i in reversed(range(2)):
            returns += step(t=m, r=reward)
            for j in reversed(range(m)):
                if i == 0 and j == 0:
                    break
                returns += step(t=j, r=reward)

        assert returns.G.n == returns.G.G == returns.G.Σr == m
        assert returns.G.t == 1
        assert returns.G.rμ == 1

    @staticmethod
    def test_sidechain_Q():
        rf, rb = R(γ=1.0), R(γ=1.0)
        correct, n = 0.0, 0
        for i in range(m):
            g = rn()
            if choice([True, False]):
                correct += g
                n += 1
                rf += R(G=g, n=i + 1)
                rb += R(G=g, n=i + 1)

        assert correct == pytest.approx(rf.ΣG, ε)
        assert correct / n == pytest.approx(rf.Q, ε)

    @staticmethod
    def test_state_avg_returns():
        returns = Backup(rev=True, γ=0.9)
        for j in reversed(range(m)):
            returns += step(t=j, r=rn())

        sreturns, areturns = {}, {}
        for s in returns.avg_r:
            sreturns[s] = returns.avg_r[s].Q
            areturns[s] = sum(ar.Q for ar in returns.bd[s].values()) / len(
                returns.bd[s]
            )
        assert pytest.approx(sreturns, areturns)
        assert len(sreturns) == s_a_num
        assert max(len(a) for a in returns.bd.values()) == s_a_num

        srewards, arewards = {}, {}
        for s in returns.avg_r:
            srewards[s] = returns.avg_r[s].rμ
            arewards[s] = sum(ar.rμ for ar in returns.bd[s].values()) / len(
                returns.bd[s]
            )
        assert pytest.approx(srewards, arewards)
        assert len(srewards) == s_a_num
        assert max(len(a) for a in returns.bd.values()) == s_a_num

    @staticmethod
    def test_state_max_returns():
        returns = Backup(rev=True, γ=0.9)
        for j in reversed(range(m)):
            returns += step(t=j, r=rn())

        sreturns, areturns = {}, {}
        for s in returns.avg_r:
            sreturns[s] = returns.max_r[s].Q
            areturns[s] = max(list(returns.bd[s].values())).Q
        assert pytest.approx(sreturns, areturns)
        assert len(sreturns) == s_a_num
        assert max(len(a) for a in returns.bd.values()) == s_a_num


if __name__ == "__main__":
    TestR.test_reset_rev()
    TestR.test_state_avg_returns()
