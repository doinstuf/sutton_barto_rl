"""
Defines the environment, state, agent, and actions of a markov decision process.
"""
from collections import defaultdict
from copy import copy, deepcopy
from dataclasses import dataclass, field
from random import choice, random

from IPython.display import clear_output

try:
    from mdp.alias import Any, Callable, DDict, Dict, List, Optional, Union
    from mdp.learning import V, R
    from mdp import util
except ImportError:
    from alias import Any, Callable, DDict, Dict, List, Optional, Union
    import util  # type: ignore
    from learning import V, R

try:
    from unit import (
        A,
        AQ,
        S,
        SQ,
        T,
        TQ,
        Behavior,
        SelectA,
        SelectAQ,
        SelectAλ,
        Strategy,
    )
except ImportError:
    from mdp.unit import (  # type: ignore
        A,
        AQ,
        S,
        SQ,
        T,
        TQ,
        Behavior,
        SelectA,
        SelectAQ,
        SelectAλ,
        Strategy,
    )


def rand_behavior(s: S, actions: List[A]) -> A:
    return choice(actions)


@dataclass
class Policy:
    """
    ### Init
    ```python
    name: str = ""  # user-friendly name of policy
    select: SelectAλ = SelectA.rand  # action-value selection for vₖ(s)
    behavior: Behavior = lambda self, s, actions: choice(actions)  # off-policy behavior
    strategy: Strategy = Strategy.off  # on or off policy learning
    require_stable: bool = False  # don't stop iteration until no policy changes
    ε: float = 0.0  # param for ε-soft policies (probability of off-policy action)
    es: bool = False  # exploring starts indicates random action for t0
    v: V = V()  # value functions and data structures
    target: DDict[S, AQ]  # the target policy
    ```

    ### Other
    ```python
    converged: bool  # v.Δ below v.θ and stable policy
    changes_per: DDict[S, int]  # track the target policy changes per state
    changes: int = 0  # total number of policy changes per sweep
    stable: int = 0  # consecutive sweeps without policy changes
    ```
    """

    name: str = field(default="")
    select: SelectAλ = field(default=SelectA.rand, repr=False)
    behavior: Behavior = field(default=rand_behavior, repr=False)
    strategy: Strategy = Strategy.off
    v_init: Dict = field(default_factory=dict, repr=False)
    v: V = V()
    require_stable: bool = False
    ε: float = 0.0
    es: bool = False

    changes_per: DDict[S, int] = field(
        default_factory=lambda: defaultdict(int), repr=False
    )
    changes: int = 0
    stable: int = 0
    target: DDict[S, AQ] = field(
        default_factory=lambda: defaultdict(lambda: A()), repr=False
    )

    @property
    def converged(self) -> bool:
        no_changes = self.changes_calc() == 0
        policy_stable = self.stable >= 4
        values_converged = self.v.Δ <= self.v.θ

        if self.require_stable:
            converged = no_changes and policy_stable and values_converged
        else:
            converged = values_converged

        return converged

    def changes_calc(self) -> int:
        """Called each sweep. Calculate the count of target policy changes."""
        changes = sum(self.changes_per.values())
        if changes == 0 and self.changes == 0:
            self.stable += 1
        else:
            self.stable = 0
            self.changes = changes

        return changes

    def update(self, s: S, av: R) -> None:
        """Update the target policy."""
        if s not in self.target or self.target[s] != av.a:
            self.changes_per[s] = 1
            self.target[s] = av.a
        else:
            self.changes_per[s] = 0

    def eval(self, s: S, actions: List[A] = [], kwargs: Dict[str, Any] = {}) -> AQ:
        """Evaluate the policy against the state and action."""
        if actions:
            kwargs.update({"actions": actions, "s": s})
        else:
            kwargs.update({"s": s})

        if self.strategy != Strategy.off and s in self.target and random() >= self.ε:
            return self.target[s]
        return self.behavior(**kwargs)  # type: ignore

    def print(self):
        clear_output(wait=True)
        print(self)


PolicyQ = Optional[Policy]


@dataclass
class Environment:
    """
    ```python
    s: S
    t: int = 0
    ```
    Methods that need to be implemented by the specific model.
    ```
    on_action(self, a: A) -> S  # generate the next state
    all_sn(self, s: S) -> List[S]  # all next states for DP methods
    all_s(self) -> List[S]  # all possible states for DP methods
    ```
    """

    s: S
    t: int = 0
    first_s: S = field(init=False, repr=False)

    def __post_init__(self) -> None:
        self.first_s = self.s

    def reset(self, s: SQ = None) -> None:
        self.t = 0
        if s is not None:
            self.s = s
            self.first_s = s
        else:
            self.s = self.first_s

    def act(self, a: A) -> S:
        """Public api for an agent to interact with environment."""
        sn = self.on_action(a)
        self.s = sn
        self.t += 1
        return sn

    def on_action(self, a: A) -> S:
        """Called by `act()`. This gets overwritten by inheritting class."""
        raise NotImplementedError

    def all_sn(self, s: S) -> List[S]:
        """All possible next states from current state, regardless of action."""
        raise NotImplementedError

    def all_s(self) -> List[S]:
        """All possible states that could occur. For use in `mdp.learning.Algorithms.value_iteration`."""
        raise NotImplementedError


EnvironmentQ = Optional[Environment]


@dataclass
class Agent:
    """
    ### Init
    ```python
    env: Environment  # environment object with the state
    π: Policy = Policy()  # Values stored here with the policy
    ```
    ### Other
    ```
    a: AQ = None  # latest action
    r: float = 0.0  # latest reward
    Σr: float = 0.0  # total rewards
    ```
    Methods that need to be implemented by the specific model.
    ```
    all_a(s: S) -> List[A]  # Possible actions from current state.
    all_t(self, a: A) -> List[T]  # Possible steps
    calc_reward(s: S, sn: S) -> float  # reward
    ```
"""

    env: Environment
    π: Policy = Policy()
    a: AQ = field(default=None)
    r: float = field(default=0.0, repr=False)
    Σr: float = field(default=0.0, repr=True)

    def reset(self, s: SQ = None, deep: bool = False) -> None:
        self.r = 0.0
        self.Σr = 0.0
        self.a = None
        self.env.reset(s)
        if deep:
            self.π = Policy()

    @staticmethod
    def calc_reward(s: S, sn: S) -> float:
        raise NotImplementedError

    @staticmethod
    def all_a(s: S) -> List[A]:
        """Possible actions given current state."""
        raise NotImplementedError

    def all_t(self, a: A) -> List[T]:
        """All possible next steps."""
        raise NotImplementedError

    def act(self, a: AQ = None) -> T:
        r, t, s = self.r, self.env.t, self.env.s
        if a is None and self.π.es and self.env.t == 0:
            a = choice(self.all_a(s))  # random start action for exploring starts
        self.a = a if a is not None else self.eval(s)
        sn = self.env.act(self.a)
        self.r = self.calc_reward(s, sn)
        self.Σr += self.r

        return T(t, s, self.a, r, sn, self.r)

    def eval(self, s: S) -> A:
        return self.π.eval(s, self.all_a(s))


AgentQ = Optional[Agent]
