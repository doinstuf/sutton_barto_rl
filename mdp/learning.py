from collections import defaultdict, deque
from dataclasses import dataclass, field
from operator import xor

import numpy as np

try:
    from alias import Any, BoolQ, Callable, CallableQ, DDict
    from alias import Dict, Iterator, List, Tuple, Union
    from unit import AQ, PQ, SQ, TQ, A, P, S, SelectA, SelectAλ, Strategy, T, Y
    import util
except ImportError:
    from mdp.alias import Any, BoolQ, Callable, CallableQ, DDict
    from mdp.alias import Dict, Iterator, List, Tuple, Union
    from mdp.unit import AQ, PQ, SQ, TQ, A, P, S, SelectA, SelectAλ, Strategy, T, Y
    from mdp import util  # type: ignore

Policy = Any  # mdp.framework.Policy
Agent = Any  # mdp.framework.Agent


def γr(γ, G, r, t):
    "Discounted return iteration function."
    return G + γ ** t * r


def γr_rev(γ, G, r, t):
    "Discounted return (revrerse) iteration function."
    return γ * G + r


@dataclass
class R:
    """
    Returns object for computing running returns efficiently.

    ```python
    r: float = 0.0         # reward at step t of episode
    rμ: float = 0.0        # avg reward at step t of episode
    t: int = 0             # step count in the episode
    a: AQ = None           # the action, for computing probabilities
    sn: SQ = None          # the next state, for computing s' probabilities
    γ: float = 1.0         # discount recency weight
    υ: Y = 0.0             # estimated bellman value by iteration
    G: float = 0.0         # the return of the rewards r
    Q: float = 0.0         # the expected return
    n: int = 0             # total number of visits across all episodes
    Σr: float = 0.0        # sum of the rewards at each step
    ΣG: float = 0.0        # sum of the return at each step
    ps: PQ = None          # s' probability
    pa: PQ = None          # action probability
    rev: bool = True       # episode played in reverse
    γr: CallableQ = γr_rev # func for computing disc returns
    lock: bool = False     # lock Q due to v_init
    ```
    """

    r: float = field(default=0.0)
    t: int = field(default=0)
    a: AQ = field(default=None, repr=False)
    sn: SQ = field(default=None, repr=False)
    γ: float = field(default=1.0)
    υ: Y = field(default=None)
    G: float = field(default=0.0)
    n: int = field(default=0)
    Σr: float = field(default=0.0)
    ΣG: float = field(default=0.0)
    Q: float = field(default=0.0)
    _Q: float = field(init=False, repr=False)
    ps: PQ = field(default=None, repr=False)
    pa: PQ = field(default=None, repr=False)
    rev: bool = field(default=True, repr=False)
    γr: CallableQ = field(default=None, repr=False)
    lock: bool = field(default=False, repr=False)

    @property
    def rμ(self) -> float:
        return self.Σr / self.n

    @property
    def Q(self) -> float:
        return self._Q if self.lock or self.n == 0 else self.ΣG / self.n

    @Q.setter
    def Q(self, o: float):
        self._Q = o

    def __add__(self, rn: "R") -> "R":
        # initial accumulate
        self.n += 1
        self.t, self.a, self.sn, self.r = rn.t, rn.a, rn.sn, rn.r

        # compute discounted return
        self.G = rn.G if rn.n else self.γr(self.γ, self.G, self.r, self.t)

        # probability updates
        if self.pa:
            self.pa += rn.a
        if self.ps:
            self.ps += rn.sn

        # 2nd order accumulate
        self.Σr += self.r
        self.ΣG += self.G

        return self

    def __repr__(self) -> str:
        return f"R({util.number_repr(self)})"

    def __iter__(self) -> "R":
        yield self

    def __lt__(self, o) -> bool:
        return self.Q < o.Q

    def __lshift__(self, o) -> "R":
        """Used as an in-place greater than assignment"""
        return o if o > self else self


def bd_init(γ: float, rev: bool, γr: Callable) -> DDict[S, DDict[AQ, R]]:
    return defaultdict(lambda: defaultdict(lambda: R(γ=γ, rev=rev, γr=γr, ps=P())))


def avg_r_init(γ: float, rev: bool, γr: Callable) -> DDict[S, R]:
    return defaultdict(lambda: R(γ=γ, rev=rev, γr=γr, pa=P(), υ=Y(0.0)))


def max_r_init(γ: float, rev: bool, γr: Callable) -> DDict[S, R]:
    return defaultdict(lambda: R(Q=-1e9, γ=γ, rev=rev, γr=γr))


@dataclass
class Backup:
    """
    Backup diagram and methods for navigating the backup diagram.
    ```python
    rev: bool = True  # True if this is an episode being played in reverse
    γ: float = 1.0  # discount for weighting future returns
    use_rn: bool = True  # use the reward from t + 1 instead of t for returns
    v_init: Dict[S, Y]  # initial values for the tabular value function
    G: R = R(γ, rev, γr)  # return for all steps in the current episode
    avg_r: DDict[S, R]  # state values/returns table
    max_r: Dict[S, R]  # the action with max value. while the avg value can be found in `avg_r`
    bd: DDict[S, DDict[AQ, R]]  # future discounted rewards for all state/action combinations
    υ: Dict[S, Y]  # State-value table (run Algorithms.value_iteration())
    Q: Dict[S, R]  # State-returns table (run Algorithms.mc_first_visit())
    aQ: Dict[S, Dict[A, R]]  # Action-returns table (run Algorithms.mc_exploring_starts())
    ```
    """

    rev: bool = True
    γ: float = 1.0
    use_rn: bool = True
    v_init: Dict[S, Y] = field(default_factory=dict, repr=False)
    G: R = field(init=False, repr=False)
    bd: DDict[S, DDict[AQ, R]] = field(init=False, repr=False)
    γr: Callable = field(init=False, repr=False)
    avg_r: DDict[S, R] = field(init=False, repr=False)
    max_r: DDict[S, R] = field(init=False, repr=False)

    def __post_init__(self):
        self.γr = γr_rev if self.rev else γr
        self.G = R(γ=self.γ, rev=self.rev, γr=self.γr)
        self.bd = bd_init(self.γ, self.rev, self.γr)
        self.avg_r = avg_r_init(self.γ, self.rev, self.γr)
        self.max_r = max_r_init(self.γ, self.rev, self.γr)

        # static value init (will be locked)
        for s, v in self.v_init.items():
            """None action is used to store state-values"""
            self.avg_r[s].Q = v.v
            self.avg_r[s].υ = v
            self.avg_r[s].lock = True

    @property
    def υ(self):
        """bellman update values"""
        return {s: v.υ for s, v in self.avg_r.items()}

    @property
    def Q(self):
        """'state-values"""
        return self.avg_r

    @property
    def aQ(self):
        """state-action-values"""
        return self.bd

    @property
    def node_count(self):
        """Number of nodes (state-action pairs) in the backup diagram."""
        return len([r for a in self.bd.values() for r in a.values()])

    @property
    def update_count(self):
        """Number of times nodes in the backup diagram were updated."""
        return sum([r.n for a in self.bd.values() for r in a.values()])

    def __add__(self, t: T) -> "Backup":
        """Add a step to the backup diagram and update returns."""
        tn, s, sn, a, r = t.t, t.s, t.sn, t.a, t.rn if self.use_rn else t.r
        G = self.G

        # start of new episode
        if (self.rev and G.t == 0) or (not self.rev and tn == 0):
            G = R(γ=self.γ, rev=self.rev, γr=self.γr)

        # add the step reward to the episodic return G,
        # then to the state and action values (Q = avg(G0...))
        G += R(r=r, t=tn, a=a, sn=sn)
        self.bd[s][a] += G
        self.avg_r[s] += G
        self.max_r[s] <<= self.bd[s][a]
        self.G = G

        return self

    def rs(self, s: S) -> R:
        """Discounted return of state."""
        return self.avg_r[s]

    def ra(self, s: S, a: A) -> R:
        """Discounted return of action."""
        return self.bd[s][a]

    def sn(self, s: S, a: A) -> List[S]:
        """List of next states."""
        return list(self.ra(s, a).ps)

    def an(self, s: S) -> List[A]:
        """List of next actions."""
        return list(self.rs(s).pa)

    def ars(self, s: S) -> List[R]:
        """List of discounted returns `R` per action."""
        return list(self.bd[s].values())

    def psn(self, s: S, a: A, sn: S) -> float:
        """P(s'|s,a) Probability of next state given state and action."""
        return self.bd[s][a].ps[sn]

    def psa(self, s: S, a: A) -> float:
        """P(a|s) Probability of action given state."""
        return self.rs(s).pa[a]

    def rsn(self, s: S, a: A) -> List[R]:
        """Returns of next states s'."""
        return [self.rs(sn) for sn in self.sn(s, a)]


StateValue = Callable[[Policy, S], float]
ActionValue = Callable[[Policy, S, A], float]
Episode = Callable[[Agent], List[T]]


@dataclass
class V:
    """
    Values, backup diagram, and value equations.
    ```python
    θ: float = 1e-6  # threshold cutoff for value iteration Δ
    use_rn: bool = True  # use the reward from t + 1 instead of t for returns
    γ: float = 0.9  # the recency discount for calculating returns
    t: int = 0  # iteration count
    v_init: Dict[S, Y]  # initial values for the tabular value function
    swp: DDict[str, List] = {}  # sweep logs per iteration
    r: Backup = Backup()  # backup diagram and returns
    Δ: float  # max dist between current value estimates and prev
    ```
    """

    t: int = 0
    γ: float = field(default=0.9, repr=True)
    θ: float = 1e-6
    use_rn: bool = True
    v_init: Dict[S, Y] = field(default_factory=dict, repr=False)
    swp: DDict[str, List] = field(default_factory=lambda: defaultdict(list), repr=False)
    r: Backup = field(init=False, repr=False)

    def __post_init__(self):
        self.r = Backup(γ=self.γ, v_init=self.v_init, use_rn=self.use_rn)

    def __repr__(self) -> str:
        f = 7 if self.θ >= 1e-6 else 10
        return f"V(t={self.t}, γ={self.γ}{', use_rn=F' if not self.use_rn else ''}, θ={self.θ:.{f}f}, Δ={self.Δ:.{f}f})"

    @property
    def Δ(self) -> float:
        if self.r.bd:
            return max([self.r.rs(s).υ.δ for s in self.r.bd])
        return 1e3

    def qπkn(self, ar: R) -> R:
        """As defined in eq. 4.10"""
        γ, a, r = self.γ, ar.a, 0 if self.v_init else ar.rμ
        υs = lambda sn: self.r.rs(sn).υ.v
        p = lambda sn: ar.ps[sn]
        bellman = lambda p, r, v: p * (r + γ * v)

        q = 0.0
        for sn in ar.ps:
            q += bellman(p(sn), r, υs(sn))
        return R(a=a, Q=q)

    def υπkn(self, π: Policy, s: S) -> R:
        """As defined in eq. 4.10"""
        ars = self.r.ars(s)
        avs = [self.qπkn(a) for a in ars]
        av = π.select(ars=avs)
        self.r.avg_r[s].υ += Y(av.Q)
        return av

    def sweep(self, i: str = "iteration", k: int = 1):
        results = self.r.Q
        self.swp["state"] += results
        self.swp["value_estimate"] += [r.υ.v for r in results.values()]
        self.swp["Q"] += [r.Q for r in results.values()]
        self.swp[i] += [self.t for _ in range(len(results))]
        self.t += k


EpProcessor = Callable[[Episode, Agent, int, int], Episode]


@dataclass
class Algorithms:
    """
    Learning methods built on top of the backup diagram and returns classes.

    ## Functions
    ```python
    policy_iteration(agent: Agent)
    value_iteration(agent: Agent, states: List[S]) # After eq. 4.10
    mc_first_visit(episode: Episode, agent: Agent, kwargs: Dict[str, Any], n: int = 10)
    mc_exploring_starts(episode: Episode, agent: Agent, kwargs: Dict[str, Any], n: int = 10)
    mc_on_policy_εgreedy(episode: Episode, agent: Agent, kwargs: Dict[str, Any], n: int = 10, ε: float = 0.1)
    ```
    """

    @staticmethod
    def episodes(
        episode: Episode,
        agent: Agent,
        kwargs: Dict[str, Any],
        n: int = 10,
        ep_processor: EpProcessor = lambda ep, a, n, i: ep,
    ) -> Tuple[int, Iterator[List[T]]]:
        """Episode generator for monte carlo methods."""
        from tqdm import tqdm

        for i in tqdm(range(n), mininterval=0.5, smoothing=0.0):
            yield i, ep_processor(episode(agent, **kwargs), agent, n, i)

    @staticmethod
    def rev_ep(ep: Episode, a: Agent, n: int, i: int) -> Episode:
        return reversed(ep)

    @staticmethod
    def policy_iteration(agent: Agent) -> Dict[S, A]:
        print(f"Using `{agent.π.select.__name__}()` to select action-values.")
        print(f"Doc string: {agent.π.select.__doc__}")
        for s in agent.π.v.r.bd:
            agent.π.update(s, agent.π.select(s, agent.π.v.r))
        return agent.π.target

    @staticmethod
    def value_iteration(
        agent: Agent,
        states: List[S] = None,
        verbose: int = 1,
        kwargs: Any = None,
        episode: Any = None,
        n: Any = None,
    ) -> Dict[S, Y]:
        """
        Algorithm as defined immediately following eq. 4.10.
        Args `kwargs` and `episode` and `n` are not used by are there for a common function signature.
        """
        states = agent.env.all_s() if states is None else states
        first_pass = True
        while not agent.π.converged:
            for s in states:
                if first_pass:
                    for a in agent.all_a(s):
                        agent.env.reset(s)
                        for t in agent.all_t(a):
                            agent.π.v.r += t
                av = agent.π.v.υπkn(agent.π, s)
                agent.π.update(s, av)
            agent.π.v.sweep()
            if verbose:
                agent.π.print()
            first_pass = False
            # util.Plot.delta_heatmap(agent.π.v.r.υ)
        return agent.π.v.r.υ

    @staticmethod
    def mc_episodic_core(
        episode: Episode,
        agent: Agent,
        kwargs: Dict[str, Any],
        n: int = 10,
        verbose: int = 1,
        ep_processor: EpProcessor = lambda ep, a, n, i: ep,
    ) -> Agent:
        """Base algorithm for all episodic mc algorithms."""

        rev_ep = Algorithms.rev_ep if agent.π.v.r.rev else ep_processor
        for i, ep in Algorithms.episodes(episode, agent, kwargs, n, rev_ep):

            #  compute returns
            for t in ep:
                agent.π.v.r += t

                # update policy: necessary for on-policy behavior
                if agent.π.strategy != Strategy.off:
                    s = t.s
                    agent.π.update(s, agent.π.select(s, agent.π.v.r))

            #  log
            if i == 500 or (i and i % 2_000 == 0):
                agent.π.v.sweep(i="episode", k=0)
            agent.π.v.t += 1

        if agent.π.strategy == Strategy.off:
            _ = Algorithms.policy_iteration(agent)
        return agent

    @staticmethod
    def mc_first_visit(
        episode: Episode,
        agent: Agent,
        kwargs: Dict[str, Any],
        n: int = 10,
        verbose: int = 1,
    ) -> Dict[S, R]:
        """
        State values are the discounted, averaged returns for each state.
        Section 5.1, page 92.
        """

        # configure
        memory, agent.π.v.r.rev = agent.π.v.r.rev, True

        agent = Algorithms.mc_episodic_core(episode, agent, kwargs, n, verbose)
        agent.π.v.r.rev = memory
        return agent.π.v.r.Q

    @staticmethod
    def mc_exploring_starts(
        episode: Episode,
        agent: Agent,
        kwargs: Dict[str, Any],
        n: int = 10,
        verbose: int = 1,
    ) -> Dict[S, Dict[A, R]]:
        """
        State values are the discounted, averaged returns for each state.
        Section 5.3, page 99.
        """

        # configure
        memory = agent.π.v.r.rev, agent.π.es
        agent.π.v.r.rev, agent.π.es = True, True

        agent = Algorithms.mc_episodic_core(episode, agent, kwargs, n, verbose)
        agent.π.v.r.rev, agent.π.es = memory
        return agent.π.v.r.aQ

    @staticmethod
    def mc_on_policy_εgreedy(
        episode: Episode,
        agent: Agent,
        kwargs: Dict[str, Any],
        n: int = 10,
        ε: float = 0.1,
        verbose: int = 1,
    ) -> Dict[S, Dict[A, R]]:
        """
        State values are the discounted, averaged returns for each state.
        Section 5.4, page 101.
        """

        # configure
        memory = agent.π.v.r.rev, agent.π.ε, agent.π.strategy
        agent.π.v.r.rev = True
        agent.π.ε = ε if agent.π.ε == 0 else agent.π.ε
        agent.π.strategy = Strategy.ε_greedy

        agent = Algorithms.mc_episodic_core(
            episode, agent, kwargs, n, verbose, Algorithms.log_decreasing_ε
        )
        agent.π.v.r.rev, agent.π.ε, agent.π.strategy = memory
        return agent.π.v.r.aQ

    @staticmethod
    def episodic_value_iteration(
        episode: Episode,
        agent: Agent,
        kwargs: Dict[str, Any],
        n: int = 10,
        verbose: int = 1,
    ) -> Dict[S, Y]:
        """
        Use the bellman value update via posterior
        probabilities estimated from episodes.
        """
        from random import choices

        memory, agent.π.v.r.rev = agent.π.v.r.rev, False
        for i, ep in Algorithms.episodes(episode, agent, kwargs, n):

            #  update state and action probabilities
            for t in ep:
                agent.π.v.r += t

            #  choose n states at random to update value (for performance guarantees)
            n = min(3, len(ep))
            for t in choices(ep, k=n):
                av = agent.π.v.υπkn(agent.π, t.s)
                agent.π.update(t.s, av)

            #  log
            if i == 500 or (i and i % 2_000 == 0):
                agent.π.v.sweep(i="episode", k=0)
            agent.π.v.t += 1
        agent.π.v.r.rev = memory
        return agent.π.v.r.υ

    @staticmethod
    def gradient_boosting(
        episode: Episode,
        agent: Agent,
        kwargs: Dict[str, Any],
        n: int = 10,
        verbose: int = 1,
        b: "Boosting" = None,
    ) -> "Boosting":
        try:
            from boosting import Boosting
        except:
            from mdp.boosting import Boosting

        b = Boosting(agent=agent, n=n) if b is None else b
        i_total = 0 if b.mat is None else b.mat[b.cols.ep].max()
        b.epoch += 1
        data = defaultdict(list)
        for i, ep in Algorithms.episodes(episode, agent, kwargs, n, Algorithms.rev_ep):
            for t in ep:
                agent.π.v.r += t
                s, a, tt = t.s, t.a, t.t
                rsa = agent.π.v.r.ra(s, a)
                data["states"].append(s)
                data["actions"].append(a)
                data["values"].append(agent.π.v.r.G.G)
                data["eps"].append(i + i_total)
                data["ts"].append(tt)
                data["qa"].append(rsa.Q)
                data["rμ"].append(rsa.rμ)
        b.gen_mat(**data)
        b.fit()
        return b

    @staticmethod
    def log_decreasing_ε(episode: Episode, agent: Agent, n: int, i: int) -> Episode:
        try:
            logs_ε = getattr(agent.π, "logs_ε")
        except:
            logs_ε = Algorithms.logs_ε(n, 10, agent.π.ε)
            agent.π.logs_ε = logs_ε
        agent.π.ε = logs_ε[i] if i in logs_ε else agent.π.ε
        return episode

    @staticmethod
    def logs_ε(n: int, m: int, beg: float, end: float = 0.001) -> Dict[int, float]:
        """
        Generates log spaced ε values for training.
        ```python
        n: int      # number of episodes
        m: int      # number of unique ε values
        beg: float  # first ε value
        end: float  # last ε value
        ```
        Returns
        ```python
        Dict[int, float]  # {iteration: ε}
        ```
        """
        ε_list = np.logspace(np.log2(beg), np.log2(end), m, base=2)
        i_list = range(0, n, n // m)
        return {i: ε for i, ε in zip(i_list, ε_list)}
