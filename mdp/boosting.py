from collections import defaultdict
from dataclasses import dataclass, field
from functools import lru_cache
from itertools import product

import catboost as cb

try:
    from alias import Any, Callable, DDict, Dict, List, Set, Tuple, Union
    from learning import Algorithms, R, γr_rev
    from unit import A, F, S, Strategy, T
    from util import np, pd
except ImportError:
    from mdp.alias import Any, Callable, DDict, Dict, List, Set, Tuple, Union
    from mdp.learning import Algorithms, R, γr_rev
    from mdp.unit import A, F, S, Strategy, T
    from mdp.util import np, pd

Agent = Any
Feature = Union[str, int, float]
Features = Tuple[Feature, ...]


# https://catboost.ai/docs/concepts/python-reference_parameters-list.html
cb_args = lambda: {
    "depth": 5,
    "iterations": 250,
    "loss_function": "RMSE",  # RMSE, MAE, Quantile, LogLinQuantile, Poisson, MAPE, Lq
    "random_seed": 24,
    "learning_rate": 0.03,  # 0.03
    "l2_leaf_reg": 3.0,  # 3.0
    "bootstrap_type": "Bayesian",  # Bayesian, Bernoulli, MVS
    "bagging_temperature": 1.0,  # 1.0
    "sampling_frequency": "PerTreeLevel",  # PerTreeLevel, PerTree
    "sampling_unit": "Object",  # Object, Group
    "rsm": 1.0,
    "grow_policy": "SymmetricTree",  # SymmetricTree, Depthwise, Lossguide
    "boosting_type": "Plain",  # Plain, Ordered
    "leaf_estimation_method": "Gradient",  # Gradient, Newton
}


def boosting_format(b: "Boosting") -> Dict[str, Any]:
    """For `Boosting.__str__`"""
    df, nn = b.mat, b.mat is not None
    return {
        "π.name": b.agent.π.name,
        "π.strategy": b.agent.π.strategy.name,
        "π.es": b.agent.π.es,
        "π.ε": b.agent.π.ε,
        "model.loss_function": b.cb_args["loss_function"],
        "model.depth": b.cb_args["depth"],
        "last_n_epochs": b.last_n_epochs,
        "weight_groups": b.weight_groups,
        "first_s": b.first_s,
        "epochs": f"{df.epoch.min()} to {df.epoch.max()}" if nn else "~",
        "episodes": f"{round(df.ep.max() - df.ep.min(), -3):,}" if nn else "~",
        "policy_cache": b.__getitem__.cache_info(),
    }


@dataclass(repr=False)
class featuresdict(defaultdict):
    """Uses the key in the default factory func."""

    schema: pd.DataFrame

    def __missing__(self, s):
        ret = self[s] = self.default_factory(s)
        return ret

    def default_factory(self, s):
        df = pd.DataFrame(self.schema)
        size = df.shape[0]
        feats = {f: v for f, v in s.features().items() if f != "s_hash"}
        cols, vals = list(feats), list(feats.values())
        s_feat = np.repeat(vals, repeats=size).reshape(len(cols), size).T
        df[cols] = s_feat
        return df


@dataclass
class Cols:
    """
    Keeps track of column names.
    ```python
    # Init
    s: S  # state template, used for generating feature names
    a: A  # action template, '''

    # Properties
    pre_agg: List[str]      # Feature columns as the steps come in
    post_agg: List[str]     # Feature columns as they are post-stats computation
    m_feats: List[str]      # combinations between metric and stat e.g. 'Q_std'
    metrics: List[str]      # Qa + rμ aggregate metrics from backup diagram
    aggs: Dict[str, List]   # aggregations for metrics
    keys: List[str]         # state + actions cols
    step: List[str]         # e + ep + t cols
    design_cols: List[str]  # all columns given to model/predictor

    # Variables
    S: str = "S"                               # state column
    A: str = "A"                               # action column
    e: str = "epoch"                           # epoch column
    ep: str = "ep"                             # episode column
    t: str = "t"                               # time step column
    Qa: str = "Qa"                             # avg discounted return of the state-action
    rμ: str = "rμ"                             # avg reward of the state-action
    w: str = "weight"                          # weight per row in modeling
    loss: str = "Q"                            # the loss column (e.g. return of ep)
    pred: str = "Qp"                           # prediction of loss by the model
    k_hash: Lists[str] = ["s_hash", "a_hash"]  # cols that store the hash of keys
    stats: List[str]                           # agg funcs to be computed on metrics
    cat: Set[str] = {}                         # categorical columns
    s_feats: List[str]                         # initialized from self.s
    a_feats: List[str]                         # initialized from self.a
    s_feats_all: List[str]                     # feats including *_hash
    a_feats_all: List[str]                     # feats including *_hash
    ```
    """

    s: S = field(repr=False)
    a: A = field(repr=False)

    S: str = field(default="S")
    A: str = field(default="A")
    e: str = field(default="epoch")
    ep: str = field(default="ep")
    t: str = field(default="t")
    Qa: str = field(default="Qa")
    rμ: str = field(default="rμ")
    w: str = field(default="weight")
    loss: str = field(default="Q")
    pred: str = field(default="Qp")

    k_hash: List[str] = field(default_factory=lambda: ["s_hash", "a_hash"])
    stats: List[str] = field(default_factory=lambda: ["mean", "std", "median"])
    cat: Set[str] = field(default_factory=set)
    s_feats: List[str] = field(init=False)
    a_feats: List[str] = field(init=False)
    s_feats_all: List[str] = field(init=False, repr=False)
    a_feats_all: List[str] = field(init=False, repr=False)

    def __post_init__(self):
        s = self.s.features(val_only=False)
        a = self.a.features(val_only=False)
        self.s_feats_all = [name for name in s]
        self.a_feats_all = [name for name in a]
        self.s_feats = [n for n in s if n not in self.k_hash]
        self.a_feats = [n for n in a if n not in self.k_hash]
        if not self.cat:
            self.cat.update([n for n, f in s.items() if f.cat and n not in self.k_hash])
            self.cat.update([n for n, f in a.items() if f.cat and n not in self.k_hash])

    @property
    def keys(self) -> List[str]:
        return [self.S, self.A]

    @property
    def step(self) -> List[str]:
        return [self.e, self.ep, self.t]

    @property
    def metrics(self) -> List[str]:
        return []  # [self.Qa, self.rμ]

    @property
    def aggs(self) -> Dict[str, List]:
        """Arg for `df.groupby().agg()`."""
        aggs = {f: "first" for f in self.s_feats + self.a_feats}
        aggs.update({m: self.stats for m in self.metrics})
        return aggs

    @property
    def m_feats(self) -> List[str]:
        """All flattened column names for combinations between metric and stat."""
        aggs = self.aggs
        return [f"{m}_{s}" for m in self.metrics for s in aggs[m]]

    @property
    def pre_agg(self) -> List[str]:
        """Feature columns as they are pre-stats i.e. as the steps come in."""
        return [*self.s_feats, *self.a_feats, *self.metrics]

    @property
    def post_agg(self) -> List[str]:
        """Feature columns as they are post-stats i.e. after all episodes are complete."""
        return [*self.s_feats, *self.a_feats, *self.m_feats]

    @property
    def design_cols(self) -> List[str]:
        """Columns that make up the design matrix."""
        return self.post_agg


Filter = Callable[[pd.Series], pd.Series]


@dataclass
class Boosting:
    """
    Apply gradient boosting techniques to learn the optimal policy for an episodic MDP.
    ```python
    agent: Agent                                 # agent
    n: int = 1_000                               # number of episodes
    cb_args: Dict[str, Any] = {}                 # args for model
    last_n_epochs: int = 5                       # latest epochs to keep. 0 is all.
    weight_groups: bool = True                   # inverse weight rows by state percent of total
    first_s: bool = True                         # keep only first seen action-state step per ep
    self.min_weight: float = 0.01                # minimum weight per sample (if weight_groups)
    step_filter: Callable[[pd.Series], pd.Series] # user callable to remove steps from episodes
    cols: Cols                                   #
    feat_lookup: DDict[S, pd.DataFrame]          # give data to predictor
    feat_lookup_df: pd.DataFrame                 # '''
    actions: pd.DataFrame                        # action features
    model: cb.CatBoostRegressor = cb(**cb_args)  # catboost model
    ```

    ### Other
    ```python
    pool: cb.Pool          # catboost pool object
    mat: pd.DataFrame      # design matrix (one row per step)
    epoch: int = 0         # current iteration of training
    ```
    """

    agent: Agent = field(repr=False)

    n: int = field(default=1_000, repr=True)
    cb_args: Dict[str, Any] = field(default_factory=cb_args, repr=True)
    last_n_epochs: int = field(default=5, repr=True)
    weight_groups: bool = field(default=True, repr=True)
    first_s: bool = field(default=True, repr=True)
    min_weight: float = field(default=0.01, repr=True)
    step_filter: Filter = field(default=None, repr=False)

    model: cb.CatBoostRegressor = field(init=False, repr=False)
    pool: cb.Pool = field(init=False, repr=False)
    mat: pd.DataFrame = field(default=None, repr=False)
    epoch: int = field(default=0, repr=True)
    cols: Cols = field(default=None, repr=False)
    feat_lookup: DDict[S, pd.DataFrame] = field(default=None, repr=False)
    feat_lookup_df: pd.DataFrame = field(default=None, repr=False)
    actions: pd.DataFrame = field(default=None, repr=False)

    def __post_init__(self):
        self.model = cb.CatBoostRegressor(**self.cb_args)

    @property
    def design_schema(self) -> pd.DataFrame:
        """New dataframe instance with the design schema and action features."""
        cols, size = self.cols.design_cols, self.actions.shape[0]
        df = pd.DataFrame(columns=cols, data=np.zeros((size, len(cols))))
        df[self.actions.columns] = self.actions
        return df

    @lru_cache(maxsize=None)
    def __getitem__(self, s: S) -> A:
        """The predictor class acts like a target policy table."""
        features = self.feat_lookup[s]
        actions = features[self.cols.A].values
        vals: List[float] = self.model.predict(features.drop(columns=[self.cols.A]))
        best: A = self.best_action(actions, vals)
        return best

    def __contains__(self, s: S) -> bool:
        """Since this is a function approximator, every state is valid."""
        return True if self.mat is not None else False

    def __hash__(self) -> int:
        """Dummy hash just for the `__getitem__` lru_cache"""
        return -1

    def __str__(self) -> str:
        cfg = boosting_format(self)
        cfgstr = ",\n".join(f"  {k}: {v}" for k, v in cfg.items())
        return f"| config:\n{cfgstr}"

    @staticmethod
    def best_action(actions: List[Features], vals: List[float]) -> int:
        """Hash of action with highest estimated value."""
        assert len(actions) == len(vals)
        return actions[np.argmax(vals)]

    def fit(self, v: int = 50):
        cols = self.cols
        if self.weight_groups:
            self.calc_weights()
        self.pool = cb.Pool(
            self.mat[cols.design_cols], label=self.mat[cols.loss], cat_features=cols.cat
        )
        if self.weight_groups:
            self.pool.set_weight(self.mat[cols.w])

        self.model.fit(self.pool, verbose=v)
        self.mat[cols.pred] = self.model.predict(self.mat[cols.design_cols])

        # make the model the target policy of the agent
        self.agent.π.target = self
        self.agent.π.strategy = Strategy.on
        self.__getitem__.cache_clear()

    def gen_mat(
        self,
        states: List[S],
        actions: List[A],
        values: List[float],
        eps: List[int],
        ts: List[int],
        qa: List[float],
        rμ: List[float],
    ):
        epoch = [self.epoch for _ in range(len(states))]
        if self.mat is None:
            self.cols = Cols(s=states[0], a=actions[0])
        cols, l = self.cols, len(states)

        # make dataframe (row per step)
        data = {
            cols.S: states,
            cols.A: actions,
            cols.loss: values,
            cols.ep: eps,
            cols.t: ts,
            cols.e: epoch,
            cols.Qa: qa,
            cols.rμ: rμ,
            cols.w: [np.nan for _ in range(l)],
            cols.pred: [np.nan for _ in range(l)],
        }
        data.update({f: [s.features()[f] for s in states] for f in cols.s_feats_all})
        data.update({f: [a.features()[f] for a in actions] for f in cols.a_feats_all})
        df = pd.DataFrame(data)
        df = df.where(self.step_filter) if self.step_filter else df

        if self.first_s:
            df = df.groupby([cols.ep, *cols.k_hash]).agg("last").reset_index()
        if self.mat is None:
            self.mat = df
        else:
            self.mat = self.mat.drop(
                columns=cols.m_feats
            )  # get rid of old aggregate features
            assert list(self.mat.columns) == list(df.columns)
            self.mat = pd.concat([self.mat, df], sort=False).reset_index(drop=True)
        if self.last_n_epochs:
            drop_epoch = self.epoch - self.last_n_epochs if self.last_n_epochs else -1
            self.mat = self.mat.where(lambda x: x[cols.e] > drop_epoch)

        # generate aggregate lookup features keyed off of state and action
        agg_cols = [*cols.keys, *cols.m_feats]
        self.gen_feat_lookup()
        self.mat = self.mat.merge(self.feat_lookup_df[agg_cols], on=cols.keys)

    def calc_weights(self):
        """Adjust weight based on state groups."""
        s, t = self.cols.S, self.cols.t
        group_counts = self.mat[[s, t]].groupby(s).agg("count")
        group_min: int = group_counts[t].min()
        group_counts = group_counts.to_dict()[t]
        weight = lambda g: max(group_min / group_counts[g], self.min_weight)
        group_id = lambda gk: tuple(gk)[1:] if len(gk) > 2 else tuple(gk)[1]
        self.mat[self.cols.w] = [
            weight(group_id(g)) for g in self.mat[[s]].itertuples()
        ]

    def gen_feat_lookup(self):
        """Dict for looking up features for runtime prediction."""
        cols = self.cols
        keep, a_cols = [*cols.keys, *cols.pre_agg], [cols.A, *cols.a_feats]
        df = self.mat[keep].groupby(cols.keys).agg(cols.aggs).reset_index(level=1)
        df.columns = [cols.A, *cols.post_agg]
        self.actions = df[a_cols].groupby(a_cols, as_index=False).all()
        df = df.fillna(0.0)  # std dev can be NaN here if too few samples
        self.feat_lookup = featuresdict(schema=self.design_schema)
        for s in df.index:
            sfeat = df.xs(s)
            try:  # series
                self.feat_lookup[s] = sfeat.to_frame().T
            except:  # frame
                self.feat_lookup[s] = sfeat
        self.feat_lookup_df = df.reset_index()

    def analyze(self) -> Tuple[Dict[S, R], Dict[S, A]]:
        """Format for plotting."""
        max_a = lambda a, v: max(zip(a, v), key=lambda a_v: a_v[1])
        cols = self.cols

        # avg per action-value
        avs = (
            self.mat[[*cols.keys, cols.pred]]
            .groupby(cols.keys, as_index=False)
            .agg("mean")
        )

        # all action avgs per state
        svs = avs.groupby(cols.S).agg(lambda x: list(x)).reset_index()

        # best action-value pair
        svs["best"] = svs.apply(lambda row: max_a(row[cols.A], row[cols.pred]), axis=1)

        qs: Dict[S, R] = {
            row[1][cols.S]: R(Q=row[1]["best"][1]) for row in svs.T.items()
        }
        p: Dict[S, float] = {
            row[1][cols.S]: self.agent.π.target[row[1][cols.S]] for row in svs.T.items()
        }
        p_all = {
            row[1][cols.S]: {
                a: round(v, 3)
                for a, v in zip(
                    self.feat_lookup[row[1][cols.S]][cols.A],
                    self.model.predict(
                        self.feat_lookup[row[1][cols.S]].drop(columns=cols.A)
                    ),
                )
            }
            for row in svs.T.items()
        }

        return qs, p, p_all
