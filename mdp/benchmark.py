from dataclasses import dataclass, field
from pstats import SortKey, Stats  # type: ignore
from random import choice

import pandas as pd

from mdp.alias import Any, Callable, Dict, Iterator, List, Tuple
from mdp.framework import Agent
from mdp.learning import Algorithms, Episode
from mdp.unit import SelectA, SelectAλ


StatsVals = Tuple[str, int, str, int, int, float, float]


@dataclass
class StatsFmt:
    """Format the the `pstats.Stats` object like rows in a table."""

    stats: Stats
    col_names: Tuple[str] = field(
        default_factory=lambda: ("file", "line", "func", "ncalls", "tottime", "cumtime")
    )

    def __iter__(self) -> StatsVals:  # type: ignore
        yield from self.stats_row(self.stats)

    @staticmethod
    def stats_row(p: Stats) -> Iterator[StatsVals]:
        stats = p.strip_dirs().sort_stats(SortKey.TIME).stats  # type: ignore
        for k, v in stats.items():
            yield (*k, *v[1:4])


@dataclass
class Config:
    """
    A container that defines each run of the benchmark.
    ```python
    alg: str  # method from mdp.Algorithms e.g. "Algorithm.mc_first_visit"
    n: int  # number of episodes
    select: SelectAλ  # mdp.unit.Select method
    ep_args: Dict[str, Any]  # kwargs for your Episode function
    ```
    """

    __slots__ = ["alg", "n", "select", "ep_args"]
    alg: str
    n: int
    select: SelectAλ
    ep_args: Dict[str, Any]

    @property
    def col_names(self) -> Tuple[str, ...]:
        cols = [s for s in self.__slots__ if s != "ep_args"]
        ep_cols = [s for s in self.ep_args]
        return tuple(cols + ep_cols)

    @property
    def col_vals(self) -> Tuple[Any, ...]:
        vals = [getattr(self, s) for s in self.__slots__ if s != "ep_args"]
        ep_args = [i for i in self.ep_args.values()]
        return tuple(vals + ep_args)


@dataclass
class DfFmt:
    """Helper functions for formatting the `pstats.Stats` profile results as a `DataFrame`."""

    @classmethod
    def brackets(self, x: pd.Series) -> pd.Series:
        return x.startswith("<") & x.endswith(">")

    @classmethod
    def strip_brackets(self, x: pd.Series) -> pd.Series:
        return x.strip("<").strip(">") if self.brackets(x) else x

    @classmethod
    def simplify_func(self, x: pd.Series) -> pd.Series:
        return (
            x.split("method")[-1]
            .split(" objects")[0]
            .split(" of type object ")[0]
            .strip()
        )

    @classmethod
    def python_files(self, x: pd.Series) -> pd.Series:
        return (
            x["file"]
            if x["file"] != "~"
            else x["func"].split(" of ")[-1].strip("'")
            if " of " in x["func"]
            else x["func"].split(".")[0].strip("'")
        )

    @classmethod
    def func_builtins(self, x: pd.Series) -> pd.Series:
        return x.split(" of ")[0].split(".")[-1].strip("'")

    @classmethod
    def fmt_profile_results(
        self, results: List[StatsVals], cfg: Config, stats: StatsFmt
    ) -> pd.DataFrame:
        df = (
            pd.DataFrame(results, columns=(*cfg.col_names, *stats.col_names))
            .sort_values(by=["alg", "tottime"], ascending=False)
            .reset_index(drop=True)
        )
        df["select"] = df["select"].apply(lambda x: x.__name__)
        df["alg"] = df["alg"].apply(lambda x: x.split(".")[1])
        df["file"] = df["file"].apply(lambda x: self.strip_brackets(x))
        df["func"] = df["func"].apply(lambda x: self.strip_brackets(x))
        df["func"] = df["func"].apply(lambda x: self.simplify_func(x))
        df["file"] = df.apply(lambda x: self.python_files(x), axis=1)
        df["func"] = df["func"].apply(lambda x: self.func_builtins(x))
        return df

    @staticmethod
    def summarize(
        rdf: pd.DataFrame,
        cols: List[str] = ["file", "line", "func"],
        aggs: List[str] = ["mean", "min", "max", "median", "sum"],
    ) -> pd.DataFrame:
        """Aggregate `aggs` values of `rdf.tottime` within `cols` groupings."""
        return (
            rdf.groupby(cols)
            .agg({"tottime": aggs, "n": ["sum", "count"], "ncalls": "sum"})
            .sort_values(by=("tottime", "sum"), ascending=False)
            .reset_index()
            .round(4)
        )


def run_benchmark(agent: Agent, ep: Episode, configs: List[Config]) -> pd.DataFrame:
    """Runs `cfg.alg(ep, agent, cfg.ep_args, cfg.n) for cfg in configs`."""
    from cProfile import runctx

    results: List[StatsVals] = []
    for cfg in configs:
        print(f"\n{cfg}")
        agent.π.select = cfg.select
        cmd = f"{cfg.alg}(episode=ep, agent=agent, kwargs=cfg.ep_args, n=cfg.n)"
        runctx(cmd, globals(), locals(), "restats")
        stats = StatsFmt(Stats("restats"))
        results += [(*cfg.col_vals, *s) for s in stats]  # type: ignore
    return DfFmt.fmt_profile_results(results, cfg, stats)
