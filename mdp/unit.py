"""
Contains the fundamental unit or building block objects for the learning and
interacting framework. This module should not need to import any other higher
level modules. Should make it easier to read through the algorithms, esp. with
intellisense.
"""
from collections import defaultdict
from dataclasses import dataclass, field
from enum import Enum
from operator import xor

try:
    from mdp.alias import Any, AnyQ, Callable, DDict, Dict
    from mdp.alias import Iterator, List, Num, Optional, Tuple, Union
    from mdp import util
except:
    from alias import Any, AnyQ, Callable, DDict, Dict
    from alias import Iterator, List, Num, Optional, Tuple, Union
    import util  # type: ignore


class Strategy(Enum):
    """The strategy for the policy."""

    on = 0
    off = 1
    ε_greedy = 2


@dataclass
class F:
    """
    Feature
    ```python
    name: str
    val: Union[int, str, float]
    cat: bool  # True is categorical
    ```
    """

    __slots__ = ["name", "val", "cat"]

    name: str
    val: Union[int, str, float]
    cat: bool


def feat_val(ob: Any, f: F, val_only: bool) -> Union[F, Any]:
    f_val = getattr(ob, f.name)
    f_val = f_val if util.num_or_str(f_val) else hash(f_val)
    return f_val if val_only else F(f.name, f_val, f.cat)


def features_method(
    ob: Any, a_or_s: str, val_only: bool = True
) -> Dict[str, Union[Any, F]]:
    """
    Returns the invidivdual feature values if enumerated in:
    ```python
    ob._feat: List[F]
    ```
    """
    # always add hash as feature
    # add it first so that it can be accessed in order
    feat = {
        f"{a_or_s}_hash": hash(ob) if val_only else F(f"{a_or_s}_hash", hash(ob), True)
    }

    if hasattr(ob, "_feat"):
        feat.update({f.name: feat_val(ob, f, val_only) for f in ob._feat})
    return feat


@dataclass
class P:
    """
    Probability
    ```python
    n: int = 0              # global count
    c: DDict[Any, int] = 0  # local count
    ```
    """

    n: int = 0
    c: DDict[Any, int] = field(default_factory=lambda: defaultdict(int))

    def __getitem__(self, key: Any) -> int:
        """Probability of `key`."""
        if key in self.c and self.n:
            return self.c[key] / self.n
        return 0.0

    def __repr__(self) -> str:
        return f"P(n={self.n}, {', '.join([str(k) + '=' + f'{self[k]:.1%}' for k in self])})"

    def __iter__(self) -> Any:
        yield from self.c

    def __contains__(self, key: Any) -> bool:
        return key in self.c

    def __add__(self, key: Any):
        if key:
            self.c[key] += 1
            self.n += 1
        return self

    def __hash__(self) -> int:
        return -100 * self.n ** 2

    def __eq__(self, o) -> bool:
        return self.n == o.n


PQ = Optional[P]
_feat: List[F] = [F("name", None, False)]


@dataclass(frozen=True)
class _StepAtomic:
    """
    Atomic object representing state and action per step.
    Immutable because it must be hashable and will avoid need to deepcopy.
    Subclasses must implement:
    ```python
    _feat: List[F]
    @property feat_vals(self) -> Tuple[Num, ...]
    @property feat_size(self) -> Tuple[Num, ...]
    ```
    """

    name: int = field(default=1, repr=True)

    @property
    def feat_vals(self) -> Tuple[Num, ...]:
        """Used as indices, hashing, and comparison."""
        return (self.name,)

    @property
    def feat_size(self) -> Tuple[Num, ...]:
        """Max possible vals correspending to `feat_vals`."""
        return (10_000,)

    def __eq__(self, o) -> bool:
        """Uniquely defines the learnable properties of state."""
        try:
            return self.feat_vals == o.feat_vals
        except:
            return False

    def __hash__(self) -> int:
        """Uniquely defines the learnable properties of state."""
        return hash(self.feat_vals)  # slow: example only

    def __lt__(self, o) -> bool:
        self.feat_vals < o.feat_vals

    def features(self, val_only: bool = True) -> Dict[str, Union[Any, F]]:
        return NotImplementedError


@dataclass(frozen=True)
class S(_StepAtomic):
    """
    State. Immutable because it must be hashable and
    will avoid need to deepcopy. Subclasses must implement:
    ```python
    _feat: List[F]
    @property feat_vals(self) -> Tuple[Num, ...]
    @property feat_size(self) -> Tuple[Num, ...]
    __eq__(self, o) -> bool
    __hash__(self) -> int
    ```
    """

    def features(self, val_only: bool = True) -> Dict[str, Union[Any, F]]:
        return features_method(self, "s", val_only=val_only)


SQ = Optional[S]


@dataclass(frozen=True)
class A(_StepAtomic):
    """
    Action. Immutable because it must be hashable and
    will avoid need to deepcopy. Subclasses must implement:
    ```python
    _feat: List[F]
    @property feat_vals(self) -> Tuple[Num, ...]
    @property feat_size(self) -> Tuple[Num, ...]
    __eq__(self, o) -> bool
    __hash__(self) -> int
    ```
    """

    def features(self, val_only: bool = True) -> Dict[str, Union[Any, F]]:
        return features_method(self, "a", val_only=val_only)


AQ = Optional[A]


@dataclass(frozen=True)
class T:
    """
    Step (immutable)
    `{t0, s0, a0, r0, s1, r1, i}`
    ```python
    t: int  # time step
    s: S  # state
    a: A  # action
    r: float  # reward
    sn: S = field(repr=False)  # state t+1
    rn: float = 0.0  # reward t+1
    i: IntQ = None  # episode number
    ```
    """

    __slots__ = ["t", "s", "a", "r", "sn", "rn"]

    t: int  # time step
    s: S  # state
    a: A  # action
    r: float  # reward
    sn: S  # state t+1
    rn: float  # reward t+1

    def __gt__(self, o: "T") -> bool:
        return self.t > o.t

    def __lt__(self, o: "T") -> bool:
        return self.t < o.t

    def __eq__(self, o: Any) -> bool:
        try:
            return self.s == o.s and self.a == o.a
        except:
            return False

    def __hash__(self) -> int:
        """State and action define a unique step for first-instance episodes."""
        return hash((self.s, self.a))

    @classmethod
    def rename(self, v: str) -> str:
        r_map = {
            "p_value": "pv",
            "d_value": "dv",
            "d_top_value": "dtv",
            "useable_ace": "ua",
            "draw": "act",
        }
        return r_map[v] if v in r_map else v

    def as_dict(self, add: Dict = {}) -> Dict[str, Union[str, int, float, bool]]:
        """Primarily for pd.DataFrame constructor."""
        t_d = dict(add)
        for attr in self.__annotations__:
            val = getattr(self, attr)
            if hasattr(val, "__annotations__"):
                for subattr in val.__annotations__:
                    if util.should_repr(val, subattr):
                        subval = getattr(val, subattr)
                        t_d[
                            ".".join([self.rename(attr), self.rename(subattr)])
                        ] = util.val_fmt(subval)
            else:
                t_d[self.rename(attr)] = util.val_fmt(val)
        return t_d


TQ = Optional[T]
R = Any  # mdp.learning.R
BackupQ = Any  # mdp.learning.Backup


@dataclass
class SelectA:
    """
    Collection of common policy action-value selection methods.
    `s: S` and `r: Backup` are used to efficiently compute the rolling values
    if available. Else, `ars: List[R]` will be used to re-compute.
    ```python
    best: SelectAλ == Callable[[SQ, BackupQ, List[R]], R]  # max value
    rand: SelectAλ == Callable[[SQ, BackupQ, List[R]], R]  # average value
    ```
    """

    @staticmethod
    def best(s: SQ = None, r: BackupQ = None, ars: List[R] = []) -> R:
        """Max action-value."""
        return r.max_r[s] if s and r else max(ars)

    @staticmethod
    def rand(s: SQ = None, r: BackupQ = None, ars: List[R] = []) -> R:
        """Averaged value, max among those."""
        if s and r:
            max_r, avg_r = r.max_r[s], r.avg_r[s]
            avg_r.a = max_r.a
            return avg_r
        max_r = max(ars)
        max_r.Q = sum(v.Q for v in ars) / len(ars)
        return max_r


SelectAQ = Optional[SelectA]
SelectAλ = Callable[[SQ, BackupQ, List[R]], R]
Behavior = Callable[
    [S, Optional[Iterator[A]], AnyQ], A
]  # type: ignore # policy behavior


class Status(Enum):
    win = 0
    lose = 1
    play = 2
    tie = 3


StatusQ = Optional[Status]


@dataclass
class Y:
    """
    Float object used for managing value update logic in place.
    ```python
    v: float         # current value
    vp: float = 0.0  # previous value
    t: int = 0       # number of times accessed
    k: int = 0       # number of times changed
    δ: float = 1.0   # distance between v and vp
    ```
    """

    v: float
    vp: float = 0.0
    t: int = 0  # number of times accessed
    k: int = 0  # number of times changed
    δ: float = field(default=0.0, repr=False)
    _v: float = field(default=0.0, init=False, repr=False)

    def __repr__(self, d=0) -> str:
        return f"Y({util.number_repr(self)})"

    def __gt__(self, o: "Y") -> bool:
        return self.v > o.v

    def __eq__(self, o: "Y") -> bool:
        try:
            return self.v == o.v
        except:
            return False

    def __bool__(self) -> bool:
        return bool(self.v or self.vp or self.t or self.k)

    @property
    def v(self) -> float:
        return self._v

    @v.setter
    def v(self, v: float) -> None:
        self.vp = self._v
        self._v = v
        self.t += 1
        if self._v != self.vp:
            self.k += 1
            self.δ = abs(self._v - self.vp)
        else:
            self.δ = 0.0

    def __add__(self, o) -> Tuple[Any, bool]:
        """used to iterate the values in place"""
        if o:
            self.v = o.v
        return self
