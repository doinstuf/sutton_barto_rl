"""Type aliases and definitions. Kind of like a header file."""
from typing import (
    Any,
    Callable,
    DefaultDict,
    Deque,
    Dict,
    Generator,
    Iterator,
    List,
    Optional,
    Set,
    Tuple,
    Union,
)

AnyQ = Union[Any, None]
BoolQ = Union[bool, None]
CallableQ = Union[Callable, None]
DataFrame = Any
DDict = DefaultDict
DDictQ = Union[DefaultDict, None]
DictQ = Union[Dict, None]
FloatQ = Union[float, None]
IntQ = Union[int, None]
ListQ = Union[List, None]
Num = Union[float, int]
NumQ = Union[float, int, None]
Series = Any
TupleQ = Union[Tuple, None]
