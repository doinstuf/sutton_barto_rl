"""Monte Carlo DataFrame"""
from dataclasses import dataclass, field
from collections import defaultdict

from mdp.alias import Any, Dict, List
from mdp.learning import R, Backup
from mdp.unit import S, A, T
from mdp.util import pd, np


@dataclass
class RowT:
    """
    ## Init
    ```python
    ep: int   # episode number
    step: T   # mdp.unit.T step
    r: R      # mdp.learning.R return
    pol: str  # policy name e.g. "target"
    ```
    """

    ep: int
    step: T
    r: R
    pol: str
    t: int = field(init=False)
    s: S = field(init=False)
    sn: S = field(init=False)
    a: A = field(init=False)

    def __post_init__(self):
        t = self.step
        self.t, self.s, self.sn, self.a = t.t, t.s, t.sn, t.a

    def to_dict(self) -> Dict[str, Any]:
        return {a: getattr(self, a) for a in self.__annotations__}


l, fl = ["last"], ["first", "last"]
keys = ["trj", "t", "s", "a", "G", "p(trj|s0)"]
aggs = [l, fl, fl, fl, l, l, l, l]
ep_agg = lambda: {k: a for k, a in zip(keys, aggs)}


@dataclass
class Cols:
    """Common column groups."""

    ep: List[str] = field(default_factory=lambda: ["pol", "ep"])
    row: List[str] = field(default_factory=lambda: ["pol", "ep", "t"])
    ep_s: List[str] = field(default_factory=lambda: ["pol", "ep", "s"])
    s0: List[str] = field(default_factory=lambda: ["pol", "s0"])
    trj: List[str] = field(default_factory=lambda: ["pol", "ep", "s", "a"])
    s_trj: List[str] = field(default_factory=lambda: ["s", "trj", "pol"])
    prob: List[str] = field(default_factory=lambda: ["p(a|s)", "p(sn|s,a)"])
    isr: List[str] = field(default_factory=lambda: ["trj", "G", "p(trj|s0)"])
    order: List[str] = field(
        default_factory=lambda: [
            *["pol", "trj", "ep", "t", "tn", "s0", "s", "a"],
            *["G", "p(a|s)", "p(sn|s,a)", "p(trj|s0)"],
        ]
    )
    isr_order: List[str] = field(
        default_factory=lambda: [
            *["trj", "p(trj|s0)_tgt", "p(trj|s0)_bhv", "isr"],
            *["G_tgt", "G_bhv", "G_bhv_w", "G_bhv_mse", "G_bhv_mse_w"],
        ]
    )
    ep_agg: Dict[str, List[str]] = field(default_factory=ep_agg)


@dataclass
class EpisodeDF:
    """
    Builds a pandas dataframe of steps in episode(s).

    ## Init
    ```python
    rows: List[RowT] = []
    df: pd.DataFrame = None
    col: Cols = Cols()
    bd: Dict[str, Backup] = {}  # {RowT.pol: Backup}
    ```

    ## Usage
    ```python
    with EpisodeDF() as edf:
        for ep in range(10):
            for step in episode:
                agent.π.v.r += step
                edf += RowT(ep=ep, t=step, r=agent.π.v.r.G, pol=agent.π.name)
    ```
    """

    rows: List[RowT] = field(default_factory=list, repr=False)
    df: pd.DataFrame = field(default=None, repr=False)
    col: Cols = field(default_factory=lambda: Cols(), repr=True)
    bd: Dict[str, Backup] = field(default_factory=dict, repr=True)

    @property
    def first_t(self) -> pd.DataFrame:
        "First step per episode."
        return self.df.where(lambda row: row["t"] == 0)

    @property
    def last_t(self) -> pd.DataFrame:
        "Last step per episode."
        return self.df.where(lambda row: row["tn"] == row["t"])

    def __enter__(self):
        self.on_enter()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.on_exit()

    def __add__(self, o: RowT):
        self.rows.append(o)
        return self

    def on_enter(self):
        pass

    def on_exit(self):
        df = pd.DataFrame([r.to_dict() for r in self.rows])
        df = df.sort_values(by=self.col.row).reset_index(drop=True)
        df = self.cols(df)
        self.df = df

    def cols(self, df: pd.DataFrame) -> pd.DataFrame:
        """Row/step level column additions."""
        col = self.col

        df["tn"] = df[col.row].groupby(col.ep).transform("max")
        df["s0"] = df[col.ep_s].groupby(col.ep).transform("first")
        df["trj"] = df.merge(self.trajectory(df), on=col.ep, how="left")["trj"]
        df["G"] = [r.G for r in df["r"]]
        df = self.bd_prob(df)
        df = self.trj_prob(df)[col.order]

        return df

    def trajectory(self, df: pd.DataFrame) -> pd.DataFrame:
        "Column of ids uniquely defining the episode trajectory."
        col = self.col
        df = df[col.trj].groupby(col.ep).agg(lambda x: tuple(x))
        df["trj"] = [hash(tuple(x)) for x in df[["s", "a"]].values]
        return df.drop(columns=["s", "a"])

    def bd_prob(self, df: pd.DataFrame) -> pd.DataFrame:
        """Populates df with action and next state probabilities from backup diagram."""
        bd = self.bd
        psa = lambda x, bd: bd[x["pol"]].psa(x["s"], x["a"])
        psn = lambda x, bd: bd[x["pol"]].psn(x["s"], x["a"], x["sn"])

        df["p(a|s)"] = df.apply(lambda x: psa(x, bd), axis=1)
        df["p(sn|s,a)"] = df.apply(lambda x: psn(x, bd), axis=1)

        return df

    def trj_prob(self, df: pd.DataFrame) -> pd.DataFrame:
        "Eq. 5.4 p. 104 - trajectory probability, cumulative per step in episode."
        col = self.col

        tmp = df.loc[:, col.ep + col.prob]
        tmp["p(trj|s0)"] = tmp[col.prob[0]] * tmp[col.prob[1]]
        tmp = tmp.groupby(col.ep)["p(trj|s0)"].agg("cumprod")
        df["p(trj|s0)"] = tmp.values

        return df

    def importance_sampling(
        self, bhv: str = "behavior", tgt: str = "target"
    ) -> pd.DataFrame:
        """Imp. sampling ratio, weighted return, and error calc for given policies."""

        # helpers
        rpl = lambda l: l.replace(bhv, "bhv").replace(tgt, "tgt")
        fmt = lambda l0, l1: f"{l0}_{rpl(l1)}".strip("_")
        bhv_nan = lambda x: pd.notnull(x[("ep", "target")])
        mse = lambda x, y: np.sqrt(np.square(x - y))

        # variables
        p = "p(trj|s0)"
        col, df = self.col, self.df
        agg_drop = ["trj", "p(trj|s0)_bhv", "p(trj|s0)_tgt"]
        g, ep = "G", "ep"
        suf = ["_tgt", "_bhv", "_isr", "_isw"]
        cols = [ep + s for s in suf if s not in suf[-2:]] + [g + s for s in suf]
        agg1 = {"p(trj|s0)": "first", "ep": "count", "G": "sum"}
        agg2 = {c: "sum" for c in cols}
        agg2.update({"isr": "first"})

        # probabilities and avg return by state-trajectory
        sdf = df.groupby(col.s_trj).agg(agg1).unstack().where(lambda x: bhv_nan(x))
        sdf = sdf.dropna()
        isr = sdf[(p, tgt)] / sdf[(p, bhv)]
        sdf["G_isr"] = sdf[("G", bhv)] * isr
        sdf["G_isw"] = sdf["G_isr"]  # placeholder
        sdf["isr"] = isr
        sdf.columns = [fmt(l0, l1) for l0, l1 in sdf.columns]

        # weighted return per state
        sdf = sdf.reset_index().drop(columns=agg_drop)
        sdf = sdf.groupby("s").agg(agg2)
        sdf["G_tgt"] = sdf["G_tgt"] / sdf["ep_tgt"]
        sdf["G_bhv"] = sdf["G_bhv"] / sdf["ep_bhv"]
        sdf["G_isr"] = sdf["G_isr"] / sdf["ep_bhv"]
        sdf["G_isw"] = sdf["G_isr"] / sdf["isr"]

        # error metrics
        sdf["G_bhv_mse"] = mse(sdf["G_bhv"], sdf["G_tgt"])
        sdf["G_isr_mse"] = mse(sdf["G_isr"], sdf["G_tgt"])
        sdf["G_isw_mse"] = mse(sdf["G_isw"], sdf["G_tgt"])

        # format
        sdf["episodes"] = df.ep.unique().size
        sdf["ep_tgt"] = sdf["ep_tgt"].astype(int)
        sdf["ep_bhv"] = sdf["ep_bhv"].astype(int)
        sdf = sdf.round(6)

        return sdf
