import seaborn as sns


def non_zero(s=None, r=None, ars=None):
    """Use best smallest non-zero stake action-value."""
    from mdp.learning import R
    from gambler import Stake

    non_zero = [a for a in ars if a.a.amount != 0]
    if non_zero:
        best = max(non_zero)
        ties = [a for a in non_zero if a.a.amount == best.a.amount]
        if ties:
            return min(ties, key=lambda x: x.a.amount)
        return best
    return R(a=Stake(1), Q=0.0)


def rand_non_zero(s=None, r=None, ars=None):
    """Use max non-zero stake action-value."""
    from mdp.unit import SelectA
    from mdp.learning import R
    from gambler import Stake

    non_zero = [a for a in ars if a.a.amount != 0]
    if non_zero:
        return SelectA.rand(non_zero)
    return R(a=Stake(1), Q=0.0)


def default_policy():
    import numpy as np
    from gambler import Capital, Stake
    from mdp.framework import Policy
    from mdp.unit import Y, Strategy, S
    from mdp.learning import V, R, Backup
    from typing import List
    from random import choice

    def select(s: S = None, r: Backup = None, ars: List[R] = None) -> R:
        """Use best smallest non-zero stake action-value."""
        if ars is None:
            if r:
                ars = r.ars(s)
            else:
                raise Exception
        non_zero = [a for a in ars if a.a.amount != 0]
        if non_zero:
            best = max(non_zero)
            ties = [a for a in non_zero if a.a.amount == best.a.amount]
            if ties:
                return min(ties, key=lambda x: x.a.amount)
            return best
        return R(a=Stake(1), Q=0.0)

    v_init = {Capital(100): Y(v=1.0, vp=1.0)}
    return Policy(
        name="smallest_optimal",
        select=select,
        strategy=Strategy.off,
        v=V(v_init=v_init, γ=1.0),
        require_stable=True,
    )


def plot_policy(agent):
    from matplotlib.pyplot import show

    policy = sorted(agent.π.target.items())  # sort by state
    values = [x[1].amount for x in policy]
    indices = [i for i in range(len(values))]
    ax = sns.lineplot(y=values, x=indices)
    ax.set(xlabel="Capital", ylabel="Stake")
    show()


def plot_values(agent):
    import pandas as pd
    from matplotlib.pyplot import legend, show
    from mdp import util

    df = util.Plot.vdf(agent.π.v.swp)
    df["state"] = df["state"].apply(lambda x: x.cap)
    df = df.where(lambda x: x["plot"])
    ax = sns.lineplot(y="value_estimate", x="state", hue="iteration", data=df)
    legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.0)
    show()


def gamble(agent, pause=1.0):
    from time import sleep
    from gambler import Capital
    from mdp.unit import Status

    agent.reset(Capital(20))
    print(agent.env.s.terminal())
    while agent.env.s.terminal() == Status.play:
        print(agent)
        _ = agent.act()
        sleep(pause)
    print(agent)
    print(agent.env.s.terminal())
    agent.reset()
    return agent


def episode(agent):
    """Episode for Gambler agent."""
    from mdp.unit import Status
    from gambler import Capital
    from random import randint

    steps = []
    while agent.env.s.terminal() == Status.play:
        steps.append(agent.act())
    steps.append(agent.act())  # terminal state
    agent.reset(s=Capital(randint(agent.env.s.min + 1, agent.env.s.max - 1)))
    return steps
