from dataclasses import dataclass, field
from random import choice, random

import numpy as np

import chapter04.util as c4util

from mdp import util
from mdp.alias import *
from mdp.framework import Agent, Environment, Policy
from mdp.learning import V, Algorithms
from mdp.unit import A, S, SelectA, Strategy, T, Status, Y


@dataclass
class Coin:
    """
    ```python
    weight: float = 0.4
    heads: bool = True
    ```
    """

    weight: float = field(default=0.4, repr=False)
    heads: bool = True

    def flip(self, simulate: int = 0) -> List[bool]:  # type: ignore # from alias
        flip_n = lambda n: [random() < self.weight for _ in range(n)]
        if simulate:
            return flip_n(simulate)
        else:
            flip = flip_n(1)
            self.heads = flip[0]
            return flip


CoinQ = Optional[Coin]  # type: ignore # from alias


@dataclass(frozen=True)
class Capital(S):
    """
    State
    ```python
    cap: int
    max: int = 100
    min: int = 0
    ```
    """

    name: str = field(init=False, repr=False)
    cap: int
    max: int = field(default=100, repr=False)
    min: int = field(default=0, repr=False)

    def __eq__(self, o) -> bool:
        return self.cap == o.cap

    def __hash__(self) -> int:
        return self.cap

    def __lt__(self, o: S) -> bool:
        return self.cap < o.cap

    def __gt__(self, o: S) -> bool:
        return self.cap > o.cap

    def __add__(self, o: S) -> S:
        cap = max(min(self.cap + o.cap, self.max), self.min)
        return Capital(cap, self.max, self.min)

    def __sub__(self, o: S) -> S:
        cap = max(min(self.cap - o.cap, self.max), self.min)
        return Capital(cap, self.max, self.min)

    def terminal(self) -> Status:
        if self.cap >= self.max:
            return Status.win
        elif self.cap <= self.min:
            return Status.lose
        else:
            return Status.play


CapitalQ = Optional[Capital]  # type: ignore # from alias


@dataclass(frozen=True)
class Stake(A):
    """
    Action
    ```python
    amount: int
    heads: bool = True
    ```
    """

    name: str = field(init=False, repr=False)
    amount: int
    heads: bool = field(default=True, repr=False)

    def __eq__(self, o) -> bool:
        if isinstance(o, type(self)):
            return self.amount == o.amount
        return False

    def __hash__(self) -> int:
        return self.amount

    def __gt__(self, o):
        return self.amount > o.amount

    def __lt__(self, o):
        return self.amount < o.amount


StakeQ = Optional[Stake]  # type: ignore # from alias


@dataclass
class CoinFlips(Environment):
    t: int = 0
    s: Capital = field(default_factory=lambda: Capital(cap=20))
    coin: Coin = Coin()

    def next_state(self, s: S, a: A, heads: bool) -> S:
        """The next state is either capital + or - the stake, depending on coin flip heads or tails."""
        result = lambda: (s.cap + a.amount) if heads == a.heads else (s.cap - a.amount)
        return Capital(result(), s.max, s.min)

    def on_action(self, a: Stake) -> Capital:
        """Called by `act()`. This gets overwritten by inheritting class."""
        return self.next_state(self.s, a, self.coin.flip()[0])

    def all_sn(self, s: Capital) -> List[S]:  # type: ignore # from alias
        return [Capital(cap=x) for x in range(self.s.min + 1, self.s.max)]


@dataclass
class Gambler(Agent):

    π: Policy = field(default_factory=c4util.default_policy, repr=False)

    @staticmethod
    def all_a(s: Capital) -> List[Stake]:  # type: ignore # from alias
        min_stake, max_stake = s.min, min(s.cap, s.max - s.cap)
        heads, tails = (  # type: ignore
            [Stake(i, True) for i in range(min_stake, max_stake + 1)],
            [],
        )
        return heads + tails

    def calc_reward(self, s: Capital, sn: Capital) -> int:
        if s.terminal() != Status.play and sn.terminal() != Status.play:
            return 0  # after end of episode
        return 1 if sn.terminal() == Status.win else 0

    def all_t(self, a: Stake) -> List[T]:  # type: ignore # from alias
        r, s, t, p = self.r, self.env.s, self.env.t, self.env.coin.weight
        heads, ph = self.env.next_state(s, a, True), p
        tails, pt = self.env.next_state(s, a, False), 1 - p
        rh, rt = self.calc_reward(s, heads), self.calc_reward(s, tails)

        # generate duplicate output steps to represent the probability
        step_heads = [T(t, s, a, r, heads, rh) for _ in range(int(ph * 10))]
        step_tails = [T(t, s, a, r, tails, rt) for _ in range(int(pt * 10))]
        return step_heads + step_tails


if __name__ == "__main__":
    from pprint import pprint
    import pandas as pd
    import seaborn as sns
    from matplotlib.pyplot import show

    print("\nValue function...")
    agent = Gambler(CoinFlips(s=Capital(cap=20)))
    states = agent.env.all_sn(agent.env.s)
    v = Algorithms.value_iteration(agent, states)
    c4util.plot_values(agent)
    c4util.plot_policy(agent)

    print("\nTrained free play...")
    agent.π.strategy = Strategy.on
    steps = c4util.episode(agent)
    pprint(steps)

    print("\nMonte Carlo first state")
    agent = Gambler(CoinFlips(s=Capital(cap=20)))
    agent.π.v = V(γ=1.0, v_init={})  # {Capital(100): Y(v=1.0, vp=1.0)}
    Q = Algorithms.mc_first_visit(
        episode=c4util.episode, agent=agent, kwargs={}, n=10_000
    )
    pprint({s: f"{r.Q:.3f}" for s, r in Q.items()})

    print("\nMonte Carlo exploratory starts")
    agent = Gambler(CoinFlips(s=Capital(cap=20)))
    agent.π.v = V(γ=1.0, v_init={})  # {Capital(100): Y(v=1.0, vp=1.0)}
    # agent.π.strategy = Strategy.on
    aQ = Algorithms.mc_exploring_starts(
        episode=c4util.episode, agent=agent, kwargs={}, n=10_000
    )
    policy_table = {s: (r.a, r.Q) for s, r in agent.π.v.r.max_r.items()}
    pprint(policy_table)
    c4util.plot_policy(agent)

    print("\nEpisodic Value Iteration")
    agent = Gambler(
        CoinFlips(), π=Policy(v=V(γ=1.0, v_init={Capital(100): Y(v=1.0, vp=1.0)}))
    )
    v = Algorithms.episodic_value_iteration(
        episode=c4util.episode, agent=agent, kwargs={}, n=10_000
    )
    df = pd.DataFrame(agent.π.v.swp)
    df["capital"] = df["state"].apply(lambda x: x.cap)
    ax = sns.lineplot(y="value_estimate", x="capital", hue="episode", data=df)
    _ = ax.set_title("Episodic Value Iteration")
    show()
    c4util.plot_policy(agent)
