from card import *


class TestCard:
    def test_card_hash(self):
        test = len(set(deck()))
        correct = len(["C", "H", "D", "S"]) * len(
            [2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K", "A"]
        )
        assert test == correct

        d1, d2 = Deck(deal_method="replace"), Deck(deal_method="remove")
        d1.shuffle()
        assert set(d1.cards) == set(d2.cards)

        _ = d1.deal()
        assert set(d1.cards) == set(d2.cards)
        _ = d2.deal()
        assert set(d1.cards) != set(d2.cards)
        print(  # deck by suit to see which card was dealt
            "\n\n".join(
                f"{k.value}: {v}"
                for k, v in {
                    c.suit: [c_in for c_in in sorted(d2.cards) if c_in.suit == c.suit]
                    for c in d2.cards
                }.items()
            )
        )
        d2.add()
        assert set(d1.cards) == set(d2.cards)
