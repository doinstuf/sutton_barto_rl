from collections import deque
from copy import copy
from dataclasses import dataclass, field
from enum import Enum
from operator import xor
from random import choice, shuffle
from typing import Any, Dict, List, Optional, Set, Tuple, Union, Deque

from numpy.random import choice as np_choice

from mdp.util import nested_xor

AnyQ, DictQ, TupleQ, ListQ = (
    Optional[Any],
    Optional[Dict],
    Optional[Tuple],
    Optional[List],
)


class Rank(Enum):
    """n2: number two, fj = face jack"""

    n2 = "2"
    n3 = "3"
    n4 = "4"
    n5 = "5"
    n6 = "6"
    n7 = "7"
    n8 = "8"
    n9 = "9"
    nx = "X"
    fj = "J"
    fq = "Q"
    fk = "K"
    fa = "A"

    def __repr__(self) -> str:
        return f"{self.value}"


class Suit(Enum):
    bs = "♠"
    bc = "♣"
    rh = "♡"
    rd = "♢"

    def __repr__(self) -> str:
        return f"{self.value}"


card_properties: Dict[Rank, int] = {
    Rank.n2: 2,
    Rank.n3: 3,
    Rank.n4: 4,
    Rank.n5: 5,
    Rank.n6: 6,
    Rank.n7: 7,
    Rank.n8: 8,
    Rank.n9: 9,
    Rank.nx: 10,
    Rank.fj: 10,
    Rank.fq: 10,
    Rank.fk: 10,
    Rank.fa: 11,
}


@dataclass(frozen=True, unsafe_hash=True, eq=True)
class Card:
    """
    ```python
    suit: Suit
    rank: Rank
    v: int
    ```
    """

    suit: Suit
    rank: Rank
    v: int = field(init=False, repr=False)

    def __post_init__(self):
        self.__dict__["v"] = card_properties[self.rank]

    def __gt__(self, o) -> bool:
        return self.v > o.v

    def __lt__(self, o) -> bool:
        return self.v < o.v


CardQ = Optional[Card]


deck_cache = [Card(s, r) for s in Suit for r in Rank]
endless_cards: Deque[Card] = deque(np_choice(deck_cache, size=10 ** 6), maxlen=10 ** 6)
random_card = lambda dq: (dq.rotate(), dq[0])[1]


def deck(n: int = 52) -> List[Card]:
    new_deck = copy(deck_cache)
    shuffle(new_deck)
    return new_deck[:n]


@dataclass
class Deck:
    cards: List[Card] = field(default_factory=deck, repr=False)
    deal_method: str = field(default="replace", repr=True)  # 'remove'
    _reference: List[Card] = field(default_factory=deck, repr=False)

    def __eq__(self, o) -> bool:
        """Decks are equal if they have the same cards in the same order."""
        try:
            return self.cards == o.cards
        except:
            return False

    def __hash__(self) -> int:
        return nested_xor([hash(c) for c in self.cards])

    def deal(self) -> Card:
        """Draw 1 card from the deck"""
        if self.deal_method == "replace":
            return random_card(endless_cards)
        if self.deal_method == "remove":
            if self.cards:
                return self.cards.pop()
            else:
                self.cards = deck()
                return self.cards.pop()
        else:
            raise NotImplementedError

    def shuffle(self) -> None:
        """Replace the entire deck with a new shuffled deck."""
        shuffle(self.cards)

    def add(self, card: CardQ = None) -> None:
        """Adds a single card to the deck. Useful for repeated simulation."""
        if card:
            assert card not in self.cards
        else:
            card = choice(list(set(self._reference) - set(self.cards)))
        self.cards.append(card)
        self.shuffle()


if __name__ == "__main__":
    from pprint import pprint

    d = Deck()
    pprint(sorted(d.cards))
    print(d)
    print(len(d.cards))
