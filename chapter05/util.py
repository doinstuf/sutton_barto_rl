from enum import Enum
from random import choice
from typing import List, Tuple, Any, Dict, DefaultDict as DDict

from mdp.util import pd, np
from tqdm import tqdm

from mdp.learning import V
from mdp.unit import SelectA, T, Strategy
from mdp.util import pd
from mdp.framework import Policy

Agent, S, R, A = Any, Any, Any, Any


class Draw(Enum):
    hit = 1
    stay = 0

    def __repr__(self) -> str:
        return f"{self.name[0]}"


class AgentT(Enum):
    dealer = 0
    player = 1

    def __repr__(self) -> str:
        return f"{self.name[0]}"


def deal_hands(deck: Any) -> Tuple[List[int], List[int]]:
    d_hand, p_hand = [], []
    for c in range(4):
        if c % 2 == 0:
            d_hand.append(deck.deal().v)
        else:
            p_hand.append(deck.deal().v)
    return d_hand, p_hand


dealer_behavior = lambda s, actions: actions[0] if s.d_value < 17 else actions[1]
dealer_policy_object = Policy(
    name="hit below 17", behavior=dealer_behavior, strategy=Strategy.off, v=V(γ=1.0)
)
dealer_policy = lambda: dealer_policy_object


default_behavior = lambda s, actions: actions[0] if s.p_value < 20 else actions[1]


def default_policy(γ: float = 1.0, strategy=Strategy.off):
    return Policy(
        name="hit below 20",
        select=SelectA.best,
        behavior=default_behavior,
        strategy=strategy,
        v=V(γ=γ),
    )


random_behavior = (
    lambda s, actions: actions[0]
    if s.p_value <= 11
    else (actions[1] if s.p_value >= 21 else choice(actions))
)


def random_policy(γ: float = 1.0, strategy=Strategy.off, name="random above 11"):
    return Policy(
        name=name,
        select=SelectA.rand,
        behavior=random_behavior,
        strategy=strategy,
        v=V(γ=γ),
    )


true_random_behavior = lambda s, actions: choice(actions)


def true_random(γ: float = 1.0, strategy=Strategy.off, name="all random"):
    return Policy(
        name=name,
        select=SelectA.rand,
        behavior=true_random_behavior,
        strategy=strategy,
        v=V(γ=γ),
    )


random_argmax_behavior = (
    lambda s, actions: actions[0]
    if s.p_value <= 11
    else (actions[1] if s.p_value >= 21 else choice(actions))
)
random_argmax_policy = Policy(
    name="hit below 20",
    select=SelectA.best,
    behavior=random_argmax_behavior,
    strategy=Strategy.off,
    v=V(γ=1.0),
)
random_argmax = lambda: random_argmax_policy


from mdp.mcdf import EpisodeDF, RowT


def example_5_4(max_ep: int = 100_000) -> Tuple[pd.DataFrame, List[EpisodeDF]]:
    """Importance sampling using blackjack problem."""
    from chapter05.blackjack import Player, Table
    from chapter05.optimal_policy import target

    # helpers
    episode = lambda agent: reversed(blackjack_episode(agent))
    its = lambda n, m: np.logspace(np.log10(100), np.log10(n), m).astype(int)

    def run(n: int) -> EpisodeDF:
        tgt_π = Policy(name="target", strategy=Strategy.on, v=V(γ=1.0), target=target())
        b_agent = Player(env=Table(), π=random_policy(γ=1.0, name="behavior"))
        t_agent = Player(env=Table(), π=tgt_π)
        agents = [b_agent, t_agent]

        with EpisodeDF() as ep_df:
            for agent in agents:
                r, pol = agent.π.v.r, agent.π.name
                for ep in tqdm(range(n)):
                    for t in episode(agent):
                        r += t
                        ep_df += RowT(ep=ep, step=t, r=r.G, pol=pol)
                ep_df.bd.update({pol: r})

        return ep_df

    # run episodes
    spaces = np.ceil(2 * (np.log10(max_ep) - 1)).astype(int)
    iterations = its(max_ep, spaces)
    dl: List[pd.DataFrame] = []

    print(f"episodes per iteration: {', '.join([str(i) for i in iterations])}")
    for i in iterations:
        ep_df = run(i)
        dl.append(ep_df)

    # isr results for plotting
    rn = {"G_isr_mse": "ordinary", "G_isw_mse": "weighted", "G_bhv_mse": "none"}
    plotdf = pd.concat([d.importance_sampling() for d in dl]).reset_index()
    plotdf = plotdf.melt(
        id_vars="episodes", value_vars=list(rn), var_name="method", value_name="mse"
    )
    plotdf.episodes = [f"{ep:,}" for ep in plotdf.episodes.round(-2)]
    plotdf.method = [rn[m] for m in plotdf.method]

    return plotdf, dl


def validate_episode(player: Any, dealer: Any, player_steps: List[T]):
    assert player.env is dealer.env
    assert player.env.s is dealer.env.s
    assert len(player_steps) > 0
    assert len(player_steps) == len(set(player_steps))
    assert sum([abs(s.r) for s in player_steps]) <= 1
    assert sum([abs(s.rn) for s in player_steps]) <= 1
    assert player_steps[-1].t == len(player_steps) - 1


player_step = lambda t, ep, pt: (ep.append(t), pt.append(t))


def blackjack_episode(
    agent: Any,
    pause: int = 0,
    verbose: int = 0,
    reset: bool = True,
    player_only: bool = True,
) -> List[T]:
    """Generates a complete, independent episode List[T]"""
    from chapter05.blackjack import Player, AgentT

    if pause:
        from time import sleep

    if reset and agent.env.t > 0:
        agent.reset()

    player, π, env = agent, agent.π, agent.env
    dealer = Player(env, role=AgentT.dealer, π=dealer_policy())
    episode: List[T] = []
    player_steps: List[T] = []

    if verbose > 1:
        print(f"Policy: {π}")

    # player's turn
    while player.active() and player.playing(env.hands):
        player_step(player.act(), episode, player_steps)

    # dealer's turn
    while dealer.playing(env.hands) and dealer.active():
        episode.append(dealer.act())

    player_step(player.act(), episode, player_steps)

    # the status state will get marked as duplicate visit, but we need the reward
    if len(player_steps) >= 2 and player_steps[-1].s == player_steps[-2].s:
        l, nl = player_steps[-1], player_steps[-2]
        player_steps[-2] = T(nl.t, nl.s, nl.a, nl.r, nl.sn, l.rn)
        _ = player_steps.pop()
    if player_steps[-1].s.p_value > 21:
        _ = player_steps.pop()

    if 1 < verbose:
        print(",\n".join([str(e) for e in episode]))
        validate_episode(player, dealer, player_steps)

    if verbose:
        reward = episode[-1].rn if episode[-1].rn else episode[-1].r
        print(
            f"Result: {env.hands.status} Reward: {reward}\n(Player: {player.hand_value()} vs Dealer: {dealer.hand_value()})\n"
        )
    if pause:
        sleep(pause)

    return player_steps if player_only else episode


def plot_values(
    agent: Any,
    Q: Dict[Any, Any],
    ace: bool,
    values: bool = False,
    s: Tuple[int, int] = (18, 7),
    max_v: int = 21,
    min_v: int = 12,
    f: int = 2,
    updates: bool = True,
    show: bool = True,
    ax_c: int = 1,
) -> Tuple[pd.DataFrame, Dict[str, Dict[int, int]], Any]:
    import seaborn as sns
    import matplotlib.pyplot as plt

    sns.set(style="whitegrid")
    tqdm.pandas()

    # build dataframe
    df = pd.DataFrame({"state": list(Q.keys()), "returns": list(Q.values())})

    df["useable_ace"] = df["state"].apply(lambda x: x.useable_ace)
    df = df.where(lambda x: x["useable_ace"] == ace)

    df["player_sum"] = df["state"].apply(lambda x: x.p_value)
    df = df.where(lambda x: x["player_sum"] <= max_v)
    df = df.where(lambda x: x["player_sum"] >= min_v)

    df["dealer_showing"] = df["state"].apply(lambda x: x.d_top_value)
    df["Q"] = (
        df["returns"].apply(lambda x: x.υ.v)
        if values
        else df["returns"].apply(lambda x: x.Q)
    )
    df["updates"] = df["returns"].apply(lambda x: x.n)
    df_ace = df.reset_index()

    # mean value
    dealer_vals = sorted(df_ace["dealer_showing"].unique())
    player_vals = sorted(df_ace["player_sum"].unique())
    len_d, len_p = len(dealer_vals), len(player_vals)
    vals_matrix = np.full((len_d, len_p), np.nan)
    updates_matrix = np.full((len_d, len_p), np.nan)
    accesses_matrix = np.full((len_d, len_p), np.nan)
    mapping = {
        "d": {v: i for v, i in zip(dealer_vals, range(len_d))},
        "p": {v: i for v, i in zip(player_vals, range(len_p))},
    }

    # build 2d array
    for i in range(len(df_ace)):
        ind_d = mapping["d"][df_ace["dealer_showing"][i]]
        ind_p = mapping["p"][df_ace["player_sum"][i]]
        vals_matrix[ind_d, ind_p] = df_ace["Q"][i]
        updates_matrix[ind_d, ind_p] = df_ace["updates"][i]

    # plot
    fig, ax = plt.subplots(figsize=s, ncols=ax_c, sharey=False)
    if ax_c > 1:
        ax1, ax2 = ax[0], ax[1]
    else:
        ax1, ax2 = ax, None
    _ = sns.heatmap(
        vals_matrix,
        annot=True,
        fmt=f".{f}f",
        square=True,
        ax=ax1,
        xticklabels=mapping["p"],
        yticklabels=mapping["d"],
    )
    ax1.set_xlabel("Player Sum")
    ax1.set_ylabel("Dealer Showing")
    π = agent.π.name if agent.π.name else f"{agent.π.strategy}"
    last_iter = agent.π.v.t
    ax1.set_title(
        f"Blackjack State Value (useable ace: {ace})\nPolicy under evaluation: {π}\nEpisodes: {last_iter:,}"
    )

    # plot
    if np.nansum(updates_matrix) > 0:
        fig, ax = plt.subplots(figsize=s)
        ax = sns.heatmap(
            updates_matrix,
            annot=True,
            fmt=f".{0}f",
            square=True,
            ax=ax,
            xticklabels=mapping["p"],
            yticklabels=mapping["d"],
        )
        ax.set_xlabel("Player Sum")
        ax.set_ylabel("Dealer Showing")
        π = agent.π.name if agent.π.name else f"{agent.π.strategy}"
        title = f"State Value Updates (useable ace: {ace})"
        π = f"Policy under evaluation: {π}"
        iterations = f"Episodes: {last_iter:,}, Total Updates"
        updates_cnt = f"{np.nansum(updates_matrix):.0f}"
        ax.set_title(f"{title}\n{π}\n{iterations}: {updates_cnt}")

    if show:
        plt.show()
    return df_ace, mapping, ax2


def plot_policy(
    agent: Any,
    Q: Dict[Any, Any],
    ace: bool,
    values: bool = False,
    s: Tuple[int, int] = (18, 7),
    max_v: int = 21,
    min_v: int = 12,
    f: int = 1,
) -> None:
    import seaborn as sns
    import numpy as np
    from mdp.util import pd
    import matplotlib.pyplot as plt

    sns.set(style="whitegrid")

    df_ace, mapping, ax2 = plot_values(
        agent, Q, ace, values, s, max_v, min_v, f, show=False, ax_c=2
    )
    π = agent.π.target

    if π:
        # build 2d array
        dealer_vals = sorted(df_ace["dealer_showing"].unique())
        player_vals = sorted(df_ace["player_sum"].unique())
        len_d, len_p = len(dealer_vals), len(player_vals)
        vals_matrix = np.full((len_d, len_p), np.nan)
        for i in range(len(df_ace)):
            ind_d = mapping["d"][df_ace["dealer_showing"][i]]
            ind_p = mapping["p"][df_ace["player_sum"][i]]

            if df_ace["state"][i] in π:
                state = df_ace["state"][i]
                a = π[state]
                draw = int(str(a.draw) == str(Draw.hit))
            else:
                draw = np.nan
            vals_matrix[ind_d, ind_p] = draw

        # plot
        _ = sns.heatmap(
            vals_matrix,
            annot=True,
            fmt=".0f",
            square=True,
            ax=ax2,
            xticklabels=mapping["p"],
            yticklabels=mapping["d"],
        )
        ax2.set_xlabel("Player Sum")
        ax2.set_ylabel("Dealer Showing")
        π = agent.π.name if agent.π.name else f"{agent.π.strategy}"
        ax2.set_title(
            f"Blackjack Learned Policy (useable ace: {ace})\nPolicy under evaluation: {π}\n1 = hit, 0 = stay"
        )
    plt.show()


def format_sav(aQ: Dict[Any, Dict[Any, Any]], ace: bool, n: int = 3) -> str:
    """
    Format and print the top state-action-values. Works with the
    `agent.π.v.r.aQ` object.
    ```python
    aQ: Dict[S, Dict[A, R]]  # state-action-values table
    ace: bool                # ace or no ace
    n: int = 3               # number of action-values
    ```
    """
    return "   State        R     \n" + "\n".join(
        [
            " | ".join(
                (
                    f"p={s.p_value:2}, d={s.d_top_value:2}",
                    ", ".join([f"{i[0].draw.value}: {i[1]:= .3f}" for i in a[:n]]),
                )
            )
            for s, a in sorted(
                [
                    (
                        s,
                        sorted(
                            [(a, r.Q) for a, r in aQ[s].items()],
                            reverse=True,
                            key=lambda a: a[1],
                        ),
                    )
                    for s, ar in aQ.items()
                    if 11 < s.p_value < 21 and s.useable_ace == ace
                ],
                key=lambda s: (s[0].p_value, s[0].d_top_value),
            )
        ]
    )


if __name__ == "__main__":
    from pprint import pprint

    r = example_5_4()
    pprint(r)
