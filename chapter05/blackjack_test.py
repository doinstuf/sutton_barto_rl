from itertools import product
from operator import xor
from random import randint
from typing import *

from blackjack import *
from card import *

from mdp.util import nested_xor


m = 1_000


class TestBlackjack:
    @staticmethod
    def test_nested_xor():
        ints: List[int] = [randint(0, 100) for _ in range(7)]
        test: int = nested_xor(ints)
        correct: int = xor(
            xor(
                xor(xor(xor(xor(ints[0], ints[1]), ints[2]), ints[3]), ints[4]), ints[5]
            ),
            ints[6],
        )
        assert test == correct

    @staticmethod
    def test_episode():
        agent = Player(env=Table())
        test, correct = [], []
        for i in range(m):
            episode = blackjack_episode(agent, verbose=10)
            test.append(sum([abs(step.rn) for step in episode]))
            correct.append(max([abs(step.rn) for step in episode]))

        # reward
        assert test == correct
        assert sum(test) <= len(test)

    @staticmethod
    def test_state_hash_collisions():
        agent = Player(env=Table())
        test, correct = set(), set()
        for i in range(m):
            episode = blackjack_episode(agent, verbose=10)
            for t in episode:
                test.add(hash(t.s))
                correct.add(t.s)
        assert len(test) == len(correct)

    @staticmethod
    def test_action_hash_collisions():
        agent = Player(env=Table())
        test, correct = set(), set()
        for i in range(m):
            episode = blackjack_episode(agent, verbose=10)
            for t in episode:
                test.add(hash(t.a))
                correct.add(t.a)
        assert len(test) == len(correct)

    @staticmethod
    def test_state_copy():
        """
        Passing state by reference in places can cause
        the history to get updated unintentionally
        """
        for i in range(m):
            episode: List[Step] = []
            env = Table()
            dealer = Player(env, role=AgentT.dealer)
            player = Player(env, role=AgentT.player)
            while player.active() and player.playing(env.hands):
                episode.append(player.act())
            while dealer.playing(env.hands) and dealer.active():
                dealer.act()

            print(i)
            print("\n".join([str(t) for t in episode]))
            print(env.first_s, env.s)

            if any([t.a.draw == Draw.hit for t in episode]):
                assert env.first_s is not env.s
                assert env.first_s != env.s

            assert len(episode) == len(set(episode))
