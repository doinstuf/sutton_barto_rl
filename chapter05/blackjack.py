import sys

sys.path.append("../utils/")
sys.path.append("./utils/")

from copy import copy, deepcopy
from dataclasses import dataclass, field
from functools import lru_cache
from random import choice
from operator import xor

from mdp import util
from mdp.alias import List, Tuple, Optional, Any, IntQ, Num
from mdp.framework import A, AQ, S, SQ, Agent, Environment, Policy
from mdp.unit import Status, StatusQ, T, Strategy, F
from utils.simple_repr import Formatters, SimpleRepr

from chapter05.card import Card, CardQ, Deck, deck
from chapter05.util import (
    Draw,
    AgentT,
    deal_hands,
    default_policy,
    blackjack_episode,
    random_policy,
)


turn_features: List[F] = [F("draw", None, True)]


@dataclass(frozen=True)
class Turn(A):
    """Action
    ```python
    draw: Draw    # Draw.hit or Draw.stay
    role: AgentT  # AgentT.player or AgentT.dealer
    ```
    """

    draw: Draw
    role: AgentT = field(repr=True)
    name: str = field(init=False, repr=False)
    _feat: List[F] = field(default_factory=lambda: turn_features, repr=False)

    @property
    def feat_vals(self) -> Tuple[Num]:
        return (self.draw.value,)

    @property
    def feat_size(self) -> Tuple[Num]:
        return (2,)

    def __eq__(self, o) -> bool:
        """Uniquely defines the learnable properties of state."""
        try:
            return self.draw == o.draw
        except:
            return False

    def __hash__(self) -> int:
        """Uniquely defines the learnable properties of state."""
        return self.draw.value

    def __repr__(self, d: int = 0) -> str:
        f = Formatters(recursor=SimpleRepr.simple_repr, p={str: {"s": 4}})
        return SimpleRepr(self, formatters=f).__repr__(d=d)


visible_features: List[F] = [
    F("p_value", None, False),
    F("d_top_value", None, False),
    F("useable_ace", None, True),
]


@dataclass(frozen=True)
class Visible(S):
    """
    The immutable, visible feature state.
    ```python
    p_value: int      # value of player's hand
    d_value: int      # value of dealer's hand
    d_top_value: int  # value of dealer's top card
    useable_ace: int  # if the player has a useable ace
    ```
    """

    p_value: int
    d_value: int
    d_top_value: int
    useable_ace: int
    name: str = field(init=False, repr=False)
    _feat: List[F] = field(default_factory=lambda: visible_features, repr=False)

    @property
    def feat_vals(self) -> Tuple[Num, Num, Num]:
        return self.p_value, self.d_top_value, self.useable_ace

    @property
    def feat_size(self) -> Tuple[Num, Num, Num]:
        return 31, 13, 2  # p _value in [4, 30]

    def __eq__(self, o) -> bool:
        """Uniquely defines the learnable properties of state."""
        try:
            return (
                self.p_value == o.p_value
                and self.d_top_value == o.d_top_value
                and self.useable_ace == o.useable_ace
            )
        except:
            return False

    def __lt__(self, o) -> bool:
        try:
            attr = ["useable_ace", "p_value", "d_top_value"]
            for a in attr:
                v1, v2 = getattr(self, a), getattr(o, a)
                if v1 < v2:
                    return True
                if v1 > v2:
                    return False
            return False
        except:
            return False

    def __hash__(self) -> int:
        """Uniquely defines the learnable properties of state."""
        return 100 * self.p_value + 10_000 * self.d_top_value + self.useable_ace

    def __repr__(self) -> str:
        return f"Vis(p={self.p_value:2}, dt={self.d_top_value:2}, d={self.d_value:2}, ua={self.useable_ace})"


VisibleQ = Optional[Visible]  # type: ignore


@dataclass
class Hands:
    """
    The mutable state of the table: player and dealer hands.
    ```python
    status: Status = Status.play
    d_turn: Turn = Turn(Draw.hit, AgentT.dealer)
    p_turn: Turn = Turn(Draw.hit, AgentT.player)
    p_hand: List[Card] = []
    last_action: AQ = None
    d_hand: List[Card] = []
    v_max: int = 21
    ```
    """

    status: Status = Status.play
    d_turn: Turn = field(default=Turn(draw=Draw.hit, role=AgentT.dealer), repr=False)
    p_turn: Turn = field(default=Turn(draw=Draw.hit, role=AgentT.player), repr=False)
    p_hand: List[Card] = field(default_factory=list, repr=False)
    last_action: AQ = field(default=None, repr=False)
    d_hand: List[Card] = field(default_factory=list, repr=False)
    v_max: int = field(default=21, repr=False)

    def __repr__(self, d=0) -> str:
        """simple_repr"""
        return SimpleRepr(
            self,
            s=3,
            attrs=["status", "d_top_value", "d_value", "p_value", "useable_ace"],
        ).__repr__(d=d)

    @property
    def d_top_value(self) -> int:
        return self.d_top or 0

    @property
    def d_value(self) -> int:
        return self.calc_value(tuple(self.d_hand))[0]

    @property
    def p_value(self) -> int:
        return self.calc_value(tuple(self.p_hand))[0]

    @property
    def useable_ace(self) -> bool:
        return self.calc_value(tuple(self.p_hand))[1]

    @property
    def risky_hit(self) -> bool:
        """Range where a decision must be made."""
        return 12 <= self.p_value < 21

    @property
    def d_top(self) -> int:
        return self.d_hand[0] if self.d_hand else 0

    def natural_blackjack(self) -> None:
        p, d = self.p_value, self.d_value
        if p == self.v_max and d == self.v_max:
            self.status = Status.tie
        elif p == self.v_max:
            self.status = Status.win
        elif d == self.v_max:
            self.status = Status.lose

    @staticmethod
    def next_status(p: int, d: int, v_max: int, d_draw: Draw, p_draw: Draw) -> StatusQ:
        # player stays and dealer stays
        if d_draw == Draw.stay and p_draw == Draw.stay:
            if p > d:
                return Status.win
            if p < d:
                return Status.lose
            else:
                return Status.tie

        # dealer stays and player blackjack
        if p == v_max and d_draw == Draw.stay:
            return Status.win

        # player stays and dealer blackjack
        if d == v_max and p_draw == Draw.stay:
            return Status.lose

        # player blackjack and dealer blackjack
        if p == v_max and d == v_max:
            return Status.tie

        return None

    def hit(self, a: A, card: IntQ = None) -> VisibleQ:  # type: ignore
        self.last_action = a  # always update for tracking purposes
        dt = self.d_top_value
        if self.status == Status.play:

            # the current player hit
            if card is not None:

                # dealer hits
                if a.role == AgentT.dealer:
                    self.d_turn = a
                    self.d_hand.append(card)
                    d, (p, ua) = (self.d_value, self.calc_value(tuple(self.p_hand)))

                    # dealer busts
                    if d > self.v_max:
                        self.status = Status.win
                        return Visible(p, d, dt, ua)

                # player hits
                if a.role == AgentT.player:
                    self.p_turn = a
                    self.p_hand.append(card)
                    d, (p, ua) = (self.d_value, self.calc_value(tuple(self.p_hand)))

                    # player busts
                    if p > self.v_max:
                        self.status = Status.lose
                        return Visible(p, d, dt, ua)
            else:
                d, (p, ua) = (self.d_value, self.calc_value(tuple(self.p_hand)))
                if a.role == AgentT.player:
                    self.p_turn = a
                else:
                    self.d_turn = a

            status = self.next_status(
                p, d, self.v_max, self.d_turn.draw, self.p_turn.draw
            )
            self.status = status if status is not None else self.status

            return Visible(p, d, dt, ua)

        # no changes to status so no changes to state
        else:
            return None

    @staticmethod
    @lru_cache(maxsize=None)
    def calc_value(hand: Tuple[int, ...]) -> Tuple[int, int]:
        """Highest possible hand value that doesn't exceed v_max"""
        ace_count, total, v_max = 0, 0, 21
        for c in hand:
            ace_count, total = ace_count + (c == 11), total + c
            if ace_count and total > v_max:
                ace_count, total = ace_count - 1, total - 10
        return total, min(1, ace_count)


HandsQ = Optional[Hands]


@dataclass
class Table(Environment):
    s: Visible = field(init=False, repr=True)
    _hands: HandsQ = field(default=None, repr=False)  # type: ignore
    deck: Deck = field(default_factory=Deck, repr=False)

    @property
    def hands(self):
        return self._hands

    @hands.setter
    def hands(self, o):
        self._hands = o
        self.s = Visible(o.p_value, o.d_value, o.d_top_value, o.useable_ace)

    def __post_init__(self, h: SQ = None) -> None:
        if h:
            self.hands = h
        elif self._hands is None:
            d, p = deal_hands(self.deck)
            self.hands = Hands(d_hand=d, p_hand=p)

        # check for a natural blackjack
        self.hands.natural_blackjack()
        super().__post_init__()

    def reset(self, h: SQ = None) -> None:
        self.t = 0
        if h is None:
            d, p = deal_hands(self.deck)
            h = Hands(d_hand=d, p_hand=p)
        self.__post_init__(h)

    def act(self, a: A) -> S:
        """Dealer does not increment environment step."""
        sn = super().act(a)
        self.t -= 1 if a.role == AgentT.dealer else 0
        return sn

    def next_state(self, s: S, a: A) -> S:
        if a.draw == Draw.hit:
            card = self.deck.deal().v
            return self.hands.hit(a=a, card=card) or s
        else:
            return self.hands.hit(a=a) or s

    def on_action(self, a: A) -> S:
        """ Called by `act()`. This gets overwritten by inheritting class. """
        if self.hands.status == Status.play:
            sn = self.next_state(self.s, a)
            return sn
        return self.s


TableQ = Optional[Table]


action_cached_player = (
    Turn(draw=Draw.hit, role=AgentT.player),
    Turn(draw=Draw.stay, role=AgentT.player),
)
action_cached_dealer = (
    Turn(draw=Draw.hit, role=AgentT.dealer),
    Turn(draw=Draw.stay, role=AgentT.dealer),
)


@dataclass
class Player(Agent):

    role: AgentT = field(default=AgentT.player)
    π: Policy = field(default_factory=default_policy)

    def __post_init__(self):
        if self.role == AgentT.player:
            self.hit_cached, self.stay_cached = action_cached_player
            return
        self.hit_cached, self.stay_cached = action_cached_dealer

    def hand_value(self) -> int:
        if self.role == AgentT.player:
            return self.env.hands.p_value
        return self.env.hands.d_value

    def playing(self, h: Hands) -> bool:
        """Game is in play and haven't blackjacked / busted."""
        # assert self.env.s.p_value == h.p_value
        return h.status == Status.play  # and self.hand_value() < h.v_max

    def active(self) -> bool:
        """Haven't previously stayed."""
        return self.a is None or self.a.draw != Draw.stay

    def act(self, a: AQ = None) -> T:
        if (  # random start action for exploring starts
            a is None and self.π.es and self.env.t == 0 and self.role == AgentT.player
        ):
            a = choice(self.all_a(self.env.s))
        t = super().act(a)
        t = T(t.t, t.s, t.a, t.r, t.sn, 0.0) if t.r else t
        return t

    def all_a(self, s: S) -> List[A]:
        if self.role == AgentT.dealer:
            return [self.hit_cached, self.stay_cached]
        val = self.hand_value()
        if val == 21:
            return [self.stay_cached, self.stay_cached]
        if val < 12:
            return [self.hit_cached, self.hit_cached]
        return [self.hit_cached, self.stay_cached]

    def calc_reward(self, _: S, __: S) -> float:
        if self.role == AgentT.dealer:
            return 0.0
        status = self.env.hands.status
        if status == Status.tie:
            return 0.0
        if status == Status.win:
            return 1.0
        if status == Status.lose:
            return -1.0
        return 0.0


PlayerQ = Optional[Player]


if __name__ == "__main__":
    from pprint import pprint
    from tqdm import tqdm
    from time import sleep
    from mdp.unit import SelectA
    from mdp.learning import Algorithms, V
    from mdp.util import pd
    from chapter05.optimal_policy import target
    from util import plot_policy, plot_values, format_sav

    # play games
    m = 10
    agent = Player(env=Table())  # type: ignore # mypy sucks
    episodes: List[T] = []
    for i in range(m):
        episode = blackjack_episode(agent, pause=0.0, verbose=0, player_only=True)
        episodes += [t.as_dict({"ep": f"{i:03}"}) for t in episode]
    df = pd.DataFrame(episodes)
    print("\n", df.groupby(["ep", "t", "a.role"]).max())
    print(
        "\n",
        df.groupby(["ep"]).agg(
            {
                "rn": lambda x: "lose" if x.min() else "win" if x.max() else "tie",
                "sn.pv": "max",
                "sn.dv": "max",
            }
        ),
    )

    print("\nmonte carlo first visit")
    m = 10_000
    agent = Player(env=Table(), π=default_policy(γ=1.0))  # type: ignore # mypy sucks
    Q = Algorithms.mc_first_visit(
        episode=blackjack_episode, agent=agent, kwargs={}, n=m
    )
    # print(format_sav(agent.π.v.r.aQ, ace=True))
    plot_values(agent, Q, ace=True, f=2)
    plot_values(agent, Q, ace=False, f=2)

    print("\nexploring starts")
    m = 10_000
    agent = Player(env=Table(), π=default_policy(γ=1.0))  # type: ignore # mypy sucks
    aQ = Algorithms.mc_exploring_starts(
        episode=blackjack_episode, agent=agent, kwargs={}, n=m
    )
    Q = agent.π.v.r.Q
    plot_policy(agent, Q, ace=True, f=2)
    plot_policy(agent, Q, ace=False, f=2)

    print("\nepisode value iteration")
    m = 10_000
    agent = Player(  # type: ignore # mypy sucks
        env=Table(), π=random_policy(γ=0.9, strategy=Strategy.off)
    )
    agent.π.es = True
    v = Algorithms.episodic_value_iteration(
        episode=blackjack_episode, agent=agent, kwargs={}, n=m
    )
    Q = agent.π.v.r.Q
    plot_policy(agent, Q, values=True, ace=True, f=1)
    plot_policy(agent, Q, values=True, ace=False, f=1)

    print("\nMC on-policy first visit control egreedy")
    m = 10_000
    π = Policy(select=SelectA.best, ε=0.3, strategy=Strategy.ε_greedy, v=V(γ=1.0))
    agent = Player(env=Table(), π=π)  # type: ignore # mypy sucks
    aQ = Algorithms.mc_on_policy_εgreedy(
        episode=blackjack_episode, agent=agent, kwargs={}, n=m
    )
    Q = agent.π.v.r.Q
    plot_policy(agent, Q, ace=True, f=2)
    plot_policy(agent, Q, ace=False, f=2)

    print("\nGradient boosting exploring starts")
    m = 10_000
    agent = Player(
        Table(),
        π=Policy(es=True, select=SelectA.best, strategy=Strategy.on, v=V(γ=0.5)),
    )
    # agent.π.target = target()
    b = Algorithms.gradient_boosting(blackjack_episode, agent, {}, n=m)
    print(b)
    for _ in range(2):
        b = Algorithms.gradient_boosting(blackjack_episode, agent, {}, n=m, b=b)
    Q = b.analyze()[0]
    plot_policy(agent, Q, True, f=2)
    plot_policy(agent, Q, False, f=2)
