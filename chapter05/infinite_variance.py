from collections import deque
from dataclasses import dataclass, field
from enum import Enum

import numpy as np

from mdp.alias import Deque, List, Iterator, Num, Tuple
from mdp.framework import A, Agent, Environment, Policy, S
from mdp.unit import F, Status, Strategy, T


class Way(Enum):
    left = 0
    rigt = 1


@dataclass(frozen=True)
class Turn(A):
    way: Way
    name: str = field(init=False, repr=False)

    def __eq__(self, o) -> bool:
        return self.way == o.way

    def __hash__(self) -> int:
        return hash(self.way)


left_cache, rigt_cache = Turn(Way.left), Turn(Way.rigt)


@dataclass(frozen=True)
class One(S):
    status: Status
    name: str = field(init=False, repr=False)

    def __eq__(self, o) -> bool:
        return self.status == o.status

    def __hash__(self) -> int:
        return hash(self.status)


win_cache, lose_cache, play_cache = One(Status.win), One(Status.lose), One(Status.play)

# fast random transition probability
p, n = 0.9, 10 ** 7
transition_prob: Deque[float] = deque(np.random.binomial(1, p, size=n), maxlen=n)
transition = lambda dq: (dq.rotate(), dq[0])[1]


@dataclass
class Env(Environment):
    s: One = field(default=play_cache)

    def on_action(self, a: A) -> S:
        """generate the next state"""
        if a.way == Way.rigt:
            return win_cache
        if transition(transition_prob):
            return play_cache
        return lose_cache

    @staticmethod
    def all_s() -> List[S]:
        return [win_cache, play_cache]


@dataclass
class Agt(Agent):
    env: Env

    def __repr__(self) -> str:
        return f"Agt(a={self.a}, env={self.env})"

    @staticmethod
    def all_a(s: S) -> List[A]:
        """Possible actions from current state."""
        return [left_cache, rigt_cache]

    @staticmethod
    def calc_reward(s: S, sn: S) -> float:
        return 1.0 if sn.status == Status.win else 0.0


def episode(agent: Agent) -> Iterator[T]:
    agent.reset()
    while agent.env.s.status == Status.play:
        yield agent.act()


if __name__ == "__main__":
    agent = Agt(Env())
    for i in range(10):
        print(f"\nep: {i}")
        ep = list(episode(agent))
        r, s, ep_str = ep[-1].rn, ep[-1].sn.status, "\n".join(str(t) for t in ep)
        print(f"{ep_str}\n| {s}: reward={r}")
