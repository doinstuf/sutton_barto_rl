from blackjack import Player, Table
from mdp.benchmark import Config, DfFmt, run_benchmark
from mdp.learning import Algorithms
from mdp.unit import SelectA, Strategy
from util import blackjack_episode, default_policy

# config
configs = [
    Config(alg, n, select, {"verbose": 0})
    for alg in [
        "Algorithms.mc_first_visit",
        "Algorithms.mc_exploring_starts",
        "Algorithms.mc_on_policy_εgreedy",
        "Algorithms.episodic_value_iteration",
    ]
    for n in [25_000]
    for select in [SelectA.best, SelectA.rand]
]

# run benchmarks
agent = Player(env=Table(), π=default_policy(γ=1.0))  # type: ignore # (mypy sucks)
rdf = run_benchmark(agent=agent, ep=blackjack_episode, configs=configs)

# summarize results
algorithm_summary = DfFmt.summarize(rdf, ["alg", "select"])
code_summary = DfFmt.summarize(rdf, ["file", "line", "func"])
by_file = DfFmt.summarize(rdf, ["file"])

print("\n| algorithm summary:\n", algorithm_summary.head(25))
print("\n| code summary:\n", code_summary.head(25))
print("\n| by filename:\n", by_file.head(10), "\n")
print(f"calc_value.cache_info(): {agent.env.hands.calc_value.cache_info()}")
print(f"bd node count: {agent.π.v.r.node_count}")
print(f"bd node update count: {agent.π.v.r.update_count}")
