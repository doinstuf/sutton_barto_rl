from collections import defaultdict
from copy import copy, deepcopy
from dataclasses import dataclass, field
from enum import Enum
from pprint import pformat
from random import choice, choices, randint, random
from typing import *

from numpy import float64, int32, ndarray

ListQ = Optional[List]
IntQ = Optional[int]

# (object, depth, elements to show, length) -> formatted
Formatter = Callable[[Any, int, int, int], str]

# (object, attrs, depth, elements to show) -> formatted
Recursor = Callable[[Any, List[str], int, int], str]


def sanitize(s: str, d: int) -> str:
    if d > 2:
        return s.replace("'", "")
    else:
        return s.replace("\\n", "").replace("',", ",").replace("'", " ")


unicode_length: Dict[str, str] = {
    "short_equals": "\ua78a",
    "arrow_left": "\u2190",
    "arrow_left_slash": "\u219a",
}


def via_simple_repr(v: Any) -> bool:
    simplify = (
        lambda s: "".join(s.split()).strip().lower().replace("_", "") if s else []
    )
    doc_s = (v.__repr__.__doc__, v.__class__.__repr__.__doc__)
    return any(["simplerepr" in simplify(doc) for doc in doc_s])


@dataclass
class Formatters:
    recursor: Recursor  # likely SimpleRepr.simple_repr()
    sample: bool = False
    w: IntQ = None
    l: bool = True
    e: bool = True
    s: int = 2
    f: int = 2
    pd: int = 5
    ls: str = unicode_length["arrow_left"]  # length symbol
    lf: Callable[[int], str] = lambda l: f"{l:.0e}".replace(
        "+", ""
    ) if l >= 1_000 else f"{l}"  # length format
    formatters: Dict[Type, Recursor] = field(default_factory=dict, repr=False)
    p: Dict[Type, Dict[str, Any]] = field(default_factory=dict, repr=False)

    def __post_init__(self) -> None:
        # formatter params
        default_params = defaultdict(dict)
        default_params.update({str: {"s": 0, "j": "<"}})
        keys = set(self.p.keys()).union(default_params.keys())
        for k in keys:
            default_params[k].update(self.p[k] if k in self.p else {})
        self.p = dict(default_params)

        # formatters
        default_formatters = {
            bool: lambda v, d, n, L: f"{str(v):>5}",
            int: self.int_formatter,
            int32: self.int_formatter,
            float: self.float_formatter,
            float64: self.float_formatter,
            str: self.str_formatter,
            list: self.list_formatter,
            ndarray: self.list_formatter,
            set: self.list_formatter,
            dict: self.dict_formatter,
            defaultdict: self.dict_formatter,
            tuple: self.list_formatter,
            type(None): lambda v, d, n, L: f"{v}",
        }
        default_formatters.update(self.formatters)
        self.formatters = default_formatters

    def this_fmt(self, v: Any, d: int) -> str:
        args = {"width": self.w} if self.w != 0 and self.w is not None else {}
        return pformat(v, **args) if d <= self.pd else str(v)

    def next_fmt(self, vi: Any, d: int, n: int) -> str:
        if via_simple_repr(vi):
            try:
                return vi.__repr__(d=d)
            except TypeError:
                raise TypeError(
                    "You must add a depth `d` argument to your class repr e.g. def __repr__(d=0)"
                )
        return self.recursor(vi, d=d, n=n)

    def int_formatter(self, v: Any, d: int, n: int, L: int) -> str:
        return f"{v:= {self.s},}"  # at least self.s digits with commas

    def float_formatter(self, v: Any, d: int, n: int, L: int) -> str:
        return f"{v:= .{self.f}f}"  # n decimal places and + sign

    def str_formatter(self, v: Any, d: int, n: int, L: int) -> str:
        base: str = f'{v[:15].strip():{self.p[type(v)]["j"]}{self.p[type(v)]["s"]}}'
        tag: str = f"... {self.ls}{self.lf(L)}" if L > 15 else ""  # truncated string
        return f"{base}{tag}"

    def list_formatter(self, v: Any, d: int, n: int, L: int) -> str:
        vs = choices(v, k=n) if self.sample else list(v[:n])
        vs = [self.next_fmt(vi, d=d, n=n) for vi in vs]
        if L > n:
            if self.e:
                vs.append(f"...")
            vs = self.this_fmt(vs, copy(d))
            return f"{vs}{self.ls}{self.lf(L)}" if self.l else vs
        else:
            return self.this_fmt(vs, copy(d))

    def dict_formatter(self, v: Any, d: int, n: int, L: int) -> str:
        vs = choices(list(v.items()), k=n) if self.sample else list(v.items())[:n]
        vs = {self.next_fmt(k, d=d, n=n): self.next_fmt(vi, d=d, n=n) for k, vi in vs}
        if L > n:
            vs = self.this_fmt(vs, copy(d))
            if self.e:
                vs = f"{vs[:-1]}, ...{vs[-1]}"
            return f"{vs}{self.ls}{self.lf(L)}" if self.l else vs
        else:
            return self.this_fmt(vs, copy(d))

    def format(self, v: Any, d: int, n: int) -> str:
        """Special value formatting per data type."""

        t = type(v)
        try:
            L = len(v)
        except:
            L = 1

        if t in self.formatters:
            return sanitize(self.formatters[t](v, d, n, L), d)

        return sanitize(v.__repr__(), d)


FormattersQ = Optional[Formatters]


@dataclass(repr=False)
class SimpleRepr:
    """
    ## SimpleRepr
    Kind of like a `@dataclass` repr for custom classes, but additional formatting available.
    Element reduction for large and/or nested complex objects (e.g. `Dict[str, List[List[Union[int, str]]]]`)

    ```python
    ListQ = Optional[List]
    IntQ = Optional[int]
    ```

    ### Init arguments
    ```python
        ob: int                   # object to print
        attrs: ListQ[str]] = None # optional list of attributes on object to print
        n: int = 5                # number of elements in list-like objects to print
        w: IntQ = None            # width in characters (None pformat auto-selects)
        pd: int = 10              # pformat depth - depths at which pformat is used to format
        sample: bool = False      # show random n elements per collection. False shows first n.
        l: bool = True            # length is displayed for truncated collections
        e: bool = True            # elipses are displayed for truncated collections
        s: int = 2                # size of padding for floats and ints. good for vert alignment
        f: int = 2                # number of decimals rendered for a float
        reduction: float = 1.5    # factor for reducing `n` per `d`
                                  # e.g. 2.0 would be half n per depth, 0.1 would be 10x increase per n

        # non-init members
        _d: int = 0                                             # current object depth of recursion
        _stack: DefaultDict[int, List[str]] = defaultdict(list) # a list of formatted strings per depth
    ```

    ### Example standared objects

    ```python
    from simple_repr import SimpleRepr
    from random import randint, random

    rand = lambda: [2 * random() - 1 for _ in range(100)]
    index = [i for i in range(100)]
    test_ob = {k: v for k, v in zip(index, [rand() for _ in index])}

    print(SimpleRepr(test_ob))
    ```
        {  0 :  [ -0.83,  -0.04,   0.96,  ... ]←100,
           1 :  [  0.08,   0.44,   0.78,  ... ]←100,
           2 :  [ -0.45,   0.79,   0.85,  ... ]←100,
           3 :  [ -0.70,  -0.28,  -0.10,  ... ]←100,
           4 :  [  0.30,   0.00,  -0.05,  ... ]←100, ...}←100

    ### Example custom class:
    ```python
        from dataclasses import dataclass, field
        from simple_repr import SimpleRepr
        from random import randint, random

        @dataclass
        class MyClass:
            a: int
            b: float
            c: str = field(repr=False, default="A very long string.")

            def __repr__(self, d: int = 0) -> str:
                '''simple_repr'''
                return SimpleRepr(self).__repr__(d=d)

        a, b = lambda: randint(-5, 5), lambda: 2 * random() - 1
        test_ob = [MyClass(a(), b()) for _ in range(100)]

        print(SimpleRepr(test_ob))
    ```
            [ MyClass(a= 3, b=-0.74),
              MyClass(a=-2, b= 0.63),
              MyClass(a= 0, b= 0.81),
              MyClass(a=-2, b= 0.80),
              MyClass(a= 5, b= 0.38),
              ... ]←100
    """

    ob: Any
    attrs: Optional[List[str]] = None
    n: int = 5
    _w: IntQ = None
    w: IntQ = None
    pd: int = 10
    sample: bool = False
    l: bool = True
    e: bool = True
    s: int = 2  # padding for ints (useful for vertical alignment)
    f: int = 2  # decimals for float (useful for vertical alignment)
    reduction: float = 1.5
    formatters: Formatters = None
    _stack: DefaultDict[int, List[str]] = field(
        default_factory=lambda: defaultdict(list), init=False
    )
    special: Set[Type] = field(
        default_factory=lambda: {dict, list, set, Enum, defaultdict}, init=False
    )
    _d: int = 0

    def __post_init__(self):
        self.reduction = 1.0 if self.reduction == 0 else self.reduction
        if self.formatters is None:
            self.formatters = Formatters(
                recursor=self.simple_repr,
                sample=self.sample,
                pd=self.pd,
                w=self.w,
                l=self.l,
                e=self.e,
                s=self.s,
                f=self.f,
            )

    def __repr__(self, d: IntQ = None) -> str:
        if d == None and via_simple_repr(self.ob):
            raise AttributeError(
                "You must pass the depth `d` argument via your class repr e.g. SimpleRepr(self).__repr__(d=d)"
            )
        else:
            d = 0
        return self.simple_repr(ob=self.ob, attrs=self.attrs, n=self.n, d=d)

    @property
    def w(self) -> IntQ:
        return self._w

    @w.setter
    def w(self, val: IntQ) -> None:
        if isinstance(val, int):
            self._w = val
            if self.formatters:
                self.formatters.w = val

    def simple_repr(
        self, ob: Any, d: int = 0, attrs: List[str] = None, n: int = 5
    ) -> str:
        if d == 0:
            self._stack.clear()
        d += 1
        self._d = d if d > self._d else self._d
        out_s: List[str] = []
        reprs: Dict[str, bool] = {}

        # exponentially fewer elements per depth
        n = max(1, int(n // self.reduction)) if d > 1 else n

        # respect dataclass field repr
        if attrs is None and "__dataclass_fields__" in ob.__class__.__dict__:
            reprs = {a: f.repr for a, f in ob.__dataclass_fields__.items()}
        if attrs is None:
            attrs = ob.__annotations__ if hasattr(ob, "__annotations__") else attrs

        if attrs:
            for a in attrs:
                if a[0] is not "_" and (reprs[a] if a in reprs else True):
                    v = ob.__getattribute__(a)
                    out_s.append(f"{a}={self.formatters.format(v, d, n)}")
        else:
            out_s.append(self.formatters.format(ob, d, n))

        self._stack[d] += out_s
        f_str = ", ".join([sanitize(s, d) for s in out_s])
        return f"{ob.__class__.__name__}({f_str})" if attrs else f_str


if __name__ == "__main__":
    from string import ascii_letters

    rand_str: str = lambda n=5: "".join(choices(ascii_letters, k=n))
    rand_bool = lambda: choice([True, False])
    rand_unit = lambda m: m * (2 * random() - 1)
    rand_var = lambda n, m, i, l: choice(
        [randint(1, i + l), rand_str(n), rand_unit(m), rand_bool()]
    )
    rand_list = lambda l: [rand_var(5, 1, i, l) for i in range(l)]

    ob_test = rand_list(50)
    sr = SimpleRepr(ob_test)
    print(sr, "\n")

    @dataclass
    class MyClass:
        a: int
        b: float
        c: str = field(repr=False, default="A very long string.")

        def __repr__(self, d=0) -> str:
            """simple_repr"""
            return SimpleRepr(self).__repr__(d=d)

    a, b = lambda: randint(-5, 5), lambda: 2 * random() - 1
    test_ob = [MyClass(a(), b()) for _ in range(100)]
    print(SimpleRepr(test_ob), "\n")

    rand = lambda: [2 * random() - 1 for _ in range(100)]
    index = [i for i in range(100)]
    test_ob = {k: v for k, v in zip(index, [rand() for _ in index])}
    print(SimpleRepr(test_ob), "\n")

    ob_test = {
        k: v
        for k, v in enumerate(
            [[rand_list(50) for _ in rand_list(100)] for _ in rand_list(100)]
        )
    }
    ob_test = {rand_str(): ob_test for _ in range(100)}
    sr = SimpleRepr(ob_test, s=5, n=2, reduction=0.65)
    print(sr, "\n")
