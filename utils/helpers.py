from dataclasses import dataclass, field
from typing import *

import pandas as pd

TupleQ = Optional[Tuple]

flatten = lambda l: [item for sublist in l for item in sublist]


def where(df: pd.DataFrame, func: Callable[[pd.Series], bool]) -> pd.DataFrame:
    """ Usage
    df.where(lambda x: x[0] < 0).where(lambda x: x[1] > 0)
    """
    return df[func(df)]


pd.DataFrame.where = where


def nested_xor(l: List, i: Optional[int] = None) -> int:
    """Recursive pairwise xor of all elements in list"""
    from operator import xor

    if l and i:  # recursion
        return nested_xor(l[:-1], xor(l[-1], i))
    elif l:  # entry
        return nested_xor(l[:-1], l[-1])
    elif i:  # terminal
        return i
    else:
        raise Exception
