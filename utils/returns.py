from copy import copy
from dataclasses import dataclass, field
from typing import *

TupleQ = Optional[Tuple]


@dataclass
class Return:
    """
    ## Return
    Return is a fast¹, easy to use² running average object, including windowed
    averages with exponential convergence, and recency-weighted sums.

    __________________________________________________________________________

    ¹O(n) time complexity and O(1) memory complexity with respect to t (steps)

    ²No special APIs: only the + operator is needed

    ### Init arguments
    ```python
    avg: float = 0.0 # running avg
    n: int = 0       # number of items
    α: float = 1.0   # recency weight for sum and avg_r
    Σ: float = 0.0   # sum accumulation
    t: int = 0       # number of visits / updates
    ```

    ### Non-init members
    ```python
    avg_r: float      # exponential recency weighted average
    avg_r_prev: float # avg_r at t - 1
    avg_prev: float   # avg at t - 1
    Σ_prev: float     # Σ at t - 1

    # Note: avg_r with α = 1 - 4 / window_size (in terms of t) will converge to approximately
    # the same point at the same time as a linear rolling window of that size.
    ```

    ### Example usage
    ```python
    import numpy as np
    import pandas as pd
    import seaborn as sns
    from returns import Return as R

    # make a noisy sine wave
    m, n = 4 * np.pi, 1_000
    x = np.linspace(0, m, n)
    y = np.sin(x)
    noisy = y + np.random.random(n) - 0.5

    # compute a recency average using the Return class
    a = 1 - 1 / m
    r = R(α=a)
    estimate = []
    for sample in noisy:
        r += R(sample, n=1)
        estimate.append(r.avg_r)

    df = pd.DataFrame({'x': x, 'y': y, 'noisy': noisy, 'estimate': estimate})
    _ = sns.lineplot(data=df[['x', 'noisy', 'estimate']], lw=2)
    _ = sns.lineplot(data=df[['x', 'y', 'estimate']], lw=2)
    ```

    ### Naming
    The name Return was inspired by a Markov Decision Process in reinforcement learning.
    In an MDP, the return of an action on an environment must be averaged or accumulated in
    different ways, depending on the algorithm.
    """

    avg: float = field(default=0.0)
    n: int = field(default=0)
    α: float = field(default=1.0)
    Σ: float = field(default=0.0)
    t: int = field(default=0)

    avg_r: float = field(default=0.0, init=False)
    avg_prev: float = field(default=0.0, init=False)
    Σ_prev: float = field(default=0.0, init=False)
    αp: float = field(default=0.0, init=False)

    def __post_init__(self) -> None:
        self.αp = 1.0 - self.α

    def __add__(self, x: "Return") -> "Return":
        avg, avg_r, n, t, α, Σ, αp, mean = (
            self._vars()
        )  # type: ignore # None return not possible in this usage

        avg_prev, avg_prev_r, Σ_prev = avg, avg_r, Σ
        avg = mean(avg, n, x.avg, x.n)
        Σ = α * Σ + x.avg / max(x.n, 1)
        avg_r = αp * Σ
        n = n + x.n
        t += 1

        self._vars((avg_prev, avg_prev_r, Σ_prev, avg, Σ, avg_r, n, t))
        return self

    @staticmethod
    def mean(a: float, an: int, b: float, bn: int) -> float:
        return (a * an + b * bn) / max(an + bn, 1)

    def _vars(
        self, _vars: TupleQ = None
    ) -> TupleQ:  # type: ignore # I told it that the return was optional
        """Syntactic sugar to make the core equations easier to read"""
        if _vars:
            self.avg_prev, self.avg_prev_r, self.Σ_prev, self.avg, self.Σ, self.avg_r, self.n, self.t = (
                _vars
            )
        else:
            return (
                self.avg,
                self.avg_r,
                self.n,
                self.t,
                self.α,
                self.Σ,
                self.αp,
                self.mean,
            )


def return_example():
    """
    Demonstrate the relationship between a recency weighted sum,
    a running avg, and the source data.
    """
    from pandas import DataFrame
    import seaborn as sns
    from matplotlib import pyplot as plt
    from random import random

    sns.set(style="whitegrid")

    def make_rands(n: int = 10_000, m: int = 1_000, k: int = 10) -> List[float]:
        rands, offset = [], 0
        for i in range(n):
            offset = offset + 1 if i % m == 0 and offset < 5 else offset
            rands.append(random() + offset)
        return rands

    def running_avg(
        rands: List[float] = make_rands(), a: float = 0.9, k: int = 10, m: int = 1000
    ):
        w = m // 4  # window
        rolling = [0 for _ in range(w)]
        results = [Return(avg=random(), n=1, α=a, Σ=0.0)]
        results_rolling = []
        for i in range(len(rands)):
            results.append(copy(results[i] + Return(rands[i], 1, a)))
            rolling[i % w] = rands[
                i
            ]  # type: ignore # not sure why it's complaining about the index
            results_rolling.append(sum(rolling) / w)
        return results[1:], results_rolling

    n, m, k = 10_000, 1_000, 10
    a = 1 - (4 / m)  # should take roughly the length of the period to converge
    rands = make_rands(n, m, k)
    returns, rolling = running_avg(rands, a=a, k=k, m=m)
    total = {
        "x": [r.t for r in returns],
        "raw": rands,
        f"avg_r: α = 1 - 4/{m}t": [r.avg_r for r in returns],
        "avg": [r.avg for r in returns],
        f"rolling_bruteforce: window = {m}t/4": rolling,
        "delta: avg_r - raw": [abs(r[0].avg_r - r[1]) for r in zip(returns, rands)],
        "sum": [r.Σ for r in returns],
    }
    df = DataFrame(total)

    fig = plt.figure(figsize=(14, 8))
    ax = sns.lineplot(data=df.drop(columns=["sum"]), lw=3)
    fig.axes.append(ax)
    plt.show()


if __name__ == "__main__":
    _ = return_example()
