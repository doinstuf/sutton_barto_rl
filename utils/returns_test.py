import sys

sys.path.append("../utils/")
sys.path.append("./utils/")

from copy import copy
from random import random
from typing import *

from returns import Return
from simple_repr import SimpleRepr


class TestReturns:
    """To run: `>python -m pytest`"""

    @staticmethod
    def reset():
        m = 100
        vals = [2 * random() - 0.5 for _ in range(m)]
        test = Return()
        correct = [None for _ in range(m)]
        test_vals = [None for _ in range(m)]
        return m, vals, test, correct, test_vals

    @staticmethod
    def debug_print(correct: List[float], test: List[float]) -> None:
        print(f"Trth      : {SimpleRepr(correct, n=15, pd=0, s=3)}")
        print(f"Test      : {SimpleRepr(test, n=15, pd=0, s=3)}")
        print(f"(Rev) Trth: {SimpleRepr(list(reversed(correct)), n=15, pd=0, s=3)}")
        print(f"(Rev) Test: {SimpleRepr(list(reversed(test)), n=15, pd=0, s=3)}")

    def test_return_avg(self):
        """`utils.Return`"""
        m, vals, test, correct, test_vals = self.reset()

        for i in range(m):
            test += Return(vals[i], n=1)
            test_vals[i] = copy(test)
            correct[i] = sum(vals[: i + 1]) / (i + 1)

        correct = [round(c, 9) for c in correct]
        test = [round(t.avg, 9) for t in test_vals]
        self.debug_print(correct, test)
        assert test == correct

    def test_return_sum(self):
        """`utils.Return`"""
        m, vals, test, correct, test_vals = self.reset()

        for i in range(m):
            test += Return(vals[i], n=1)
            test_vals[i] = copy(test)
            correct[i] = sum(vals[: i + 1])

        correct = [round(c, 9) for c in correct]
        test = [round(t.Σ, 9) for t in test_vals]
        self.debug_print(correct, test)
        assert test == correct

    def test_return_sum_weighted(self):
        """`utils.Return`"""
        m, vals, test, correct, test_vals = self.reset()
        a, test.α = 0.9, 0.9

        for i in range(m):
            test += Return(vals[i], n=1)
            test_vals[i] = copy(test)
            correct[i] = a * (correct[i - 1] if i else 0) + vals[i]

        correct = [round(c, 9) for c in correct]
        test = [round(t.Σ, 9) for t in test_vals]
        self.debug_print(correct, test)
        assert test == correct
