"""Utilities for chapter 4."""
from copy import deepcopy
import numpy as np
from typing import List, Generator

from gridworld import Agent, SS, Coord, Environment, Algorithms, Move, Cell, inverse


def coord_2d(size: int) -> Generator[Coord, None, None]:
    for y in range(size):
        for x in range(size):
            yield (y, x)


def assign_specials(grid: np.ndarray, ss: List[SS], size: int) -> np.ndarray:
    prime = lambda s: s.n[0] + "'"

    for s in ss:
        sp = SS(prime(s), s.yx, s.yxp, bonus=0)
        s_cell, p_cell = Cell(s.yx, s), Cell(sp.yxp, sp)
        if s_cell.validate() and p_cell.validate():
            grid[s.yx] = s_cell
            grid[s.yxp] = p_cell
        else:
            raise AttributeError

    return grid


def value_iteration(agent: Agent, states: List[Cell], verbose: int = 1):
    Algorithms.value_iteration(agent=agent, states=states, verbose=verbose)

    # assign the value to the state variable
    for c in coord_2d(agent.env.size):
        s = agent.env.grid[c]
        sv = agent.π.v.r.rs(s).υ.v
        agent.env.grid[c] = Cell(yx=s.yx, ss=s.ss, v=sv)
        # agent.env.grid[c].v = agent.π.v.υπ(agent.π, agent.env.grid[c]).q

    # calculate optimal directions from the value function
    # this will represent all of the ties that the policy won't
    move_action = lambda c: Move(c.yx) if c.yx in inverse else None
    optimal_destinations: np.ndarray = np.zeros_like(agent.env.grid)

    # for each cell in the grid, find the highest valued neighbors and calculated a unit direction vector
    for c in coord_2d(agent.env.size):
        neighbors = agent.env.all_sn(agent.env.grid[c])
        optimal_destinations[c] = [n for n in neighbors if n >= max(neighbors)]
        agent.optimal_moves[c] = [
            move_action(cell - Cell(c)) for cell in optimal_destinations[c]
        ]


def visualize_values(agent: Agent):
    grid = deepcopy(agent.env.grid)
    for c in coord_2d(agent.env.size):
        grid[c] = Cell(grid[c].yx, grid[c].ss, grid[c].v, True)
    print(grid)


def visualize_optimal_moves(agent: Agent):
    # render the optimal moves per cell
    sort_moves = {"<": 0, "^": 1, "v": 1, ">": 2, "<^v>": 3}
    optimal_moves = deepcopy(agent.optimal_moves)

    # format each movement for printing
    for c in coord_2d(agent.env.size):
        optimal_moves[c] = sorted(
            [str(move) if move else "<^v>" for move in optimal_moves[c]],
            key=lambda m: sort_moves[m],
        )
        optimal_moves[c] = f"{''.join(optimal_moves[c]):^4}"
    print(optimal_moves)


def visualize_possible_states(env: Environment) -> np.ndarray:
    grid = deepcopy(env.grid)
    s = env.s
    sn = env.all_sn(env.s)

    # render the current state as special state o
    sg = grid[s.yx]
    grid[s.yx] = Cell(yx=sg.yx, ss=SS("o", s.yx, s.yx))

    # render the next states as special states +
    for n in sn:
        grid[n.yx] = Cell(n.yx, SS("+", n.yx, n.yx))
    return grid


def walk(agent: Agent, steps: int = 10, pause: float = 1):
    from time import sleep
    from IPython.display import clear_output

    legend = "'o' = current state, '+' = candidate states\n"
    clear_output(wait=True)
    sleep(pause)
    print(legend, f"step: -\\{steps}\n\n", agent)
    sleep(pause)
    for i in range(1, steps):
        step = agent.act()
        clear_output(wait=True)
        print(legend, f"step: {i}\\{steps}\n\n", agent)
        print(step, "\n")
        sleep(pause)
