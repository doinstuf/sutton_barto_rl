from copy import deepcopy
from dataclasses import dataclass, field
from random import choice
from typing import *

import numpy as np
from tqdm import tqdm


import util as c3util
from mdp import util
from mdp.alias import *
from mdp.framework import Agent, Environment, Policy
from mdp.learning import V, Algorithms
from mdp.unit import A, S, SelectA, Strategy, T

Coord = Tuple[int, int]
CoordQ = Optional[Coord]


@dataclass
class SS:
    """Special State: bonus points for leaving."""

    n: str
    yx: Coord
    yxp: Coord
    bonus: float = 0


SSQ = Union[SS, None]


@dataclass(frozen=True)
class Cell(S):
    """
    Implementation of the state variable for the Gridworld problem.
    ```python
    yx: Coord  # location on the grid
    ss: SSQ = None  # in a special state
    v: float = 0.0  # value estimate
    render_v: bool = False  # render the value on repr
    ```
    """

    name: str = field(init=False)
    yx: Coord
    ss: SSQ = None
    v: float = 0.0  # value estimate
    render_v: bool = False

    def __repr__(self) -> str:
        if self.v and self.render_v:
            v = round(self.v, 1)
            return f"{str(v):>4}"
        elif self.ss:
            return f"{self.ss.n:<2}"
        else:
            return f'{"_":<2}'

    def __eq__(self, o: S) -> bool:
        special = self.ss.yx == o.ss.yx if self.ss and o.ss else True
        return special and self.yx == o.yx

    def __hash__(self) -> int:
        return hash(f'{self.yx}{"_" + self.ss.n if self.ss else ""}')

    def __add__(self, o: S) -> S:
        return Cell((self.yx[0] + o.yx[0], self.yx[1] + o.yx[1]))

    def __sub__(self, o: S) -> S:
        return Cell((self.yx[0] - o.yx[0], self.yx[1] - o.yx[1]))

    def __ge__(self, o: S) -> bool:
        return self.v >= o.v

    def __gt__(self, o: S) -> bool:
        return self.v > o.v

    def validate(self, upper: Optional[int] = None) -> bool:
        return (
            0 <= self.yx[0]
            and (self.yx[0] < upper if upper else True)
            and 0 <= self.yx[1]
            and (self.yx[1] < upper if upper else True)
            and (self.yx in (self.ss.yx, self.ss.yxp) if self.ss else True)
        )


CellQ = Optional[Cell]


cardinals: List[str] = ["north", "south", "east", "west"]
directions: List[Coord] = [(-1, 0), (1, 0), (0, 1), (0, -1)]
mapping: Dict[str, Coord] = {d[0]: d[1] for d in zip(cardinals, directions)}
inverse: Dict[Coord, str] = {d[1]: d[0] for d in zip(cardinals, directions)}
render: Dict[str, str] = {"north": "^", "east": ">", "west": "<", "south": "v"}


@dataclass(frozen=True)
class Move(A):
    """
    Implementation of the move action for the Gridworld problem.
    ```python
    dir: Coord = (-1, 0)  # mapping(name) choice of 'north', 'south', 'east', 'west'
    ```
    """

    name: str = field(init=False, repr=False)
    dir: Coord = field(default=(-1, 0))

    def __repr__(self) -> str:
        return render[inverse[self.dir]]

    def __eq__(self, o) -> bool:
        if isinstance(o, type(self)):
            return self.dir == o.dir
        return False

    def __hash__(self) -> int:
        return hash(self.dir)


MoveQ = Optional[Move]


new_grid = lambda s: np.array([[Cell((y, x)) for x in range(s)] for y in range(s)])


@dataclass
class Gridworld(Environment):
    """
    Implementation of the Gridworld environment.
    ```python
    t: int = 0
    size: int = 5
    specials: List[SS] = []
    ```
    """

    s: Move = field(init=False)
    specials: List[SS] = field(default_factory=list)
    init: CoordQ = None
    t: int = 0
    size: int = 5

    def __post_init__(self):
        self.grid: np.ndarray = c3util.assign_specials(
            new_grid(self.size), self.specials, self.size
        )
        self.first_s: S = self.grid[self.init] if self.init else self.grid[
            (self.size // 2, self.size // 2)
        ]
        self._s = deepcopy(self.first_s)

    def __repr__(self) -> str:
        return self.grid.__str__()

    def __str__(self) -> str:
        return self.grid.__str__()

    def reset(self, s: CellQ = None, deep: bool = False) -> None:
        self.t = 0
        self.s = self.grid[s.yx] if s is not None else self.first_s
        if deep:
            self.grid = new_grid(self.size)
            self.grid = c3util.assign_specials(self.grid, self.specials, self.size)

    @property
    def s(self) -> Cell:
        return self._s

    @s.setter
    def s(self, s: Cell) -> None:
        if not isinstance(s, property):
            if s.validate():
                self._s = s
            else:
                raise Exception

    def next_state(self, s: Cell, a: A) -> Cell:
        if self.special():  # if currently in a special, jump to the target
            return self.grid[s.ss.yxp]  # type: ignore # checked in conditional

        # update the state if destination is valid
        next_state = s + Cell(a.dir)
        if next_state.validate(self.size):
            return self.grid[next_state.yx]
        return s

    def special(self) -> bool:
        # in a special state if the cell is special and it is the source location
        return self.s.ss is not None and self.s.yx == self.s.ss.yx

    def on_action(self, a: A) -> S:
        """ Called by `act()` """
        return self.next_state(self.s, a)

    def neighbors(self, c: Cell) -> List[Coord]:
        moves = [Cell(m) for m in directions]
        return [(c + m).yx for m in moves if (c + m).validate(self.size)]

    def all_sn(self, c: Cell) -> List[Cell]:
        # in a special state with a special target, regardless of move direction
        if c.ss and c.yx == c.ss.yx:
            return [self.grid[c.ss.yxp]]

        # find and validate new locations from all possible move directions
        return [self.grid[n] for n in self.neighbors(c)]


moves_init = lambda s: np.array([[Move() for _ in range(s)] for _ in range(s)])


@dataclass
class GAgent(Agent):
    """
    Implementation of an agent that interacts with the Gridworld environment.
    ```python
    vmb: float = 0.0  # valid move bonus
    ```
    """

    vmb: float = 0.0  # valid move bonus

    def __post_init__(self):
        self.reset(deep=True)

    def __repr__(self) -> str:
        header = f'{round(self.Σr, 1)} <- reward={self.r} {"(Special State)" if self.env.special() else ""}'
        grid = c3util.visualize_possible_states(self.env)
        return f"{header}\n{grid}"

    def reset(self, s: Coord = None, deep: bool = False) -> None:
        self.r: float = 0
        self.Σr: float = 0
        self.env.reset(s, deep)
        if deep:
            self.values = {self.env.grid[c]: 0 for c in c3util.coord_2d(self.env.size)}
            self.optimal_moves = moves_init(self.env.size)

            # update the environment state estimates
            for c in c3util.coord_2d(self.env.size):
                s = self.env.grid[c]
                self.env.grid[c] = Cell(yx=s.yx, ss=s.ss)

    @staticmethod
    def all_a(s: CellQ = None) -> List[Move]:
        return [Move(d) for d in inverse]

    def calc_reward(self, s: Cell, sn: Cell) -> float:
        if s.ss and s.yx == s.ss.yx and sn.yx == s.ss.yxp:
            return s.ss.bonus
        return -1 if s == sn else self.vmb

    def all_t(self, a: A) -> List[T]:
        r, s, t = self.r, self.env.s, self.env.t
        sn = self.env.next_state(self.env.s, a)
        rn = self.calc_reward(self.env.s, sn)
        return [T(t, s, a, r, sn, rn)]


if __name__ == "__main__":
    """ Some common and some special cases """
    specials = [SS("A", (0, 1), (4, 1), 10), SS("B", (0, 3), (2, 3), 5)]

    # let the agent go for a few steps
    a = GAgent(Gridworld(init=(0, 3), specials=specials))
    c3util.walk(a, steps=3)

    # value function
    policy = Policy(
        name="random", strategy=Strategy.off, select=SelectA.rand, v=V(θ=1e-3, γ=0.9)
    )
    a = GAgent(Gridworld(init=(0, 3), specials=specials), π=policy)
    c3util.value_iteration(agent=a, states=util.flatten(a.env.grid))

    print("Random state values:")
    c3util.visualize_values(a)
    c3util.visualize_optimal_moves(a)
    a.reset()
    a.π.strategy = Strategy.on
    c3util.walk(a, steps=10)
    pa = deepcopy(a.π.target)

    policy = Policy(
        name="optimal", strategy=Strategy.off, select=SelectA.best, v=V(θ=1e-3, γ=0.9)
    )
    a = GAgent(Gridworld(init=(0, 3), specials=specials), π=policy)
    c3util.value_iteration(agent=a, states=util.flatten(a.env.grid))
    print("\nOptimal state values:")
    c3util.visualize_values(a)
    c3util.visualize_optimal_moves(a)
    pb = deepcopy(a.π.target)

    # let the agent go for a few steps
    a.π.strategy = Strategy.on
    c3util.walk(a, steps=10)

    print(pa == pb)
    print(pa, "\n")
    print(pb, "\n")
