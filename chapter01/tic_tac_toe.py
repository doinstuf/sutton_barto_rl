# 1.5 Tic-Tac-Toe`
from dataclasses import dataclass
from typing import *

Layout = List[List[Optional[int]]]
Value = Dict[str, float]


@dataclass
class Move:
    state_key: str
    player: int
    location: Tuple[int, int]
    exploratory: Optional[bool] = None  # optional set by reinforcement algorithm
    win_prob: Optional[float] = None  # optional will win set by reinforcement algorithm


class State:
    def __init__(self) -> None:
        self.board: Layout = [
            [None, None, None],
            [None, None, None],
            [None, None, None],
        ]
        self.x_winning_moves: int = 0
        self.o_winning_moves: int = 0


class Board:
    """ tic-tac-toe game board. fixed size, not general case.
    """

    def __init__(self) -> None:
        self.state: State = State()
        self.x = 1
        self.o = 0
        self.last_player: Optional[int] = None
        self.sequence: List[Move] = []
        self.winner: bool = False

    def __repr__(self) -> str:
        b = ""
        for i in range(3):
            for j in range(3):
                if self.state.board[i][j] is None:
                    b += " _"
                elif self.state.board[i][j] == self.x:
                    b += " x"
                elif self.state.board[i][j] == self.o:
                    b += " o"
            b += "\n"
        return b

    def key(self, board: Layout = None) -> str:
        """ generates a unique key for the board state """
        k, b = "", board if board else self.state.board
        for i in range(3):
            for j in range(3):
                k += str(b[i][j]) if b[i][j] is not None else "n"
        return k

    @staticmethod
    def rows(board: Layout) -> Layout:
        return board

    @staticmethod
    def columns(board: Layout) -> Layout:
        return [
            [board[0][0], board[1][0], board[2][0]],
            [board[0][1], board[1][1], board[2][1]],
            [board[0][2], board[1][2], board[2][2]],
        ]

    @staticmethod
    def diagonals(board: Layout) -> Layout:
        return [
            [board[0][0], board[1][1], board[2][2]],
            [board[2][0], board[1][1], board[0][2]],
        ]

    @staticmethod
    def flip(val: Optional[int]) -> Optional[int]:
        if val is not None:
            return 1 if val == 0 else 0
        else:
            return val

    def flip_all(self, board: Layout) -> Layout:
        temp_board: Layout = State().board
        for i in range(3):
            for j in range(3):
                temp_board[i][j] = self.flip(board[i][j])
        return temp_board

    def win(self, board: Layout) -> Tuple[int]:
        """
            checks all possible win states for x and o.
            first item in tuple is x win counts, second is o win counts.
        """

        def check_all(board: Layout) -> int:
            win = 0
            for line in self.rows(board):
                if all(line):
                    win += 1

            for line in self.columns(board):
                if all(line):
                    win += 1

            for line in self.diagonals(board):
                if all(line):
                    win += 1

            return win

        return check_all(board), check_all(self.flip_all(board))

    def play(
        self,
        player: int,
        loc: Tuple[int, int],
        print_board: bool = False,
        exploratory: Optional[bool] = None,
        win_prob: Optional[float] = None,
    ) -> str:
        valid_loc = lambda x: x[0] >= 0 and x[1] >= 0 and x[0] <= 2 and x[1] <= 2

        if self.game_over():
            return "The game is over."
        if not valid_loc(loc):
            return "Must play within board region."
        if player != self.x and player != self.o:
            return "Must be an eligible player."
        if player == self.last_player:
            return "Players must take turns."
        if self.state.board[loc[0]][loc[1]] is not None:
            return "Must play in empty location."

        # update board
        self.state.board[loc[0]][loc[1]] = player

        # update state
        win = self.win(self.state.board)
        self.state.x_winning_moves = win[0]
        self.state.o_winning_moves = win[1]
        self.last_player = player
        self.sequence.append(
            Move(self.key(self.state.board), player, loc, exploratory, win_prob)
        )

        if print_board:
            print(self)
        return

    def game_over(self) -> bool:
        return (
            self.state.x_winning_moves
            or self.state.o_winning_moves
            or all(loc is not None for loc in list_of_lists(self.state.board))
        )

    def gen_all_states(self) -> Dict[str, Tuple[State, Tuple[int]]]:
        from itertools import product

        double_winner = lambda x: x[0] >= 1 and x[1] >= 1

        all_states = product(product([0, 1, None], repeat=3), repeat=3)
        # TODO: consider removing impossible double win states for one player, e.g. two rows or two columns
        valid_states = [
            (val, self.win(val))
            for val in all_states
            if not double_winner(self.win(val))
        ]
        return {self.key(state[0]): state for state in valid_states}


class RandomPlayer:
    from random import choice

    def __init__(self, board: Board, player: int) -> None:
        self.board = board
        self.player = player
        self.last_msg: str = None

    def play(self, print_board: bool = False) -> Tuple[int, int]:
        available_moves = []
        for i in range(3):
            for j in range(3):
                if self.board.state.board[i][j] is None:
                    available_moves.append((i, j))
        if available_moves:
            loc = self.choice(available_moves)
            self.last_msg = self.board.play(self.player, loc, print_board=print_board)
        else:
            # no available moves
            loc = (-1, -1)

        return loc


list_of_lists = lambda l: [item for sublist in l for item in sublist]


class RLPlayer:
    def __init__(
        self,
        board: Board,
        player: int,
        value_function: Value = None,
        step_size: float = 0.02,
        exploratory_rate: float = 0.0,
        tie_weight: float = 0.1,  # lower than 0.5 to penalize ties more like losses
    ) -> None:
        self.step_size = step_size  # alpha
        self.board = board
        self.player = player
        self.tie_weight = tie_weight
        self.last_msg: str = None
        self.value_function = (
            self.value_function_priors(board.gen_all_states())
            if value_function is None
            else value_function
        )
        self.candidate_moves: Dict[str, List[str]] = self.next_candidate_states(
            board.gen_all_states()
        )
        self.locs_from_state_index = [
            (0, 0),
            (0, 1),
            (0, 2),
            (1, 0),
            (1, 1),
            (1, 2),
            (2, 0),
            (2, 1),
            (2, 2),
        ]
        self.exploratory_rate = exploratory_rate
        self.total_matches_played = 0

    def prior(self, wins: Tuple[int]) -> float:
        if wins[0] >= 1:
            return 1.0  # x's have won
        elif wins[1] >= 1:
            return 0.0  # o's have won
        else:
            return self.tie_weight  # neither have won

    def value_function_priors(
        self, states: Dict[str, Tuple[State, Tuple[int]]]
    ) -> Value:
        return {k: self.prior(v[1]) for k, v in states.items()}

    def next_candidate_states(
        self, states: Dict[str, Tuple[State, Tuple[int]]]
    ) -> Dict[str, List[str]]:
        """ """

        def neighbors(key: str, player: int) -> List[str]:
            n: List[str] = []
            start = lambda k, i: k[: max(0, i)]
            end = lambda k, i: k[min(len(k), i + 1) :]
            for i, c in enumerate(key):
                if c == "n":
                    n.append(f"{start(key, i)}{player}{end(key, i)}")
            return n

        return {k: neighbors(k, self.player) for k in states.keys()}

    def value_function_update(self, moves: List[Move]) -> None:
        """
            Updates win probability of all states seen at end of game,
            rather than per move during game. This is so that
                1) Losses and ties where the RL player does not make the last move get their final values propogated backwards
                2) The first two moves get updated even though it is impossible for them to end in a win
                (only state with a prior of 1.0)
                3) Learning is much faster
        """

        def update_state(state_current: str, state_next: str) -> float:
            """ page 10 """
            vst0, vst1 = (
                self.value_function[state_current],
                self.value_function[state_next],
            )
            return round(vst0 + self.step_size * (vst1 - vst0), 5)

        player_moves = [
            m
            for i, m in enumerate(moves)
            if m.player == self.player or i == len(moves) - 1
        ]
        player_moves_backwards = player_moves[::-1]
        for i, _ in enumerate(player_moves_backwards[:-1]):
            cur, nex = (
                player_moves_backwards[i + 1].state_key,
                player_moves_backwards[i].state_key,
            )
            self.value_function[cur] = update_state(cur, nex)
        self.total_matches_played += 1

        return

    def next_move_loc(self, s1: str, s2: str) -> Tuple[int, int]:
        state_index_diffs = [i for i, c in enumerate(zip(s1, s2)) if c[0] != c[1]]
        loc = self.locs_from_state_index[state_index_diffs.pop()]
        return loc

    def decide_next_state(self, state_current: str) -> Optional[Tuple[str, float]]:
        from random import random, choice

        available_moves = self.candidate_moves[state_current]
        if available_moves is None:
            return

        win_probs = [(s, self.value_function[s]) for s in available_moves]
        max_win_prob = max(win_probs, key=lambda x: x[1])[1]
        optimal_moves = [w for w in win_probs if w[1] == max_win_prob]
        exploratory_moves = [w for w in win_probs if w[1] < max_win_prob]
        if random() > self.exploratory_rate or not exploratory_moves:
            next_move = choice(optimal_moves)
        else:
            next_move = choice(exploratory_moves)

        return next_move

    def play(self, print_board: bool = False) -> Tuple[int, int]:
        state_current = self.board.key()
        state_next, win_prob = self.decide_next_state(state_current)

        if state_next:
            loc = self.next_move_loc(state_current, state_next)
            self.last_msg = self.board.play(
                self.player,
                loc,
                print_board=print_board,
                exploratory=False,
                win_prob=win_prob,
            )
        else:
            # no available moves
            loc = (-1, -1)

        return loc


def play_game(
    board: Board, player_x, player_o, x_starts=True, wait: int = 1, chatty: int = 2
) -> None:
    from time import sleep

    x_turn = x_starts
    last_msg = lambda p: f"({p.last_msg})" if p.last_msg is not None else ""

    while not (board.state.o_winning_moves or board.state.x_winning_moves) and [
        val for val in list_of_lists(board.state.board) if val is None
    ]:
        if x_turn:
            loc = player_x.play()
            if chatty > 0:
                print(f"\nx plays {loc} {last_msg(player_x)}")
        else:
            loc = player_o.play()
            if chatty > 0:
                print(f"\no plays {loc} {last_msg(player_o)}")
        x_turn = not x_turn
        if chatty > 1:
            print(board)
        sleep(wait)

    if chatty > 0:
        print("Game is over.")
    return


def play_to_train(
    num_matches: int = 500_000,
    x: RLPlayer = RLPlayer(Board(), Board().x),  # only x learns
    o: Any = RandomPlayer(Board(), Board().o),
    wait_time: float = 0.0,
    x_start_rate: float = 0.5,
    chatty: int = 0,
) -> Tuple[RLPlayer, List[Move]]:
    from random import random
    from tqdm import tqdm

    results: Dict[int, List[Move]] = {}
    for i in tqdm(range(num_matches)):
        b = Board()
        x.board = b
        o.board = b
        play_game(b, x, o, x_starts=(random() < x_start_rate), wait=wait_time, chatty=0)
        x.value_function_update(b.sequence)
        results[i] = b.sequence
        updated_values = [
            x.value_function[m.state_key]
            if m.player == x.player or j == len(b.sequence) - 1
            else None
            for j, m in enumerate(b.sequence)
        ]
        if chatty:
            print(f"Match {i}: {updated_values}")

    final_states = [x.value_function[m[-1].state_key] for m in results.values()]
    wins = len([p for p in final_states if p == 1])
    losses = len([p for p in final_states if p == 0])
    ties = len([p for p in final_states if p == x.tie_weight])
    print(f"X wins: {wins}, X losses: {losses}, ties: {ties}")

    return x, results
