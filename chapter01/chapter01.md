## Chapter 1

[./chapter01.ipynb](./chapter01.ipynb)

```python
import tic_tac_toe as t
```

### Train


```python
# `x` is a trained RLPlayer
a = 0.2
eps = 0.05
n = 100_000
x = t.RLPlayer(board=t.Board(), player=t.Board().x, exploratory_rate=a, step_size=eps)
x, moves = t.play_to_train(num_matches=n, x_start_rate=0.4)

# train x against its previous self m times
m = 3
for i in range(m):
    x_prev = t.RLPlayer(t.Board(), t.Board().o)
    x_prev.value_function = x.value_function
    x.exploratory_rate, x.step_size = x.exploratory_rate / 2, x.step_size / 2
    x, moves = t.play_to_train(x=x, o=x_prev, num_matches=(i+2)*n, x_start_rate=0.4)
```

    100%|████████████████████████████| 100000/100000 [00:18<00:00, 5349.05it/s]
    X wins: 82262, X losses: 12279, ties: 5459
    
    100%|████████████████████████████| 200000/200000 [00:39<00:00, 5009.56it/s]
    X wins: 188104, X losses: 2742, ties: 9154

    100%|████████████████████████████| 300000/300000 [00:59<00:00, 5021.58it/s]
    X wins: 282174, X losses: 4046, ties: 13780

    100%|████████████████████████████| 400000/400000 [01:19<00:00, 5022.52it/s]
    X wins: 376384, X losses: 5462, ties: 18154
    


### Play against trained RL player


```python
b = t.Board()
x.board = b
input_loc = lambda x: [int(loc.strip()) for loc in x.split(',')]

while not b.game_over():
    loc = x.play(print_board=True)
    if not b.game_over():
        loc = b.play(b.o, input_loc(input('Input location as y, x: ')), print_board=True)

if b.state.x_winning_moves:
    print('X wins')
elif b.state.o_winning_moves:
    print('O wins')
else:
    print('Tie game')
```

```
    x _ _
    _ _ _
    _ _ _
```
```
Input location as y, x: 0,1
    x o _
    _ _ _
    _ _ _
```
```
    x o _
    _ x _
    _ _ _
```
```
Input location as y, x: 2,2
    x o _
    _ x _
    _ _ o
```
```
    x o _
    x x _
    _ _ o
```
```
Input location as y, x: 1,2
    x o _
    x x o
    _ _ o
```
```
    x o _
    x x o
    x _ o

X wins
```
