# Reinforcement Learning (Sutton, Barto)

Doing this for fun. Accuracy of contents not guaranteed.

## Setup

1. Install [Miniconda 3.x](https://conda.io/en/latest/miniconda.html)
2. Create a **sutton_rl** conda python 3.7 environment with all dependencies
    - `conda env create -f environment.yml`
    - `conda activate sutton_rl`
    - `pip install -e .`
3. Making changes
    - `python -m black .`
    - `python -m pytest`

## Contents

- [Chapter 1](./chapter01/chapter01.md)
- [Chapter 2](./chapter02/chapter02.md)
- [Chapter 3](./chapter03/chapter03.md)
- [Chapter 4](./chapter04/chapter04.md)
- [Chapter 5](./chapter05/chapter05.md)

## Chapter 1

[./chapter01/chapter01.ipynb](./chapter01/chapter01.ipynb)

### Play against trained RL player
Agent: **x**, Player: **o**

```
    x _ _
    _ _ _
    _ _ _
```
```
Input location as y, x: 0,1
    x o _
    _ _ _
    _ _ _
```
```
    x o _
    _ x _
    _ _ _
```
```
Input location as y, x: 2,2
    x o _
    _ x _
    _ _ o
```
```
    x o _
    x x _
    _ _ o
```
```
Input location as y, x: 1,2
    x o _
    x x o
    _ _ o
```
```
    x o _
    x x o
    x _ o

X wins
```

## Chapter 2

[./chapter02/chapter02.ipynb](./chapter02/chapter02.ipynb)

```python
bandits = [
    # different eps
    Bandit(eps=0, q1=0, weight=weight_n),
    Bandit(eps=0.01, q1=0, weight=weight_n),
    Bandit(eps=0.1, q1=0, weight=weight_n),
    
    # optimistic initial values with weighted average
    Bandit(eps=0, q1=5, weight=weight_1),
    Bandit(eps=0.1, q1=0, weight=weight_1),
    
    # upper confidence bound selection
    Bandit(eps=-1, q1=0, c=2, weight=weight_1),
    
    # gradient action preferences with softmax selection
    Bandit(q1=4, b=4, rb=True, alpha=0.1),
    Bandit(q1=4, b=4, rb=True, alpha=0.4),
    Bandit(q1=0, b=4, rb=False, alpha=0.1),
    Bandit(q1=0, b=4, rb=False, alpha=0.4),
]

t = Testbed(bandits=bandits)
t.run_tests()
```

### Graphs


![png](/chapter02/img/output_4_0.png)


![png](/chapter02/img/output_5_0.png)

![png](/chapter02/img/output_6_0.png)


![png](/chapter02/img/output_7_0.png)



![png](/chapter02/img/output_8_0.png)

### Parameter Study

![png](/chapter02/img/output_14_0.png)


## Chapter 3

[./chapter03/chapter03.ipynb](./chapter03/chapter03.ipynb)

### Gridworld

    [[_  A  _  B  _ ]
     [_  _  _  _  _ ]
     [_  _  _  B' _ ]
     [_  _  _  _  _ ]
     [_  A' _  _  _ ]]
    
_______________
    Policy(name='random', strategy=off-policy, v=V(t=30, θ=0.0010000, Δ=0.0009068), require_stable=False, converged=True, changes=0, stable=50)

    Figure 3.2 State values of random policy:
    [[ 3.3  8.8  4.4  5.3  1.5]
     [ 1.5  3.0  2.3  1.9  0.6]
     [ 0.1  0.7  0.7  0.4 -0.4]
     [-1.0 -0.4 -0.4 -0.6 -1.2]
     [-1.9 -1.3 -1.2 -1.4 -2.0]]
    
_________________

    Policy(name='optimal', strategy=off-policy, v=V(t=20, θ=0.0010000, Δ=0.0006856), require_stable=False, converged=True, changes=0, stable=34)
    
    Figure 3.5 State values of optimal moves:
    [[22.0 24.4 22.0 19.4 17.5]
     [19.8 22.0 19.8 17.8 16.0]
     [17.8 19.8 17.8 16.0 14.4]
     [16.0 17.8 16.0 14.4 13.0]
     [14.4 16.0 14.4 13.0 11.7]]
    
____________________________
    Figure 3.5 Optimal moves:
    [[' >  ' '<^v>' ' <  ' '<^v>' ' <  ']
     [' >  ' ' ^  ' ' <^ ' ' <  ' ' <  ']
     [' >  ' ' ^  ' ' <^ ' ' <^ ' ' <^ ']
     [' >  ' ' ^  ' ' <^ ' ' <^ ' ' <^ ']
     [' >  ' ' ^  ' ' <^ ' ' <^ ' ' <^ ']]
    

### Comparing the random and optimal policies

Simulate many walks in random grids and plot results comparing the rewards of the random, value, and optimal agents


```python
ax = sns.lineplot(
    x='step', y='cumulative_reward', hue='agent',
    data=avg_cum_df.where(lambda x: x['agent'] != 'optimal vs. value')
)
```


![png](/chapter03/img/output_23_0.png)

________________

```python
ax = sns.lineplot(
    x='step', y='cumulative_reward', hue='agent',
    data=avg_cum_df.where(lambda x: x['agent'] == 'optimal vs. value')
)
```


![png](/chapter03/img/output_24_0.png)

_______________________
```python
ax = sns.distplot(
    per_agent_df \
        .where(lambda x: x['agent'] == 'optimal') \
        .where(lambda x: pd.notnull(x['cumulative_reward']))['cumulative_reward'],
    kde=False
)
```

    Optimal agent
    


![png](/chapter03/img/output_26_1.png)


____________________
```python
ax = sns.distplot(
    per_agent_df \
        .where(lambda x: x['agent'] == 'random') \
        .where(lambda x: pd.notnull(x['cumulative_reward']))['cumulative_reward'],
    kde=False
)
```

    Random agent
    


![png](/chapter03/img/output_27_1.png)
_______________________

## Chapter 4

[./chapter04/chapter04.ipynb](./chapter04/chapter04.ipynb)

### Gambler's Problem (Example 4.2 page 84)


    Policy(name='smallest_optimal', strategy=off-policy, v=V(t=34, γ=1.0, use_rn=T, θ=0.0000010, Δ=0.0000000), require_stable=True, converged=True, changes=0, stable=4)
    


![png](./chapter04/img/output_8_0.png)


    Figure 4.3
    


![png](./chapter04/img/output_9_0.png)


    Figure 4.3
    

The value iteration looks correct. The couple extra jaggies from the book result are due to floating point errors.

### Watch the skilled gambler play


```python
print(agent.π.name)
agent.π.strategy = Strategy.on
steps = c4util.episode(agent)
pprint(steps)
```

    smallest_optimal
    [T(t=0, s=Capital(cap=20), a=Stake(amount=5), r=0, rn=0),
     T(t=1, s=Capital(cap=15), a=Stake(amount=10), r=0, rn=0),
     T(t=2, s=Capital(cap=5), a=Stake(amount=5), r=0, rn=0),
     T(t=3, s=Capital(cap=10), a=Stake(amount=10), r=0, rn=0),
     T(t=4, s=Capital(cap=0), a=Stake(amount=0), r=0, rn=0)]
    

### Compare optimal and random players

Simulate many episodes with random starts.

    Random win rate: 19.92%
    Optimal win rate: 28.324%
    

#### Win times


```python
ax = sns.distplot(rdf.where(lambda x: x['agent'] == 'random')['steps'], kde=False)
_ = ax.set(xlabel='Steps (random)', ylabel='Count')
```


![png](./chapter04/img/output_22_0.png)
_______________


```python
ax = sns.distplot(rdf.where(lambda x: x['agent'] == 'optimal')['steps'], kde=False)
_ = ax.set(xlabel='Steps (optimal)', ylabel='Count')
```


![png](./chapter04/img/output_23_0.png)


The trained gambler has much shorter runs, resulting in victory more often.

_____________________
```python
rdf['log_steps'] = rdf['steps'].apply(lambda x: np.log(x))
```


```python
g = sns.pairplot(rdf.where(
    lambda x: x['agent'] == 'random').drop(columns=['steps']),
    hue='result', hue_order=['win', 'lose'], kind='reg', height=3
)
_ = g.set(title='random')
```


![png](./chapter04/img/output_26_0.png)

__________________________

```python
g = sns.pairplot(
    rdf.where(lambda x: x['agent'] == 'optimal').drop(columns=['steps']),
    hue='result', hue_order=['win', 'lose'], kind='reg', height=3
)
_ = g.set(title='optimal')
```


![png](./chapter04/img/output_27_0.png)


### Simulating Data

Two different simulation algorithms are used: Monte Carlo episodic with exploring starts, and value iteration with incremental probability updates.

#### Monte Carlo Exploring Starts

    Policy(name='mc', strategy=off-policy, v=V(t=0, γ=1.0, use_rn=F, θ=0.0010000, Δ=1000.0000000), require_stable=False, converged=False, changes=0, stable=1)
    

![png](./chapter04/img/output_34_0.png)



![png](./chapter04/img/output_35_0.png)


#### Episodic Value Iteration

Instead of providing apriori probabilities to the value iteration (e.g. p_heads = 0.4), it is possible to learn these stationary probabilities over time by playing episodes. In this way, the probability estimate improves each iteration while the time complexity remains constant.

    Policy(name='episodic_value', strategy=off-policy, v=V(t=0, γ=1.0, use_rn=T, θ=0.0010000, Δ=0.0000000), require_stable=False, converged=True, changes=0, stable=1)
    

![png](./chapter04/img/output_41_0.png)


![png](./chapter04/img/output_42_0.png)


Both simulated methods have value shapes similar to the theoretical optimal, and policies that attempt to make the max stake every time.

### Play many times and compare to the optimal value iteration agent

    MC win rate: 27.016%
    Episodic value iteration win rate: 27.192%
    

#### Win times

```python
ax = sns.distplot(rdf.where(lambda x: x['agent'] == 'mc')['steps'], kde=False)
_ = ax.set(xlabel='Steps', ylabel='Count')
```


![png](./chapter04/img/output_51_0.png)

_________________

```python
ax = sns.distplot(rdf.where(lambda x: x['agent'] == 'episodic_value')['steps'], kde=False)
_ = ax.set(xlabel='Steps', ylabel='Count')
```


![png](./chapter04/img/output_52_0.png)

_________________________

#### Compared to optimal


```python
rdf['log_steps'] = rdf['steps'].apply(lambda x: np.log(x))
g = sns.pairplot(
    rdf.where(lambda x: (x['agent'] != 'random') & (x['result'] == 'win')).drop(columns=[]),
    hue='agent', kind='reg', height=4
)
```

![png](./chapter04/img/output_54_0.png)

_____________________

```python
rdf['log_steps'] = rdf['steps'].apply(lambda x: np.log(x))
g = sns.pairplot(
    rdf.where(lambda x: (x['agent'] != 'random') & (x['result'] == 'lose')).drop(columns=[]),
    hue='agent', kind='reg', height=4
)
```


![png](./chapter04/img/output_55_0.png)


The simulated agents have similar win rates and win times to the optimal policy agent. The value estimates are noisier but good enough to find a decent policy. The monte carlo exploring starts loses more slowly but wins more slowly, too. The episodic value iteration policy is most similar to value iteration.


## Chapter 5

[./chapter05/chapter05.ipynb](./chapter05/chapter05.ipynb)

### Blackjack

#### Watch some games with the dealer/player policies

    
     Game 2
    Policy: Policy(name='hit below 20', strategy=off-policy, v=V(t=0, γ=1.0, θ=0.0000010, Δ=1000.0000000), require_stable=False, es=False, converged=False, changes=0, stable=3)
    T(t=0, s=Visible(p_value=9, d_value=13, d_top_value=10, useable_ace=False), a=Turn(draw=hit , role=player), r=0, rn=0),
    T(t=1, s=Visible(p_value=11, d_value=13, d_top_value=10, useable_ace=False), a=Turn(draw=hit , role=player), r=0, rn=0),
    T(t=2, s=Visible(p_value=21, d_value=13, d_top_value=10, useable_ace=False), a=Turn(draw=hit , role=dealer), r=0, rn=-1),
    T(t=3, s=Visible(p_value=21, d_value=22, d_top_value=10, useable_ace=False), a=Turn(draw=stay, role=player), r=0, rn=1)
    Result: Status.win Reward: 1
    (Player: 21 vs Dealer: 22)
    
    

#### Learn the state-value function for the player policy

##### Using averaged returns and Monte Carlo first state episodic


    Q = Algorithms.mc_first_visit(episode=blackjack_episode, agent=agent, kwargs={}, n=m)

    100%|███████████████████████████████| 150000/150000 [01:20<00:00, 1868.77it/s]
    

Figure 5.1
___________________


![png](./chapter05/img/output_10_0.png)



![png](./chapter05/img/output_11_0.png)

____________________

#### Action-values via Monte Carlo exploring start and random behavior policy


```python
mc_exploring = Player(env=Table(), π=default_policy(γ=1.0))
mc_exploring.π.es = True
mc_exploring.π.strategy = Strategy.on
mc_exploring.π.select = SelectA.best
```


    AQ = Algorithms.mc_exploring_starts(episode=blackjack_episode, agent=mc_exploring, kwargs={}, n=m)
    100%|███████████████████████████████| 500000/500000 [04:50<00:00, 1721.40it/s]
    
___________________

![png](./chapter05/img/output_15_0.png)


![png](./chapter05/img/output_15_2.png)

___________________

![png](./chapter05/img/output_16_0.png)


![png](./chapter05/img/output_16_2.png)


This policy and value function looks reasonable. There are a few differences from the book optimal result, not sure why.
________________

### Compare win rates against the deterministic dealer policy

Simulate many episodes and compare results.

    Default Agent:    win = 29.7%, tie = 5.0%, lose = 65.3%
    MC Explore Agent: win = 43.1%, tie = 8.8%, lose = 48.2%
    Episodic Value:   win = 43.7%, tie = 8.6%, lose = 47.7%
    

## Importance Sampling

### Example 5.4 (Blackjack)


```python
import seaborn as sns
from chapter05.util import example_5_4
```


```python
plotdf, data = example_5_4(max_ep=300_000)
```

```python
ax = sns.lineplot(x="episodes", y="mse", hue="method", data=plotdf.where(lambda x: x.method != "none"), sort=False)
```


![png](./chapter05/img/output_42_0.png)


*Note: Episodes had random starts, so more are required to lower the error*
