from dataclasses import dataclass
from random import choice, random
from typing import *

import pandas as pd
import seaborn as sns
from numpy import argmax, array, bitwise_or, exp, full, log, ndarray, ones
from numpy import round as np_round
from numpy import sqrt, zeros
from numpy.random import choice as np_choice

from utils import (
    bandit_id,
    bandit_predicate,
    normal,
    random_argmax,
    random_not_argmax,
    sumr,
)

sns.set(style="whitegrid")


def where(df: pd.DataFrame, func: Callable):
    """ Usage
    df.where(lambda x: x[0] < 0).where(lambda x: x[1] > 0)
    """
    return df[func(df)]


pd.DataFrame.where = where


@dataclass
class Action:
    count: int = 0
    reward: float = 0
    last_method: str = ""


@dataclass
class Reward:
    q: float = 0  # reward value
    o: bool = False  # was optimal action


def weight_n(n: int) -> float:
    """w(n) = 1 / n"""
    return 1 / n


def weight_1(n: int) -> float:
    """w = 0.1"""
    return 0.1


def q_sample_average(
    r_avg_prev: float, r_curr: float, weight: Callable, count: int
) -> float:
    """ page 31, eq 2.4 """
    return r_avg_prev + weight(count) * (r_curr - r_avg_prev)


class Bandit:
    """ Section 2.3 page 28 """

    def __init__(
        self,
        k: int = 10,  # k-armed bandit / number of actions
        eps: float = 0,  # probability exploratory action is taken
        n: int = 1000,  # number of steps to take for each problem
        q1: float = 0,  # initial reward estimate
        c: float = 0,  # degree of exploration in ucb selection
        b: float = 0,  # mean-offset for computing baseline action rewards
        alpha: float = 0.0,  # step size parameter for gradient update. use the gradient ascent selection method if non-zero
        rb: bool = True,  # if false, omits the reward baseline Rbar from Ht update
        label: str = "",  # optional descriptor
        weight: Callable = weight_n,  # step-size parameter, must be function of nth iteration or a static return
    ) -> None:
        self.k = k
        self.eps = eps
        self.c = c
        self.b = b
        self.n = n
        self.q1 = q1
        self.rb = rb
        self.alpha = alpha
        self.weight = weight
        self.id = bandit_id(q1, eps, c, b, rb, weight, alpha)
        self.predicate = bandit_predicate(q1, eps, c, b, rb, weight, alpha)
        self.label = label
        self.actions: ndarray = array([i for i in range(self.k)])
        self.reset()

    def __hash__(self) -> int:
        return hash(self.id)

    def __eq__(self, other) -> bool:
        return self.id == other.id

    def __repr__(self) -> str:
        return f"{self.predicate}: {self.Ht if self.alpha else self.reward_estimates}"

    def reset(self) -> None:
        self.baseline_action_vaues = normal(loc=self.b, scale=1, size=self.k)
        self.empirical_rewards = normal(
            loc=self.baseline_action_vaues, scale=1, size=self.n
        )
        self.reward_estimates: List[Action] = [
            Action(reward=self.q1) for _ in range(self.k)
        ]
        self.step = 0  # current step
        self.optimal_action = argmax(self.baseline_action_vaues)
        self.rewards: List[Reward] = [Reward() for _ in range(self.n)]
        self.reward_avg: float = self.q1
        self.Ht: ndarray = zeros(self.k)  # action preference

    def train(self) -> None:
        while self.step < self.n:
            self.update_estimate(*self.select_action(self.step))
            self.step += 1

    def select_action(self, step: int) -> Tuple[int, float, str]:
        if self.eps == -1:
            action = self.ucb(self.reward_estimates)
            method = "UCB"
        elif self.alpha:
            action = np_choice(self.actions, p=self.softmax(self.Ht))
            method = "gradient"
        elif random() > self.eps:
            action = random_argmax(self.reward_estimates)
            method = "greedy"
        else:
            action = random_not_argmax(self.reward_estimates)
            method = "exploratory"

        empirical_reward = self.empirical_rewards[action][step]
        self.rewards[step] = Reward(empirical_reward, action == self.optimal_action)
        return action, empirical_reward, method

    def update_estimate(
        self, action_index: int, reward: float, selection_method: str = ""
    ) -> None:
        action = self.reward_estimates[action_index]
        count, q_prev = action.count + 1, action.reward
        estimate = q_sample_average(q_prev, reward, self.weight, count)
        self.reward_estimates[action_index] = Action(count, estimate, selection_method)
        if selection_method == "gradient":
            if self.rb:
                self.reward_avg = q_sample_average(
                    self.reward_avg, reward, self.weight, self.step + 1
                )
            self.Ht = self.update_ht(
                Rt=reward, Rbar=self.reward_avg, At=action_index, Ht=self.Ht
            )

    def update_ht(self, Rt: float, Rbar: float, At: int, Ht: ndarray) -> ndarray:
        """ Section 2.8 eq. 2.12 """
        a_mask = full(self.k, True, dtype=bool)
        a_mask[At] = False
        delta = self.alpha * (Rt - Rbar)
        pi = self.softmax(Ht)
        Ht_next = zeros(self.k)
        Ht_next[At] = Ht[At] + delta * (1 - pi[At])
        Ht_next[a_mask] = Ht[a_mask] - delta * pi[a_mask]
        return Ht_next

    def ucb(self, actions: List[Action]) -> int:
        """ upper confidence bound """
        Qt = array([a.reward for a in actions])
        Nt = array([max(a.count, 0.01) for a in actions])
        return argmax(Qt + self.c * sqrt(log(self.step + 1) / Nt))

    @staticmethod
    def softmax(Ht: ndarray) -> ndarray:
        return exp(Ht) / exp(Ht).sum()


class Testbed:
    def __init__(self, bandits: List[Bandit] = [Bandit()], m: int = 2000) -> None:
        self.bandits = bandits  # list of bandit configurations to test
        self.m = m  # number of k-armed bandit problems to test
        self.rewards = {b: {prob: [] for prob in range(m)} for b in bandits}
        self.rewards_df: Optional[pd.DataFrame] = None

    def run_tests(self) -> None:
        from tqdm import tqdm

        for b in self.bandits:
            print(f"\n{b.predicate}")
            for prob in tqdm(range(self.m)):
                b.train()
                self.rewards[b][prob] = b.rewards
                b.reset()
            self.print_running_average(self.rewards[b], prob, b.predicate)

    def avg_per_step(self, rewards: Dict[int, List[Reward]]) -> Tuple[float, float]:
        """ averages the reward per step across the m problem instances """
        from numpy import matrix

        all_rewards = [[step.q for step in prob] for prob in rewards.values() if prob]
        all_optimal = [[step.o for step in prob] for prob in rewards.values() if prob]
        avg_rewards = np_round(matrix(all_rewards).mean(0), decimals=5).tolist()[0]
        optimal = np_round(matrix(all_optimal).mean(0), decimals=5).tolist()[0]

        return avg_rewards, optimal

    def print_running_average(
        self, rewards: Dict[int, List[Reward]], prob: int, descriptor: str
    ) -> None:
        last_avg, last_optimal = (step[-1] for step in self.avg_per_step(rewards))
        print(f"Percent Optimal: {last_optimal: .1%}, Average reward: {last_avg}")

    def build_dataframe(self) -> None:
        samples = []
        for b, reward in self.rewards.items():
            r, o = self.avg_per_step(reward)
            for step in range(b.n):
                samples.append(
                    [
                        step,
                        max(0, r[step]),
                        o[step],
                        f"= {b.eps}",
                        f"= {b.q1}",
                        b.id,
                        b.predicate,
                        b.label,
                        f"= {b.c}",
                        f"= {b.alpha}",
                    ]
                )
        self.rewards_df = pd.DataFrame(
            samples,
            columns=[
                "step",
                "avg_reward",
                "percent_optimal",
                "eps",
                "Q1",
                "id",
                "predicate",
                "label",
                "c",
                "alpha",
            ],
        )

    def graph_reward_per_iteration(
        self,
        plot_params: Dict[str, str] = {
            "x": "step",
            "y": "percent_optimal",
            "hue": "eps",
        },
        filter: Callable = None,
        bandits: List[Bandit] = None,
    ) -> pd.DataFrame:

        ids, l = [b.id for b in bandits], len(bandits)
        bandit_filter = lambda x: bitwise_or.reduce(
            [i[0] == i[1] for i in zip([x["id"] for _ in range(l)], ids)]
        )
        if self.rewards_df is None:
            self.build_dataframe()

        # plot
        samples_df = self.rewards_df
        samples_df = self.rewards_df.where(bandit_filter) if bandits else samples_df
        samples_df = self.rewards_df.where(filter) if filter else samples_df
        plot_params.update({"data": samples_df})
        sns.lineplot(**plot_params)

        return samples_df

    def fig_2_2_a(self, bandits: List[Bandit]) -> None:
        params = {"x": "step", "y": "avg_reward", "hue": "eps"}
        self.graph_reward_per_iteration(plot_params=params, bandits=bandits)

    def fig_2_2_b(self, bandits: List[Bandit]) -> None:
        params = {"x": "step", "y": "percent_optimal", "hue": "eps"}
        self.graph_reward_per_iteration(plot_params=params, bandits=bandits)

    def fig_2_3(self, bandits: List[Bandit]) -> None:
        params = {"x": "step", "y": "percent_optimal", "hue": "predicate"}
        self.graph_reward_per_iteration(plot_params=params, bandits=bandits)

    def fig_2_4(self, bandits: List[Bandit]) -> None:
        params = {"x": "step", "y": "avg_reward", "hue": "predicate"}
        self.graph_reward_per_iteration(plot_params=params, bandits=bandits)

    def fig_2_5(self, bandits: List[Bandit] = None) -> None:
        params = {"x": "step", "y": "percent_optimal", "hue": "predicate"}
        self.graph_reward_per_iteration(plot_params=params, bandits=bandits)


if __name__ == "__main__":
    gradient_bandit = Bandit(q1=4, b=4, weight=weight_1, alpha=0.4)
    t = Testbed(bandits=[gradient_bandit])
    t.run_tests()
