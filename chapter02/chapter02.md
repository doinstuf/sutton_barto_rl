## Chapter 2

[./chapter02.ipynb](./chapter02.ipynb)

```python
from k_armed_testbed import Testbed, Bandit, weight_n, weight_1
```


```python
bandits = [
    # different eps
    Bandit(eps=0, q1=0, weight=weight_n),
    Bandit(eps=0.01, q1=0, weight=weight_n),
    Bandit(eps=0.1, q1=0, weight=weight_n),
    
    # optimistic initial values with weighted average
    Bandit(eps=0, q1=5, weight=weight_1),
    Bandit(eps=0.1, q1=0, weight=weight_1),
    
    # upper confidence bound selection
    Bandit(eps=-1, q1=0, c=2, weight=weight_1),
    
    # gradient action preferences with softmax selection
    Bandit(q1=4, b=4, rb=True, alpha=0.1),
    Bandit(q1=4, b=4, rb=True, alpha=0.4),
    Bandit(q1=0, b=4, rb=False, alpha=0.1),
    Bandit(q1=0, b=4, rb=False, alpha=0.4),
]

t = Testbed(bandits=bandits)
t.run_tests()
```

```
    Q1 = 0, eps = 0, w(n) = 1 / n
    100%|████████████████████████████| 2000/2000 [00:14<00:00, 135.05it/s]
    Percent Optimal:  37.9%, Average reward: 1.05238
    
    Q1 = 0, eps = 0.01, w(n) = 1 / n
    100%|████████████████████████████| 2000/2000 [00:14<00:00, 139.41it/s]
    Percent Optimal:  60.9%, Average reward: 1.32204
    
    Q1 = 0, eps = 0.1, w(n) = 1 / n
    100%|████████████████████████████| 2000/2000 [00:14<00:00, 135.15it/s]
    Percent Optimal:  79.5%, Average reward: 1.36885
    
    Q1 = 5, eps = 0, w = 0.1
    100%|████████████████████████████| 2000/2000 [00:14<00:00, 139.92it/s]
    Percent Optimal:  87.0%, Average reward: 1.5618
    
    Q1 = 0, eps = 0.1, w = 0.1
    100%|████████████████████████████| 2000/2000 [00:15<00:00, 132.51it/s]
    Percent Optimal:  75.2%, Average reward: 1.28779
    
    Q1 = 0, c = 2, w = 0.1
    100%|████████████████████████████| 2000/2000 [00:34<00:00, 57.65it/s]
    Percent Optimal:  71.1%, Average reward: 1.38302
    
    Q1 = 4, b = 4, a = 0.1, rb = True
    100%|████████████████████████████| 2000/2000 [01:34<00:00, 21.17it/s]
    Percent Optimal:  85.4%, Average reward: 5.48367
    
    Q1 = 4, b = 4, a = 0.4, rb = True
    100%|████████████████████████████| 2000/2000 [01:34<00:00, 21.15it/s]
    Percent Optimal:  72.5%, Average reward: 5.48396
    
    Q1 = 0, b = 4, a = 0.1, rb = False
    100%|████████████████████████████| 2000/2000 [01:34<00:00, 21.52it/s]
    Percent Optimal:  48.5%, Average reward: 5.228
    
    Q1 = 0, b = 4, a = 0.4, rb = False
    100%|████████████████████████████| 2000/2000 [01:34<00:00, 21.22it/s]
    Percent Optimal:  27.1%, Average reward: 4.78429
```

### Graphs

```python
t.fig_2_2_a(bandits[:3])
```


![png](./img/output_4_0.png)



```python
t.fig_2_2_b(bandits[:3])
```


![png](./img/output_5_0.png)



```python
t.fig_2_3(bandits[3:5])
```


![png](./img/output_6_0.png)



```python
t.fig_2_4(bandits[4:6])
```


![png](./img/output_7_0.png)



```python
t.fig_2_5(bandits[6:10])
```


![png](./img/output_8_0.png)

### Parameter Study


```python
from k_armed_testbed import Testbed, Bandit, weight_n, weight_1
from utils import p_gen, flatten
from fractions import Fraction
import seaborn as sns
sns.set(style="whitegrid")
```


```python
labels = ['e-greedy', 'gradient', 'UCB', 'greedy w/ optimistic init']
params = ['eps', 'alpha', 'c', 'Q1']
p_lookup = {x[0]: x[1] for x in zip(labels, params)}

bandits = [
    [Bandit(label=labels[0], eps=p) for p in p_gen(128, 6)],
    [Bandit(label=labels[1], alpha=p, rb=True) for p in p_gen(32, 8)],
    [Bandit(label=labels[2], c=p, eps=-1) for p in p_gen(16, 7)],
    [Bandit(label=labels[3], q1=p, eps=0.05, weight=weight_1) for p in p_gen(4, 5)]
]

t = Testbed(bandits=flatten(bandits), m=2000)
t.run_tests()
t.build_dataframe()
```

```python
# map the various column parameters into single column and convert them to a fraction
samples_df = t.rewards_df
samples_df['p'] = samples_df \
    .drop(columns=['id', 'predicate']) \
    .apply(lambda x: str(Fraction(x[p_lookup[x['label']]][2:])), axis=1)
```


```python
# average the steps for each param/label group
results_df = samples_df[['label', 'p', 'avg_reward']] \
    .groupby(['label', 'p'], as_index=False) \
    .mean()

# sort according to the correct fraction label order, for plotting
p_order = [str(Fraction(p)) for p in p_gen(128, 10)]
results_df = results_df.set_index('p').loc[p_order].reset_index()
```


```python
ax = sns.lineplot(x='p', y="avg_reward", hue="label", sort=False, data=results_df)
```


![png](./img/output_14_0.png)
