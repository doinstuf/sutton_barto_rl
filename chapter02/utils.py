from typing import Callable, List, Any, Union, Tuple
from numpy import ndarray
from numpy.random import normal as np_normal
from numpy import round as np_round
from random import choice

Action = Any  # see k_armed_testbed.Action


def bandit_predicate(
    q1: float, eps: float, c: float, b: float, rb: bool, weight: Callable, alpha: float
):
    """ tabular filter predicate and formatted descriptor for plotting """
    terms = [
        f"Q1 = {q1}",
        f"eps = {eps}" if eps != -1 and not alpha else None,
        f"c = {c}" if eps == -1 else None,
        f"b = {b}" if b else None,
        f"a = {alpha}" if alpha else None,
        f"rb = {rb}" if alpha else None,
        f"{weight.__doc__}" if not alpha else None,
    ]
    return ", ".join([t for t in terms if t])


def bandit_id(
    q1: float, eps: float, c: float, b: float, rb: bool, weight: Callable, alpha: float
):
    """ uniquely identifies the configuration of the object """
    return f"{q1}_{eps}_{c}_{b}_{rb}_{alpha}_{weight.__doc__}"


def random_argmax(actions: List[Action]) -> int:
    """ argmax except selects randomly from arguments if there is a tie for max """
    max_reward = max([a.reward for a in actions])
    max_rewards = [i for i, a in enumerate(actions) if a.reward == max_reward]
    return (
        choice(max_rewards) if max_rewards else choice([x for x in range(len(actions))])
    )


def random_not_argmax(actions: List[Action]) -> int:
    """ selects randomly from all arguments that are not argmax """
    max_reward = max([a.reward for a in actions])
    not_max_rewards = [i for i, a in enumerate(actions) if a.reward != max_reward]
    return (
        choice(not_max_rewards)
        if not_max_rewards
        else choice([x for x in range(len(actions))])
    )


def normal_float(
    loc: Union[float, List[float]], scale: float, size: int, decimals: int = 5
) -> List[float]:
    return np_round(
        np_normal(loc=loc, scale=scale, size=size), decimals=decimals
    ).tolist()


def normal(
    loc: Union[float, List[float]], scale: float, size: int, decimals: int = 5
) -> Union[List[float], List[List[float]]]:
    if isinstance(loc, float) or isinstance(loc, int):
        return normal_float(loc, scale, size, decimals)
    else:
        return [normal_float(i, scale, size, decimals) for i in loc]


def sumr(l: List[Any]) -> float:
    return round(sum(l), 5)


def p_gen(s: int, n: int):
    """piecewise power of 2 generator len([1/s, ...]) == n"""
    from numpy import log2

    l = int(log2(s)) + 1
    below_0 = [0.5 ** x for x in reversed(range(l))][:n]
    above_0 = [2 ** x for x in range(l - n - 1, n) if x >= 1]
    return (below_0 + above_0)[:n]


flatten = lambda l: [item for sublist in l for item in sublist]
